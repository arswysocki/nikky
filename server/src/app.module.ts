import { join } from 'path';
import { Module } from '@nestjs/common';
import { GraphQLModule } from '@nestjs/graphql';
import { UserModule } from './user.module';

@Module({
  imports: [
    UserModule,
    GraphQLModule.forRoot({
      debug: true,
      playground: true,
      autoSchemaFile: join(process.cwd(), '../schema.graphql'),
    }),
  ],
})
export class AppModule { }
