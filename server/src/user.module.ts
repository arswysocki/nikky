import { Module } from '@nestjs/common';
import { UserCrudResolver } from './@generated/prisma-nestjs';

@Module({
  providers: [UserCrudResolver],
})
export class UserModule { }