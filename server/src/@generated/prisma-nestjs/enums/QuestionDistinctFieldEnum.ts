import { registerEnumType } from "@nestjs/graphql";

export enum QuestionDistinctFieldEnum {
  id = "id",
  type = "type",
  formId = "formId"
}
registerEnumType(QuestionDistinctFieldEnum, {
  name: "QuestionDistinctFieldEnum",
  description: undefined,
});
