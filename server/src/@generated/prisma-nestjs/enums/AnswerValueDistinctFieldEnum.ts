import { registerEnumType } from "@nestjs/graphql";

export enum AnswerValueDistinctFieldEnum {
  id = "id",
  questionId = "questionId",
  desc = "desc"
}
registerEnumType(AnswerValueDistinctFieldEnum, {
  name: "AnswerValueDistinctFieldEnum",
  description: undefined,
});
