import { registerEnumType } from "@nestjs/graphql";

export enum AnswerStatus {
  New = "New",
  Answered = "Answered",
  Skipped = "Skipped"
}
registerEnumType(AnswerStatus, {
  name: "AnswerStatus",
  description: undefined,
});
