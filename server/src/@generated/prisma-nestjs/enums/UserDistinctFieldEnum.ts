import { registerEnumType } from "@nestjs/graphql";

export enum UserDistinctFieldEnum {
  id = "id",
  firstName = "firstName",
  lastName = "lastName",
  email = "email",
  picture = "picture",
  gender = "gender",
  role = "role"
}
registerEnumType(UserDistinctFieldEnum, {
  name: "UserDistinctFieldEnum",
  description: undefined,
});
