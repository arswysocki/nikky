import { registerEnumType } from "@nestjs/graphql";

export enum QuestionType {
  text = "text",
  multiple = "multiple",
  single = "single"
}
registerEnumType(QuestionType, {
  name: "QuestionType",
  description: undefined,
});
