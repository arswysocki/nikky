import { registerEnumType } from "@nestjs/graphql";

export enum FormDistinctFieldEnum {
  id = "id",
  desc = "desc",
  assessmentId = "assessmentId"
}
registerEnumType(FormDistinctFieldEnum, {
  name: "FormDistinctFieldEnum",
  description: undefined,
});
