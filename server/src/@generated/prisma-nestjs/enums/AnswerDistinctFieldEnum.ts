import { registerEnumType } from "@nestjs/graphql";

export enum AnswerDistinctFieldEnum {
  id = "id",
  canSkip = "canSkip",
  userId = "userId",
  status = "status",
  isAnonymous = "isAnonymous",
  answerText = "answerText",
  questionId = "questionId",
  answerValueId = "answerValueId"
}
registerEnumType(AnswerDistinctFieldEnum, {
  name: "AnswerDistinctFieldEnum",
  description: undefined,
});
