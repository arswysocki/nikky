import { registerEnumType } from "@nestjs/graphql";

export enum AssessmentDistinctFieldEnum {
  id = "id",
  userId = "userId",
  recipientId = "recipientId",
  projectId = "projectId"
}
registerEnumType(AssessmentDistinctFieldEnum, {
  name: "AssessmentDistinctFieldEnum",
  description: undefined,
});
