import { registerEnumType } from "@nestjs/graphql";

export enum ProjectDistinctFieldEnum {
  id = "id"
}
registerEnumType(ProjectDistinctFieldEnum, {
  name: "ProjectDistinctFieldEnum",
  description: undefined,
});
