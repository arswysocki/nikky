import { Args, Context, ResolveField, Resolver, Root } from "@nestjs/graphql";
import { Assessment } from "../../../models/Assessment";
import { Form } from "../../../models/Form";
import { Project } from "../../../models/Project";
import { User } from "../../../models/User";
import { plainToClass } from "class-transformer";
import { AssessmentFormsArgs } from "./args/AssessmentFormsArgs";

@Resolver(() => Assessment)
export class AssessmentRelationsResolver {
  @ResolveField(() => User, {
    nullable: false,
    description: undefined,
    complexity: ({ childComplexity }) => 1 * childComplexity
  })
  async user(@Root() assessment: Assessment, @Context() ctx: any): Promise<User> {
    return plainToClass(User, await ctx.prisma.assessment.findOne({
      where: {
        id: assessment.id,
      },
    }).user({}) as User);
  }

  @ResolveField(() => [Form], {
    nullable: true,
    description: undefined,
    complexity: ({ args, childComplexity }) => ((args.take + (args.skip ?? 0)) ?? 1) * childComplexity
  })
  async forms(@Root() assessment: Assessment, @Context() ctx: any, @Args() args: AssessmentFormsArgs): Promise<Form[] | undefined> {
    return plainToClass(Form, await ctx.prisma.assessment.findOne({
      where: {
        id: assessment.id,
      },
    }).forms(args) as [Form]);
  }

  @ResolveField(() => User, {
    nullable: false,
    description: undefined,
    complexity: ({ childComplexity }) => 1 * childComplexity
  })
  async recipient(@Root() assessment: Assessment, @Context() ctx: any): Promise<User> {
    return plainToClass(User, await ctx.prisma.assessment.findOne({
      where: {
        id: assessment.id,
      },
    }).recipient({}) as User);
  }

  @ResolveField(() => Project, {
    nullable: false,
    description: undefined,
    complexity: ({ childComplexity }) => 1 * childComplexity
  })
  async Project(@Root() assessment: Assessment, @Context() ctx: any): Promise<Project> {
    return plainToClass(Project, await ctx.prisma.assessment.findOne({
      where: {
        id: assessment.id,
      },
    }).Project({}) as Project);
  }
}
