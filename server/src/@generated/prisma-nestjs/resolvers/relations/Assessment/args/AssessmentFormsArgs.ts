import { ArgsType, Field, Int } from "@nestjs/graphql";
import { FormOrderByInput } from "../../../inputs/FormOrderByInput";
import { FormWhereInput } from "../../../inputs/FormWhereInput";
import { FormWhereUniqueInput } from "../../../inputs/FormWhereUniqueInput";
import { Type as ClassTransformer__Type } from "class-transformer";
import { FormDistinctFieldEnum } from "../../../../enums/FormDistinctFieldEnum";

@ArgsType()
export class AssessmentFormsArgs {
  @ClassTransformer__Type(() => FormWhereInput)
  @Field(() => FormWhereInput, { nullable: true })
  where?: FormWhereInput | undefined;

  @ClassTransformer__Type(() => FormOrderByInput)
  @Field(() => [FormOrderByInput], { nullable: true })
  orderBy?: FormOrderByInput[] | undefined;

  @ClassTransformer__Type(() => FormWhereUniqueInput)
  @Field(() => FormWhereUniqueInput, { nullable: true })
  cursor?: FormWhereUniqueInput | undefined;

  @Field(() => Int, { nullable: true, defaultValue: 20 })
  take?: number | undefined;

  @Field(() => Int, { nullable: true })
  skip?: number | undefined;

  @Field(() => [FormDistinctFieldEnum], { nullable: true })
  distinct?: Array<keyof typeof FormDistinctFieldEnum> | undefined;
}
