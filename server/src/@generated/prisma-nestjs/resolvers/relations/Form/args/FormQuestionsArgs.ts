import { ArgsType, Field, Int } from "@nestjs/graphql";
import { QuestionOrderByInput } from "../../../inputs/QuestionOrderByInput";
import { QuestionWhereInput } from "../../../inputs/QuestionWhereInput";
import { QuestionWhereUniqueInput } from "../../../inputs/QuestionWhereUniqueInput";
import { Type as ClassTransformer__Type } from "class-transformer";
import { QuestionDistinctFieldEnum } from "../../../../enums/QuestionDistinctFieldEnum";

@ArgsType()
export class FormQuestionsArgs {
  @ClassTransformer__Type(() => QuestionWhereInput)
  @Field(() => QuestionWhereInput, { nullable: true })
  where?: QuestionWhereInput | undefined;

  @ClassTransformer__Type(() => QuestionOrderByInput)
  @Field(() => [QuestionOrderByInput], { nullable: true })
  orderBy?: QuestionOrderByInput[] | undefined;

  @ClassTransformer__Type(() => QuestionWhereUniqueInput)
  @Field(() => QuestionWhereUniqueInput, { nullable: true })
  cursor?: QuestionWhereUniqueInput | undefined;

  @Field(() => Int, { nullable: true, defaultValue: 20 })
  take?: number | undefined;

  @Field(() => Int, { nullable: true })
  skip?: number | undefined;

  @Field(() => [QuestionDistinctFieldEnum], { nullable: true })
  distinct?: Array<keyof typeof QuestionDistinctFieldEnum> | undefined;
}
