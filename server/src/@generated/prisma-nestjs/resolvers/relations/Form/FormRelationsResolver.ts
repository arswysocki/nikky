import { Args, Context, ResolveField, Resolver, Root } from "@nestjs/graphql";
import { Assessment } from "../../../models/Assessment";
import { Form } from "../../../models/Form";
import { Question } from "../../../models/Question";
import { plainToClass } from "class-transformer";
import { FormQuestionsArgs } from "./args/FormQuestionsArgs";

@Resolver(() => Form)
export class FormRelationsResolver {
  @ResolveField(() => [Question], {
    nullable: true,
    description: undefined,
    complexity: ({ args, childComplexity }) => ((args.take + (args.skip ?? 0)) ?? 1) * childComplexity
  })
  async questions(@Root() form: Form, @Context() ctx: any, @Args() args: FormQuestionsArgs): Promise<Question[] | undefined> {
    return plainToClass(Question, await ctx.prisma.form.findOne({
      where: {
        id: form.id,
      },
    }).questions(args) as [Question]);
  }

  @ResolveField(() => Assessment, {
    nullable: false,
    description: undefined,
    complexity: ({ childComplexity }) => 1 * childComplexity
  })
  async Assessment(@Root() form: Form, @Context() ctx: any): Promise<Assessment> {
    return plainToClass(Assessment, await ctx.prisma.form.findOne({
      where: {
        id: form.id,
      },
    }).Assessment({}) as Assessment);
  }
}
