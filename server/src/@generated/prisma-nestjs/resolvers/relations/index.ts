import { AnswerRelationsResolver } from "./Answer/AnswerRelationsResolver";
import { AnswerValueRelationsResolver } from "./AnswerValue/AnswerValueRelationsResolver";
import { AssessmentRelationsResolver } from "./Assessment/AssessmentRelationsResolver";
import { FormRelationsResolver } from "./Form/FormRelationsResolver";
import { ProjectRelationsResolver } from "./Project/ProjectRelationsResolver";
import { QuestionRelationsResolver } from "./Question/QuestionRelationsResolver";
import { UserRelationsResolver } from "./User/UserRelationsResolver";
import { Module } from "@nestjs/common";

export { AnswerRelationsResolver } from "./Answer/AnswerRelationsResolver";
export { AnswerValueRelationsResolver } from "./AnswerValue/AnswerValueRelationsResolver";
export { AssessmentRelationsResolver } from "./Assessment/AssessmentRelationsResolver";
export * from "./Assessment/args";
export { FormRelationsResolver } from "./Form/FormRelationsResolver";
export * from "./Form/args";
export { ProjectRelationsResolver } from "./Project/ProjectRelationsResolver";
export * from "./Project/args";
export { QuestionRelationsResolver } from "./Question/QuestionRelationsResolver";
export * from "./Question/args";
export { UserRelationsResolver } from "./User/UserRelationsResolver";
export * from "./User/args";

@Module({
  providers: [
    AnswerRelationsResolver,
    AnswerValueRelationsResolver,
    AssessmentRelationsResolver,
    FormRelationsResolver,
    ProjectRelationsResolver,
    QuestionRelationsResolver,
    UserRelationsResolver
  ],
  exports: [
    AnswerRelationsResolver,
    AnswerValueRelationsResolver,
    AssessmentRelationsResolver,
    FormRelationsResolver,
    ProjectRelationsResolver,
    QuestionRelationsResolver,
    UserRelationsResolver
  ]
})
export class RelationsResolversModule {
}
