import { Args, Context, ResolveField, Resolver, Root } from "@nestjs/graphql";
import { Answer } from "../../../models/Answer";
import { AnswerValue } from "../../../models/AnswerValue";
import { Question } from "../../../models/Question";
import { User } from "../../../models/User";
import { plainToClass } from "class-transformer";

@Resolver(() => Answer)
export class AnswerRelationsResolver {
  @ResolveField(() => Question, {
    nullable: false,
    description: undefined,
    complexity: ({ childComplexity }) => 1 * childComplexity
  })
  async question(@Root() answer: Answer, @Context() ctx: any): Promise<Question> {
    return plainToClass(Question, await ctx.prisma.answer.findOne({
      where: {
        id: answer.id,
      },
    }).question({}) as Question);
  }

  @ResolveField(() => User, {
    nullable: false,
    description: undefined,
    complexity: ({ childComplexity }) => 1 * childComplexity
  })
  async user(@Root() answer: Answer, @Context() ctx: any): Promise<User> {
    return plainToClass(User, await ctx.prisma.answer.findOne({
      where: {
        id: answer.id,
      },
    }).user({}) as User);
  }

  @ResolveField(() => AnswerValue, {
    nullable: false,
    description: undefined,
    complexity: ({ childComplexity }) => 1 * childComplexity
  })
  async answerValue(@Root() answer: Answer, @Context() ctx: any): Promise<AnswerValue> {
    return plainToClass(AnswerValue, await ctx.prisma.answer.findOne({
      where: {
        id: answer.id,
      },
    }).answerValue({}) as AnswerValue);
  }
}
