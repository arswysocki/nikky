import { ArgsType, Field, Int } from "@nestjs/graphql";
import { AssessmentOrderByInput } from "../../../inputs/AssessmentOrderByInput";
import { AssessmentWhereInput } from "../../../inputs/AssessmentWhereInput";
import { AssessmentWhereUniqueInput } from "../../../inputs/AssessmentWhereUniqueInput";
import { Type as ClassTransformer__Type } from "class-transformer";
import { AssessmentDistinctFieldEnum } from "../../../../enums/AssessmentDistinctFieldEnum";

@ArgsType()
export class ProjectAssessmentsArgs {
  @ClassTransformer__Type(() => AssessmentWhereInput)
  @Field(() => AssessmentWhereInput, { nullable: true })
  where?: AssessmentWhereInput | undefined;

  @ClassTransformer__Type(() => AssessmentOrderByInput)
  @Field(() => [AssessmentOrderByInput], { nullable: true })
  orderBy?: AssessmentOrderByInput[] | undefined;

  @ClassTransformer__Type(() => AssessmentWhereUniqueInput)
  @Field(() => AssessmentWhereUniqueInput, { nullable: true })
  cursor?: AssessmentWhereUniqueInput | undefined;

  @Field(() => Int, { nullable: true, defaultValue: 20 })
  take?: number | undefined;

  @Field(() => Int, { nullable: true })
  skip?: number | undefined;

  @Field(() => [AssessmentDistinctFieldEnum], { nullable: true })
  distinct?: Array<keyof typeof AssessmentDistinctFieldEnum> | undefined;
}
