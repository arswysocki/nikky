import { Args, Context, ResolveField, Resolver, Root } from "@nestjs/graphql";
import { Assessment } from "../../../models/Assessment";
import { Project } from "../../../models/Project";
import { plainToClass } from "class-transformer";
import { ProjectAssessmentsArgs } from "./args/ProjectAssessmentsArgs";

@Resolver(() => Project)
export class ProjectRelationsResolver {
  @ResolveField(() => [Assessment], {
    nullable: true,
    description: undefined,
    complexity: ({ args, childComplexity }) => ((args.take + (args.skip ?? 0)) ?? 1) * childComplexity
  })
  async assessments(@Root() project: Project, @Context() ctx: any, @Args() args: ProjectAssessmentsArgs): Promise<Assessment[] | undefined> {
    return plainToClass(Assessment, await ctx.prisma.project.findOne({
      where: {
        id: project.id,
      },
    }).assessments(args) as [Assessment]);
  }
}
