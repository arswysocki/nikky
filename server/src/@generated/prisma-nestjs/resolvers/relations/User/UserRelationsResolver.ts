import { Args, Context, ResolveField, Resolver, Root } from "@nestjs/graphql";
import { Answer } from "../../../models/Answer";
import { Assessment } from "../../../models/Assessment";
import { User } from "../../../models/User";
import { plainToClass } from "class-transformer";
import { UserAnswerArgs } from "./args/UserAnswerArgs";
import { UserRecipientArgs } from "./args/UserRecipientArgs";
import { UserRespondentArgs } from "./args/UserRespondentArgs";

@Resolver(() => User)
export class UserRelationsResolver {
  @ResolveField(() => [Assessment], {
    nullable: true,
    description: undefined,
    complexity: ({ args, childComplexity }) => ((args.take + (args.skip ?? 0)) ?? 1) * childComplexity
  })
  async Respondent(@Root() user: User, @Context() ctx: any, @Args() args: UserRespondentArgs): Promise<Assessment[] | undefined> {
    return plainToClass(Assessment, await ctx.prisma.user.findOne({
      where: {
        id: user.id,
      },
    }).Respondent(args) as [Assessment]);
  }

  @ResolveField(() => [Assessment], {
    nullable: true,
    description: undefined,
    complexity: ({ args, childComplexity }) => ((args.take + (args.skip ?? 0)) ?? 1) * childComplexity
  })
  async Recipient(@Root() user: User, @Context() ctx: any, @Args() args: UserRecipientArgs): Promise<Assessment[] | undefined> {
    return plainToClass(Assessment, await ctx.prisma.user.findOne({
      where: {
        id: user.id,
      },
    }).Recipient(args) as [Assessment]);
  }

  @ResolveField(() => [Answer], {
    nullable: true,
    description: undefined,
    complexity: ({ args, childComplexity }) => ((args.take + (args.skip ?? 0)) ?? 1) * childComplexity
  })
  async Answer(@Root() user: User, @Context() ctx: any, @Args() args: UserAnswerArgs): Promise<Answer[] | undefined> {
    return plainToClass(Answer, await ctx.prisma.user.findOne({
      where: {
        id: user.id,
      },
    }).Answer(args) as [Answer]);
  }
}
