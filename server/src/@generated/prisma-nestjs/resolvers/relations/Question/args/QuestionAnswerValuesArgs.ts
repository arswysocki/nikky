import { ArgsType, Field, Int } from "@nestjs/graphql";
import { AnswerValueOrderByInput } from "../../../inputs/AnswerValueOrderByInput";
import { AnswerValueWhereInput } from "../../../inputs/AnswerValueWhereInput";
import { AnswerValueWhereUniqueInput } from "../../../inputs/AnswerValueWhereUniqueInput";
import { Type as ClassTransformer__Type } from "class-transformer";
import { AnswerValueDistinctFieldEnum } from "../../../../enums/AnswerValueDistinctFieldEnum";

@ArgsType()
export class QuestionAnswerValuesArgs {
  @ClassTransformer__Type(() => AnswerValueWhereInput)
  @Field(() => AnswerValueWhereInput, { nullable: true })
  where?: AnswerValueWhereInput | undefined;

  @ClassTransformer__Type(() => AnswerValueOrderByInput)
  @Field(() => [AnswerValueOrderByInput], { nullable: true })
  orderBy?: AnswerValueOrderByInput[] | undefined;

  @ClassTransformer__Type(() => AnswerValueWhereUniqueInput)
  @Field(() => AnswerValueWhereUniqueInput, { nullable: true })
  cursor?: AnswerValueWhereUniqueInput | undefined;

  @Field(() => Int, { nullable: true, defaultValue: 20 })
  take?: number | undefined;

  @Field(() => Int, { nullable: true })
  skip?: number | undefined;

  @Field(() => [AnswerValueDistinctFieldEnum], { nullable: true })
  distinct?: Array<keyof typeof AnswerValueDistinctFieldEnum> | undefined;
}
