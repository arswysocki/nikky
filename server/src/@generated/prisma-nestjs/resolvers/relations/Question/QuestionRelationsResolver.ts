import { Args, Context, ResolveField, Resolver, Root } from "@nestjs/graphql";
import { Answer } from "../../../models/Answer";
import { AnswerValue } from "../../../models/AnswerValue";
import { Form } from "../../../models/Form";
import { Question } from "../../../models/Question";
import { plainToClass } from "class-transformer";
import { QuestionAnswerArgs } from "./args/QuestionAnswerArgs";
import { QuestionAnswerValuesArgs } from "./args/QuestionAnswerValuesArgs";

@Resolver(() => Question)
export class QuestionRelationsResolver {
  @ResolveField(() => Form, {
    nullable: false,
    description: undefined,
    complexity: ({ childComplexity }) => 1 * childComplexity
  })
  async Form(@Root() question: Question, @Context() ctx: any): Promise<Form> {
    return plainToClass(Form, await ctx.prisma.question.findOne({
      where: {
        id: question.id,
      },
    }).Form({}) as Form);
  }

  @ResolveField(() => [AnswerValue], {
    nullable: true,
    description: undefined,
    complexity: ({ args, childComplexity }) => ((args.take + (args.skip ?? 0)) ?? 1) * childComplexity
  })
  async answerValues(@Root() question: Question, @Context() ctx: any, @Args() args: QuestionAnswerValuesArgs): Promise<AnswerValue[] | undefined> {
    return plainToClass(AnswerValue, await ctx.prisma.question.findOne({
      where: {
        id: question.id,
      },
    }).answerValues(args) as [AnswerValue]);
  }

  @ResolveField(() => [Answer], {
    nullable: true,
    description: undefined,
    complexity: ({ args, childComplexity }) => ((args.take + (args.skip ?? 0)) ?? 1) * childComplexity
  })
  async answer(@Root() question: Question, @Context() ctx: any, @Args() args: QuestionAnswerArgs): Promise<Answer[] | undefined> {
    return plainToClass(Answer, await ctx.prisma.question.findOne({
      where: {
        id: question.id,
      },
    }).answer(args) as [Answer]);
  }
}
