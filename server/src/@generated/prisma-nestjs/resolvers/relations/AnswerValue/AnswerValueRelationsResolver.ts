import { Args, Context, ResolveField, Resolver, Root } from "@nestjs/graphql";
import { Answer } from "../../../models/Answer";
import { AnswerValue } from "../../../models/AnswerValue";
import { Question } from "../../../models/Question";
import { plainToClass } from "class-transformer";

@Resolver(() => AnswerValue)
export class AnswerValueRelationsResolver {
  @ResolveField(() => Question, {
    nullable: false,
    description: undefined,
    complexity: ({ childComplexity }) => 1 * childComplexity
  })
  async Question(@Root() answerValue: AnswerValue, @Context() ctx: any): Promise<Question> {
    return plainToClass(Question, await ctx.prisma.answerValue.findOne({
      where: {
        id: answerValue.id,
      },
    }).Question({}) as Question);
  }

  @ResolveField(() => Answer, {
    nullable: true,
    description: undefined,
    complexity: ({ childComplexity }) => 1 * childComplexity
  })
  async answer(@Root() answerValue: AnswerValue, @Context() ctx: any): Promise<Answer | undefined> {
    return plainToClass(Answer, await ctx.prisma.answerValue.findOne({
      where: {
        id: answerValue.id,
      },
    }).answer({}) as Answer);
  }
}
