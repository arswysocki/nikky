import { Field, Float, ID, InputType, Int } from "@nestjs/graphql";
import GraphQLJSON from "graphql-type-json";
import { JsonValue, InputJsonValue } from "../../../../../node_modules/@prisma/client";
import { Type as ClassTransformer__Type } from "class-transformer";
import { FormCreateManyWithoutAssessmentInput } from "../inputs/FormCreateManyWithoutAssessmentInput";
import { ProjectCreateOneWithoutAssessmentsInput } from "../inputs/ProjectCreateOneWithoutAssessmentsInput";
import { UserCreateOneWithoutRecipientInput } from "../inputs/UserCreateOneWithoutRecipientInput";

@InputType({
  isAbstract: true,
  description: undefined,
})
export class AssessmentCreateWithoutUserInput {
  @Field(() => String, {
    nullable: true,
    description: undefined
  })
  id?: string | undefined;

  @ClassTransformer__Type(() => FormCreateManyWithoutAssessmentInput)
  @Field(() => FormCreateManyWithoutAssessmentInput, {
    nullable: true,
    description: undefined
  })
  forms?: FormCreateManyWithoutAssessmentInput | undefined;

  @ClassTransformer__Type(() => UserCreateOneWithoutRecipientInput)
  @Field(() => UserCreateOneWithoutRecipientInput, {
    nullable: false,
    description: undefined
  })
  recipient!: UserCreateOneWithoutRecipientInput;

  @ClassTransformer__Type(() => ProjectCreateOneWithoutAssessmentsInput)
  @Field(() => ProjectCreateOneWithoutAssessmentsInput, {
    nullable: false,
    description: undefined
  })
  Project!: ProjectCreateOneWithoutAssessmentsInput;
}
