import { Field, Float, ID, InputType, Int } from "@nestjs/graphql";
import GraphQLJSON from "graphql-type-json";
import { JsonValue, InputJsonValue } from "../../../../../node_modules/@prisma/client";
import { Type as ClassTransformer__Type } from "class-transformer";
import { FormCreateWithoutQuestionsInput } from "../inputs/FormCreateWithoutQuestionsInput";
import { FormUpdateWithoutQuestionsDataInput } from "../inputs/FormUpdateWithoutQuestionsDataInput";

@InputType({
  isAbstract: true,
  description: undefined,
})
export class FormUpsertWithoutQuestionsInput {
  @ClassTransformer__Type(() => FormUpdateWithoutQuestionsDataInput)
  @Field(() => FormUpdateWithoutQuestionsDataInput, {
    nullable: false,
    description: undefined
  })
  update!: FormUpdateWithoutQuestionsDataInput;

  @ClassTransformer__Type(() => FormCreateWithoutQuestionsInput)
  @Field(() => FormCreateWithoutQuestionsInput, {
    nullable: false,
    description: undefined
  })
  create!: FormCreateWithoutQuestionsInput;
}
