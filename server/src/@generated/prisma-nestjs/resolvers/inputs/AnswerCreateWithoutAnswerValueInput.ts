import { Field, Float, ID, InputType, Int } from "@nestjs/graphql";
import GraphQLJSON from "graphql-type-json";
import { JsonValue, InputJsonValue } from "../../../../../node_modules/@prisma/client";
import { Type as ClassTransformer__Type } from "class-transformer";
import { QuestionCreateOneWithoutAnswerInput } from "../inputs/QuestionCreateOneWithoutAnswerInput";
import { UserCreateOneWithoutAnswerInput } from "../inputs/UserCreateOneWithoutAnswerInput";
import { AnswerStatus } from "../../enums/AnswerStatus";

@InputType({
  isAbstract: true,
  description: undefined,
})
export class AnswerCreateWithoutAnswerValueInput {
  @Field(() => String, {
    nullable: true,
    description: undefined
  })
  id?: string | undefined;

  @Field(() => Boolean, {
    nullable: false,
    description: undefined
  })
  canSkip!: boolean;

  @Field(() => AnswerStatus, {
    nullable: false,
    description: undefined
  })
  status!: keyof typeof AnswerStatus;

  @Field(() => Boolean, {
    nullable: false,
    description: undefined
  })
  isAnonymous!: boolean;

  @Field(() => String, {
    nullable: false,
    description: undefined
  })
  answerText!: string;

  @ClassTransformer__Type(() => QuestionCreateOneWithoutAnswerInput)
  @Field(() => QuestionCreateOneWithoutAnswerInput, {
    nullable: false,
    description: undefined
  })
  question!: QuestionCreateOneWithoutAnswerInput;

  @ClassTransformer__Type(() => UserCreateOneWithoutAnswerInput)
  @Field(() => UserCreateOneWithoutAnswerInput, {
    nullable: false,
    description: undefined
  })
  user!: UserCreateOneWithoutAnswerInput;
}
