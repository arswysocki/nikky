import { Field, Float, ID, InputType, Int } from "@nestjs/graphql";
import GraphQLJSON from "graphql-type-json";
import { JsonValue, InputJsonValue } from "../../../../../node_modules/@prisma/client";
import { Type as ClassTransformer__Type } from "class-transformer";
import { QuestionCreateWithoutAnswerInput } from "../inputs/QuestionCreateWithoutAnswerInput";
import { QuestionUpdateWithoutAnswerDataInput } from "../inputs/QuestionUpdateWithoutAnswerDataInput";

@InputType({
  isAbstract: true,
  description: undefined,
})
export class QuestionUpsertWithoutAnswerInput {
  @ClassTransformer__Type(() => QuestionUpdateWithoutAnswerDataInput)
  @Field(() => QuestionUpdateWithoutAnswerDataInput, {
    nullable: false,
    description: undefined
  })
  update!: QuestionUpdateWithoutAnswerDataInput;

  @ClassTransformer__Type(() => QuestionCreateWithoutAnswerInput)
  @Field(() => QuestionCreateWithoutAnswerInput, {
    nullable: false,
    description: undefined
  })
  create!: QuestionCreateWithoutAnswerInput;
}
