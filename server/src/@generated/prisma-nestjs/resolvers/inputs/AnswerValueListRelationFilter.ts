import { Field, Float, ID, InputType, Int } from "@nestjs/graphql";
import GraphQLJSON from "graphql-type-json";
import { JsonValue, InputJsonValue } from "../../../../../node_modules/@prisma/client";
import { Type as ClassTransformer__Type } from "class-transformer";
import { AnswerValueWhereInput } from "../inputs/AnswerValueWhereInput";

@InputType({
  isAbstract: true,
  description: undefined,
})
export class AnswerValueListRelationFilter {
  @ClassTransformer__Type(() => AnswerValueWhereInput)
  @Field(() => AnswerValueWhereInput, {
    nullable: true,
    description: undefined
  })
  every?: AnswerValueWhereInput | undefined;

  @ClassTransformer__Type(() => AnswerValueWhereInput)
  @Field(() => AnswerValueWhereInput, {
    nullable: true,
    description: undefined
  })
  some?: AnswerValueWhereInput | undefined;

  @ClassTransformer__Type(() => AnswerValueWhereInput)
  @Field(() => AnswerValueWhereInput, {
    nullable: true,
    description: undefined
  })
  none?: AnswerValueWhereInput | undefined;
}
