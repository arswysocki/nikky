import { Field, Float, ID, InputType, Int } from "@nestjs/graphql";
import GraphQLJSON from "graphql-type-json";
import { JsonValue, InputJsonValue } from "../../../../../node_modules/@prisma/client";
import { Type as ClassTransformer__Type } from "class-transformer";
import { QuestionType } from "../../enums/QuestionType";

@InputType({
  isAbstract: true,
  description: undefined,
})
export class NestedEnumQuestionTypeFilter {
  @Field(() => QuestionType, {
    nullable: true,
    description: undefined
  })
  equals?: keyof typeof QuestionType | undefined;

  @Field(() => [QuestionType], {
    nullable: true,
    description: undefined
  })
  in?: Array<keyof typeof QuestionType> | undefined;

  @Field(() => [QuestionType], {
    nullable: true,
    description: undefined
  })
  notIn?: Array<keyof typeof QuestionType> | undefined;

  @ClassTransformer__Type(() => NestedEnumQuestionTypeFilter)
  @Field(() => NestedEnumQuestionTypeFilter, {
    nullable: true,
    description: undefined
  })
  not?: NestedEnumQuestionTypeFilter | undefined;
}
