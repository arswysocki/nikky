import { Field, Float, ID, InputType, Int } from "@nestjs/graphql";
import GraphQLJSON from "graphql-type-json";
import { JsonValue, InputJsonValue } from "../../../../../node_modules/@prisma/client";
import { Type as ClassTransformer__Type } from "class-transformer";
import { UserCreateWithoutRecipientInput } from "../inputs/UserCreateWithoutRecipientInput";
import { UserUpdateWithoutRecipientDataInput } from "../inputs/UserUpdateWithoutRecipientDataInput";
import { UserUpsertWithoutRecipientInput } from "../inputs/UserUpsertWithoutRecipientInput";
import { UserWhereUniqueInput } from "../inputs/UserWhereUniqueInput";

@InputType({
  isAbstract: true,
  description: undefined,
})
export class UserUpdateOneRequiredWithoutRecipientInput {
  @ClassTransformer__Type(() => UserCreateWithoutRecipientInput)
  @Field(() => UserCreateWithoutRecipientInput, {
    nullable: true,
    description: undefined
  })
  create?: UserCreateWithoutRecipientInput | undefined;

  @ClassTransformer__Type(() => UserWhereUniqueInput)
  @Field(() => UserWhereUniqueInput, {
    nullable: true,
    description: undefined
  })
  connect?: UserWhereUniqueInput | undefined;

  @ClassTransformer__Type(() => UserUpdateWithoutRecipientDataInput)
  @Field(() => UserUpdateWithoutRecipientDataInput, {
    nullable: true,
    description: undefined
  })
  update?: UserUpdateWithoutRecipientDataInput | undefined;

  @ClassTransformer__Type(() => UserUpsertWithoutRecipientInput)
  @Field(() => UserUpsertWithoutRecipientInput, {
    nullable: true,
    description: undefined
  })
  upsert?: UserUpsertWithoutRecipientInput | undefined;
}
