import { Field, Float, ID, InputType, Int } from "@nestjs/graphql";
import GraphQLJSON from "graphql-type-json";
import { JsonValue, InputJsonValue } from "../../../../../node_modules/@prisma/client";
import { Type as ClassTransformer__Type } from "class-transformer";
import { FormCreateWithoutAssessmentInput } from "../inputs/FormCreateWithoutAssessmentInput";
import { FormWhereUniqueInput } from "../inputs/FormWhereUniqueInput";

@InputType({
  isAbstract: true,
  description: undefined,
})
export class FormCreateManyWithoutAssessmentInput {
  @ClassTransformer__Type(() => FormCreateWithoutAssessmentInput)
  @Field(() => [FormCreateWithoutAssessmentInput], {
    nullable: true,
    description: undefined
  })
  create?: FormCreateWithoutAssessmentInput[] | undefined;

  @ClassTransformer__Type(() => FormWhereUniqueInput)
  @Field(() => [FormWhereUniqueInput], {
    nullable: true,
    description: undefined
  })
  connect?: FormWhereUniqueInput[] | undefined;
}
