import { Field, Float, ID, InputType, Int } from "@nestjs/graphql";
import GraphQLJSON from "graphql-type-json";
import { JsonValue, InputJsonValue } from "../../../../../node_modules/@prisma/client";
import { Type as ClassTransformer__Type } from "class-transformer";
import { QuestionWhereInput } from "../inputs/QuestionWhereInput";

@InputType({
  isAbstract: true,
  description: undefined,
})
export class QuestionListRelationFilter {
  @ClassTransformer__Type(() => QuestionWhereInput)
  @Field(() => QuestionWhereInput, {
    nullable: true,
    description: undefined
  })
  every?: QuestionWhereInput | undefined;

  @ClassTransformer__Type(() => QuestionWhereInput)
  @Field(() => QuestionWhereInput, {
    nullable: true,
    description: undefined
  })
  some?: QuestionWhereInput | undefined;

  @ClassTransformer__Type(() => QuestionWhereInput)
  @Field(() => QuestionWhereInput, {
    nullable: true,
    description: undefined
  })
  none?: QuestionWhereInput | undefined;
}
