import { Field, Float, ID, InputType, Int } from "@nestjs/graphql";
import GraphQLJSON from "graphql-type-json";
import { JsonValue, InputJsonValue } from "../../../../../node_modules/@prisma/client";
import { Type as ClassTransformer__Type } from "class-transformer";
import { AnswerListRelationFilter } from "../inputs/AnswerListRelationFilter";
import { AnswerValueListRelationFilter } from "../inputs/AnswerValueListRelationFilter";
import { EnumQuestionTypeFilter } from "../inputs/EnumQuestionTypeFilter";
import { FormWhereInput } from "../inputs/FormWhereInput";
import { StringFilter } from "../inputs/StringFilter";

@InputType({
  isAbstract: true,
  description: undefined,
})
export class QuestionWhereInput {
  @ClassTransformer__Type(() => QuestionWhereInput)
  @Field(() => [QuestionWhereInput], {
    nullable: true,
    description: undefined
  })
  AND?: QuestionWhereInput[] | undefined;

  @ClassTransformer__Type(() => QuestionWhereInput)
  @Field(() => [QuestionWhereInput], {
    nullable: true,
    description: undefined
  })
  OR?: QuestionWhereInput[] | undefined;

  @ClassTransformer__Type(() => QuestionWhereInput)
  @Field(() => [QuestionWhereInput], {
    nullable: true,
    description: undefined
  })
  NOT?: QuestionWhereInput[] | undefined;

  @ClassTransformer__Type(() => StringFilter)
  @Field(() => StringFilter, {
    nullable: true,
    description: undefined
  })
  id?: StringFilter | undefined;

  @ClassTransformer__Type(() => FormWhereInput)
  @Field(() => FormWhereInput, {
    nullable: true,
    description: undefined
  })
  Form?: FormWhereInput | undefined;

  @ClassTransformer__Type(() => EnumQuestionTypeFilter)
  @Field(() => EnumQuestionTypeFilter, {
    nullable: true,
    description: undefined
  })
  type?: EnumQuestionTypeFilter | undefined;

  @ClassTransformer__Type(() => AnswerValueListRelationFilter)
  @Field(() => AnswerValueListRelationFilter, {
    nullable: true,
    description: undefined
  })
  answerValues?: AnswerValueListRelationFilter | undefined;

  @ClassTransformer__Type(() => StringFilter)
  @Field(() => StringFilter, {
    nullable: true,
    description: undefined
  })
  formId?: StringFilter | undefined;

  @ClassTransformer__Type(() => AnswerListRelationFilter)
  @Field(() => AnswerListRelationFilter, {
    nullable: true,
    description: undefined
  })
  answer?: AnswerListRelationFilter | undefined;
}
