import { Field, Float, ID, InputType, Int } from "@nestjs/graphql";
import GraphQLJSON from "graphql-type-json";
import { JsonValue, InputJsonValue } from "../../../../../node_modules/@prisma/client";
import { Type as ClassTransformer__Type } from "class-transformer";
import { QuestionUpdateWithoutFormDataInput } from "../inputs/QuestionUpdateWithoutFormDataInput";
import { QuestionWhereUniqueInput } from "../inputs/QuestionWhereUniqueInput";

@InputType({
  isAbstract: true,
  description: undefined,
})
export class QuestionUpdateWithWhereUniqueWithoutFormInput {
  @ClassTransformer__Type(() => QuestionWhereUniqueInput)
  @Field(() => QuestionWhereUniqueInput, {
    nullable: false,
    description: undefined
  })
  where!: QuestionWhereUniqueInput;

  @ClassTransformer__Type(() => QuestionUpdateWithoutFormDataInput)
  @Field(() => QuestionUpdateWithoutFormDataInput, {
    nullable: false,
    description: undefined
  })
  data!: QuestionUpdateWithoutFormDataInput;
}
