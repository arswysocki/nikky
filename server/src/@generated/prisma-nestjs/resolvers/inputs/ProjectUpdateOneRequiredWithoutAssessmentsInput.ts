import { Field, Float, ID, InputType, Int } from "@nestjs/graphql";
import GraphQLJSON from "graphql-type-json";
import { JsonValue, InputJsonValue } from "../../../../../node_modules/@prisma/client";
import { Type as ClassTransformer__Type } from "class-transformer";
import { ProjectCreateWithoutAssessmentsInput } from "../inputs/ProjectCreateWithoutAssessmentsInput";
import { ProjectUpdateWithoutAssessmentsDataInput } from "../inputs/ProjectUpdateWithoutAssessmentsDataInput";
import { ProjectUpsertWithoutAssessmentsInput } from "../inputs/ProjectUpsertWithoutAssessmentsInput";
import { ProjectWhereUniqueInput } from "../inputs/ProjectWhereUniqueInput";

@InputType({
  isAbstract: true,
  description: undefined,
})
export class ProjectUpdateOneRequiredWithoutAssessmentsInput {
  @ClassTransformer__Type(() => ProjectCreateWithoutAssessmentsInput)
  @Field(() => ProjectCreateWithoutAssessmentsInput, {
    nullable: true,
    description: undefined
  })
  create?: ProjectCreateWithoutAssessmentsInput | undefined;

  @ClassTransformer__Type(() => ProjectWhereUniqueInput)
  @Field(() => ProjectWhereUniqueInput, {
    nullable: true,
    description: undefined
  })
  connect?: ProjectWhereUniqueInput | undefined;

  @ClassTransformer__Type(() => ProjectUpdateWithoutAssessmentsDataInput)
  @Field(() => ProjectUpdateWithoutAssessmentsDataInput, {
    nullable: true,
    description: undefined
  })
  update?: ProjectUpdateWithoutAssessmentsDataInput | undefined;

  @ClassTransformer__Type(() => ProjectUpsertWithoutAssessmentsInput)
  @Field(() => ProjectUpsertWithoutAssessmentsInput, {
    nullable: true,
    description: undefined
  })
  upsert?: ProjectUpsertWithoutAssessmentsInput | undefined;
}
