import { Field, Float, ID, InputType, Int } from "@nestjs/graphql";
import GraphQLJSON from "graphql-type-json";
import { JsonValue, InputJsonValue } from "../../../../../node_modules/@prisma/client";
import { Type as ClassTransformer__Type } from "class-transformer";
import { AssessmentCreateWithoutRecipientInput } from "../inputs/AssessmentCreateWithoutRecipientInput";
import { AssessmentUpdateWithoutRecipientDataInput } from "../inputs/AssessmentUpdateWithoutRecipientDataInput";
import { AssessmentWhereUniqueInput } from "../inputs/AssessmentWhereUniqueInput";

@InputType({
  isAbstract: true,
  description: undefined,
})
export class AssessmentUpsertWithWhereUniqueWithoutRecipientInput {
  @ClassTransformer__Type(() => AssessmentWhereUniqueInput)
  @Field(() => AssessmentWhereUniqueInput, {
    nullable: false,
    description: undefined
  })
  where!: AssessmentWhereUniqueInput;

  @ClassTransformer__Type(() => AssessmentUpdateWithoutRecipientDataInput)
  @Field(() => AssessmentUpdateWithoutRecipientDataInput, {
    nullable: false,
    description: undefined
  })
  update!: AssessmentUpdateWithoutRecipientDataInput;

  @ClassTransformer__Type(() => AssessmentCreateWithoutRecipientInput)
  @Field(() => AssessmentCreateWithoutRecipientInput, {
    nullable: false,
    description: undefined
  })
  create!: AssessmentCreateWithoutRecipientInput;
}
