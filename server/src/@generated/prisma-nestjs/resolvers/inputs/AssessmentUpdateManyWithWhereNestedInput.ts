import { Field, Float, ID, InputType, Int } from "@nestjs/graphql";
import GraphQLJSON from "graphql-type-json";
import { JsonValue, InputJsonValue } from "../../../../../node_modules/@prisma/client";
import { Type as ClassTransformer__Type } from "class-transformer";
import { AssessmentScalarWhereInput } from "../inputs/AssessmentScalarWhereInput";
import { AssessmentUpdateManyDataInput } from "../inputs/AssessmentUpdateManyDataInput";

@InputType({
  isAbstract: true,
  description: undefined,
})
export class AssessmentUpdateManyWithWhereNestedInput {
  @ClassTransformer__Type(() => AssessmentScalarWhereInput)
  @Field(() => AssessmentScalarWhereInput, {
    nullable: false,
    description: undefined
  })
  where!: AssessmentScalarWhereInput;

  @ClassTransformer__Type(() => AssessmentUpdateManyDataInput)
  @Field(() => AssessmentUpdateManyDataInput, {
    nullable: false,
    description: undefined
  })
  data!: AssessmentUpdateManyDataInput;
}
