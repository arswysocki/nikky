import { Field, Float, ID, InputType, Int } from "@nestjs/graphql";
import GraphQLJSON from "graphql-type-json";
import { JsonValue, InputJsonValue } from "../../../../../node_modules/@prisma/client";
import { Type as ClassTransformer__Type } from "class-transformer";
import { FormCreateWithoutAssessmentInput } from "../inputs/FormCreateWithoutAssessmentInput";
import { FormScalarWhereInput } from "../inputs/FormScalarWhereInput";
import { FormUpdateManyWithWhereNestedInput } from "../inputs/FormUpdateManyWithWhereNestedInput";
import { FormUpdateWithWhereUniqueWithoutAssessmentInput } from "../inputs/FormUpdateWithWhereUniqueWithoutAssessmentInput";
import { FormUpsertWithWhereUniqueWithoutAssessmentInput } from "../inputs/FormUpsertWithWhereUniqueWithoutAssessmentInput";
import { FormWhereUniqueInput } from "../inputs/FormWhereUniqueInput";

@InputType({
  isAbstract: true,
  description: undefined,
})
export class FormUpdateManyWithoutAssessmentInput {
  @ClassTransformer__Type(() => FormCreateWithoutAssessmentInput)
  @Field(() => [FormCreateWithoutAssessmentInput], {
    nullable: true,
    description: undefined
  })
  create?: FormCreateWithoutAssessmentInput[] | undefined;

  @ClassTransformer__Type(() => FormWhereUniqueInput)
  @Field(() => [FormWhereUniqueInput], {
    nullable: true,
    description: undefined
  })
  connect?: FormWhereUniqueInput[] | undefined;

  @ClassTransformer__Type(() => FormWhereUniqueInput)
  @Field(() => [FormWhereUniqueInput], {
    nullable: true,
    description: undefined
  })
  set?: FormWhereUniqueInput[] | undefined;

  @ClassTransformer__Type(() => FormWhereUniqueInput)
  @Field(() => [FormWhereUniqueInput], {
    nullable: true,
    description: undefined
  })
  disconnect?: FormWhereUniqueInput[] | undefined;

  @ClassTransformer__Type(() => FormWhereUniqueInput)
  @Field(() => [FormWhereUniqueInput], {
    nullable: true,
    description: undefined
  })
  delete?: FormWhereUniqueInput[] | undefined;

  @ClassTransformer__Type(() => FormUpdateWithWhereUniqueWithoutAssessmentInput)
  @Field(() => [FormUpdateWithWhereUniqueWithoutAssessmentInput], {
    nullable: true,
    description: undefined
  })
  update?: FormUpdateWithWhereUniqueWithoutAssessmentInput[] | undefined;

  @ClassTransformer__Type(() => FormUpdateManyWithWhereNestedInput)
  @Field(() => [FormUpdateManyWithWhereNestedInput], {
    nullable: true,
    description: undefined
  })
  updateMany?: FormUpdateManyWithWhereNestedInput[] | undefined;

  @ClassTransformer__Type(() => FormScalarWhereInput)
  @Field(() => [FormScalarWhereInput], {
    nullable: true,
    description: undefined
  })
  deleteMany?: FormScalarWhereInput[] | undefined;

  @ClassTransformer__Type(() => FormUpsertWithWhereUniqueWithoutAssessmentInput)
  @Field(() => [FormUpsertWithWhereUniqueWithoutAssessmentInput], {
    nullable: true,
    description: undefined
  })
  upsert?: FormUpsertWithWhereUniqueWithoutAssessmentInput[] | undefined;
}
