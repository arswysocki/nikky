import { Field, Float, ID, InputType, Int } from "@nestjs/graphql";
import GraphQLJSON from "graphql-type-json";
import { JsonValue, InputJsonValue } from "../../../../../node_modules/@prisma/client";
import { Type as ClassTransformer__Type } from "class-transformer";
import { AnswerValueCreateWithoutQuestionInput } from "../inputs/AnswerValueCreateWithoutQuestionInput";
import { AnswerValueWhereUniqueInput } from "../inputs/AnswerValueWhereUniqueInput";

@InputType({
  isAbstract: true,
  description: undefined,
})
export class AnswerValueCreateManyWithoutQuestionInput {
  @ClassTransformer__Type(() => AnswerValueCreateWithoutQuestionInput)
  @Field(() => [AnswerValueCreateWithoutQuestionInput], {
    nullable: true,
    description: undefined
  })
  create?: AnswerValueCreateWithoutQuestionInput[] | undefined;

  @ClassTransformer__Type(() => AnswerValueWhereUniqueInput)
  @Field(() => [AnswerValueWhereUniqueInput], {
    nullable: true,
    description: undefined
  })
  connect?: AnswerValueWhereUniqueInput[] | undefined;
}
