import { Field, Float, ID, InputType, Int } from "@nestjs/graphql";
import GraphQLJSON from "graphql-type-json";
import { JsonValue, InputJsonValue } from "../../../../../node_modules/@prisma/client";
import { Type as ClassTransformer__Type } from "class-transformer";
import { FormScalarWhereInput } from "../inputs/FormScalarWhereInput";
import { FormUpdateManyDataInput } from "../inputs/FormUpdateManyDataInput";

@InputType({
  isAbstract: true,
  description: undefined,
})
export class FormUpdateManyWithWhereNestedInput {
  @ClassTransformer__Type(() => FormScalarWhereInput)
  @Field(() => FormScalarWhereInput, {
    nullable: false,
    description: undefined
  })
  where!: FormScalarWhereInput;

  @ClassTransformer__Type(() => FormUpdateManyDataInput)
  @Field(() => FormUpdateManyDataInput, {
    nullable: false,
    description: undefined
  })
  data!: FormUpdateManyDataInput;
}
