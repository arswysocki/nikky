import { Field, Float, ID, InputType, Int } from "@nestjs/graphql";
import GraphQLJSON from "graphql-type-json";
import { JsonValue, InputJsonValue } from "../../../../../node_modules/@prisma/client";
import { Type as ClassTransformer__Type } from "class-transformer";
import { AssessmentCreateWithoutProjectInput } from "../inputs/AssessmentCreateWithoutProjectInput";
import { AssessmentWhereUniqueInput } from "../inputs/AssessmentWhereUniqueInput";

@InputType({
  isAbstract: true,
  description: undefined,
})
export class AssessmentCreateManyWithoutProjectInput {
  @ClassTransformer__Type(() => AssessmentCreateWithoutProjectInput)
  @Field(() => [AssessmentCreateWithoutProjectInput], {
    nullable: true,
    description: undefined
  })
  create?: AssessmentCreateWithoutProjectInput[] | undefined;

  @ClassTransformer__Type(() => AssessmentWhereUniqueInput)
  @Field(() => [AssessmentWhereUniqueInput], {
    nullable: true,
    description: undefined
  })
  connect?: AssessmentWhereUniqueInput[] | undefined;
}
