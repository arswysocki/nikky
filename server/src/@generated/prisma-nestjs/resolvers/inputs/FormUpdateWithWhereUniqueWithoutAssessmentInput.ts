import { Field, Float, ID, InputType, Int } from "@nestjs/graphql";
import GraphQLJSON from "graphql-type-json";
import { JsonValue, InputJsonValue } from "../../../../../node_modules/@prisma/client";
import { Type as ClassTransformer__Type } from "class-transformer";
import { FormUpdateWithoutAssessmentDataInput } from "../inputs/FormUpdateWithoutAssessmentDataInput";
import { FormWhereUniqueInput } from "../inputs/FormWhereUniqueInput";

@InputType({
  isAbstract: true,
  description: undefined,
})
export class FormUpdateWithWhereUniqueWithoutAssessmentInput {
  @ClassTransformer__Type(() => FormWhereUniqueInput)
  @Field(() => FormWhereUniqueInput, {
    nullable: false,
    description: undefined
  })
  where!: FormWhereUniqueInput;

  @ClassTransformer__Type(() => FormUpdateWithoutAssessmentDataInput)
  @Field(() => FormUpdateWithoutAssessmentDataInput, {
    nullable: false,
    description: undefined
  })
  data!: FormUpdateWithoutAssessmentDataInput;
}
