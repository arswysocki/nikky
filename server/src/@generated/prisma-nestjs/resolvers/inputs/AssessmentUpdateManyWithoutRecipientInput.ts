import { Field, Float, ID, InputType, Int } from "@nestjs/graphql";
import GraphQLJSON from "graphql-type-json";
import { JsonValue, InputJsonValue } from "../../../../../node_modules/@prisma/client";
import { Type as ClassTransformer__Type } from "class-transformer";
import { AssessmentCreateWithoutRecipientInput } from "../inputs/AssessmentCreateWithoutRecipientInput";
import { AssessmentScalarWhereInput } from "../inputs/AssessmentScalarWhereInput";
import { AssessmentUpdateManyWithWhereNestedInput } from "../inputs/AssessmentUpdateManyWithWhereNestedInput";
import { AssessmentUpdateWithWhereUniqueWithoutRecipientInput } from "../inputs/AssessmentUpdateWithWhereUniqueWithoutRecipientInput";
import { AssessmentUpsertWithWhereUniqueWithoutRecipientInput } from "../inputs/AssessmentUpsertWithWhereUniqueWithoutRecipientInput";
import { AssessmentWhereUniqueInput } from "../inputs/AssessmentWhereUniqueInput";

@InputType({
  isAbstract: true,
  description: undefined,
})
export class AssessmentUpdateManyWithoutRecipientInput {
  @ClassTransformer__Type(() => AssessmentCreateWithoutRecipientInput)
  @Field(() => [AssessmentCreateWithoutRecipientInput], {
    nullable: true,
    description: undefined
  })
  create?: AssessmentCreateWithoutRecipientInput[] | undefined;

  @ClassTransformer__Type(() => AssessmentWhereUniqueInput)
  @Field(() => [AssessmentWhereUniqueInput], {
    nullable: true,
    description: undefined
  })
  connect?: AssessmentWhereUniqueInput[] | undefined;

  @ClassTransformer__Type(() => AssessmentWhereUniqueInput)
  @Field(() => [AssessmentWhereUniqueInput], {
    nullable: true,
    description: undefined
  })
  set?: AssessmentWhereUniqueInput[] | undefined;

  @ClassTransformer__Type(() => AssessmentWhereUniqueInput)
  @Field(() => [AssessmentWhereUniqueInput], {
    nullable: true,
    description: undefined
  })
  disconnect?: AssessmentWhereUniqueInput[] | undefined;

  @ClassTransformer__Type(() => AssessmentWhereUniqueInput)
  @Field(() => [AssessmentWhereUniqueInput], {
    nullable: true,
    description: undefined
  })
  delete?: AssessmentWhereUniqueInput[] | undefined;

  @ClassTransformer__Type(() => AssessmentUpdateWithWhereUniqueWithoutRecipientInput)
  @Field(() => [AssessmentUpdateWithWhereUniqueWithoutRecipientInput], {
    nullable: true,
    description: undefined
  })
  update?: AssessmentUpdateWithWhereUniqueWithoutRecipientInput[] | undefined;

  @ClassTransformer__Type(() => AssessmentUpdateManyWithWhereNestedInput)
  @Field(() => [AssessmentUpdateManyWithWhereNestedInput], {
    nullable: true,
    description: undefined
  })
  updateMany?: AssessmentUpdateManyWithWhereNestedInput[] | undefined;

  @ClassTransformer__Type(() => AssessmentScalarWhereInput)
  @Field(() => [AssessmentScalarWhereInput], {
    nullable: true,
    description: undefined
  })
  deleteMany?: AssessmentScalarWhereInput[] | undefined;

  @ClassTransformer__Type(() => AssessmentUpsertWithWhereUniqueWithoutRecipientInput)
  @Field(() => [AssessmentUpsertWithWhereUniqueWithoutRecipientInput], {
    nullable: true,
    description: undefined
  })
  upsert?: AssessmentUpsertWithWhereUniqueWithoutRecipientInput[] | undefined;
}
