import { Field, Float, ID, InputType, Int } from "@nestjs/graphql";
import GraphQLJSON from "graphql-type-json";
import { JsonValue, InputJsonValue } from "../../../../../node_modules/@prisma/client";
import { Type as ClassTransformer__Type } from "class-transformer";
import { StringFilter } from "../inputs/StringFilter";

@InputType({
  isAbstract: true,
  description: undefined,
})
export class AssessmentScalarWhereInput {
  @ClassTransformer__Type(() => AssessmentScalarWhereInput)
  @Field(() => [AssessmentScalarWhereInput], {
    nullable: true,
    description: undefined
  })
  AND?: AssessmentScalarWhereInput[] | undefined;

  @ClassTransformer__Type(() => AssessmentScalarWhereInput)
  @Field(() => [AssessmentScalarWhereInput], {
    nullable: true,
    description: undefined
  })
  OR?: AssessmentScalarWhereInput[] | undefined;

  @ClassTransformer__Type(() => AssessmentScalarWhereInput)
  @Field(() => [AssessmentScalarWhereInput], {
    nullable: true,
    description: undefined
  })
  NOT?: AssessmentScalarWhereInput[] | undefined;

  @ClassTransformer__Type(() => StringFilter)
  @Field(() => StringFilter, {
    nullable: true,
    description: undefined
  })
  id?: StringFilter | undefined;

  @ClassTransformer__Type(() => StringFilter)
  @Field(() => StringFilter, {
    nullable: true,
    description: undefined
  })
  userId?: StringFilter | undefined;

  @ClassTransformer__Type(() => StringFilter)
  @Field(() => StringFilter, {
    nullable: true,
    description: undefined
  })
  recipientId?: StringFilter | undefined;

  @ClassTransformer__Type(() => StringFilter)
  @Field(() => StringFilter, {
    nullable: true,
    description: undefined
  })
  projectId?: StringFilter | undefined;
}
