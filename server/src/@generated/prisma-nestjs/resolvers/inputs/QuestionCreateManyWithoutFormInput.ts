import { Field, Float, ID, InputType, Int } from "@nestjs/graphql";
import GraphQLJSON from "graphql-type-json";
import { JsonValue, InputJsonValue } from "../../../../../node_modules/@prisma/client";
import { Type as ClassTransformer__Type } from "class-transformer";
import { QuestionCreateWithoutFormInput } from "../inputs/QuestionCreateWithoutFormInput";
import { QuestionWhereUniqueInput } from "../inputs/QuestionWhereUniqueInput";

@InputType({
  isAbstract: true,
  description: undefined,
})
export class QuestionCreateManyWithoutFormInput {
  @ClassTransformer__Type(() => QuestionCreateWithoutFormInput)
  @Field(() => [QuestionCreateWithoutFormInput], {
    nullable: true,
    description: undefined
  })
  create?: QuestionCreateWithoutFormInput[] | undefined;

  @ClassTransformer__Type(() => QuestionWhereUniqueInput)
  @Field(() => [QuestionWhereUniqueInput], {
    nullable: true,
    description: undefined
  })
  connect?: QuestionWhereUniqueInput[] | undefined;
}
