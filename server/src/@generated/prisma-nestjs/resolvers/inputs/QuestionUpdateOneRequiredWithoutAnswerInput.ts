import { Field, Float, ID, InputType, Int } from "@nestjs/graphql";
import GraphQLJSON from "graphql-type-json";
import { JsonValue, InputJsonValue } from "../../../../../node_modules/@prisma/client";
import { Type as ClassTransformer__Type } from "class-transformer";
import { QuestionCreateWithoutAnswerInput } from "../inputs/QuestionCreateWithoutAnswerInput";
import { QuestionUpdateWithoutAnswerDataInput } from "../inputs/QuestionUpdateWithoutAnswerDataInput";
import { QuestionUpsertWithoutAnswerInput } from "../inputs/QuestionUpsertWithoutAnswerInput";
import { QuestionWhereUniqueInput } from "../inputs/QuestionWhereUniqueInput";

@InputType({
  isAbstract: true,
  description: undefined,
})
export class QuestionUpdateOneRequiredWithoutAnswerInput {
  @ClassTransformer__Type(() => QuestionCreateWithoutAnswerInput)
  @Field(() => QuestionCreateWithoutAnswerInput, {
    nullable: true,
    description: undefined
  })
  create?: QuestionCreateWithoutAnswerInput | undefined;

  @ClassTransformer__Type(() => QuestionWhereUniqueInput)
  @Field(() => QuestionWhereUniqueInput, {
    nullable: true,
    description: undefined
  })
  connect?: QuestionWhereUniqueInput | undefined;

  @ClassTransformer__Type(() => QuestionUpdateWithoutAnswerDataInput)
  @Field(() => QuestionUpdateWithoutAnswerDataInput, {
    nullable: true,
    description: undefined
  })
  update?: QuestionUpdateWithoutAnswerDataInput | undefined;

  @ClassTransformer__Type(() => QuestionUpsertWithoutAnswerInput)
  @Field(() => QuestionUpsertWithoutAnswerInput, {
    nullable: true,
    description: undefined
  })
  upsert?: QuestionUpsertWithoutAnswerInput | undefined;
}
