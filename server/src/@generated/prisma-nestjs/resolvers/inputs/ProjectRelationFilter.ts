import { Field, Float, ID, InputType, Int } from "@nestjs/graphql";
import GraphQLJSON from "graphql-type-json";
import { JsonValue, InputJsonValue } from "../../../../../node_modules/@prisma/client";
import { Type as ClassTransformer__Type } from "class-transformer";
import { ProjectWhereInput } from "../inputs/ProjectWhereInput";

@InputType({
  isAbstract: true,
  description: undefined,
})
export class ProjectRelationFilter {
  @ClassTransformer__Type(() => ProjectWhereInput)
  @Field(() => ProjectWhereInput, {
    nullable: true,
    description: undefined
  })
  is?: ProjectWhereInput | undefined;

  @ClassTransformer__Type(() => ProjectWhereInput)
  @Field(() => ProjectWhereInput, {
    nullable: true,
    description: undefined
  })
  isNot?: ProjectWhereInput | undefined;
}
