import { Field, Float, ID, InputType, Int } from "@nestjs/graphql";
import GraphQLJSON from "graphql-type-json";
import { JsonValue, InputJsonValue } from "../../../../../node_modules/@prisma/client";
import { Type as ClassTransformer__Type } from "class-transformer";
import { AssessmentWhereInput } from "../inputs/AssessmentWhereInput";

@InputType({
  isAbstract: true,
  description: undefined,
})
export class AssessmentRelationFilter {
  @ClassTransformer__Type(() => AssessmentWhereInput)
  @Field(() => AssessmentWhereInput, {
    nullable: true,
    description: undefined
  })
  is?: AssessmentWhereInput | undefined;

  @ClassTransformer__Type(() => AssessmentWhereInput)
  @Field(() => AssessmentWhereInput, {
    nullable: true,
    description: undefined
  })
  isNot?: AssessmentWhereInput | undefined;
}
