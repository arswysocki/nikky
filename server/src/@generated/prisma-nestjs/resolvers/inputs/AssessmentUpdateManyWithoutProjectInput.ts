import { Field, Float, ID, InputType, Int } from "@nestjs/graphql";
import GraphQLJSON from "graphql-type-json";
import { JsonValue, InputJsonValue } from "../../../../../node_modules/@prisma/client";
import { Type as ClassTransformer__Type } from "class-transformer";
import { AssessmentCreateWithoutProjectInput } from "../inputs/AssessmentCreateWithoutProjectInput";
import { AssessmentScalarWhereInput } from "../inputs/AssessmentScalarWhereInput";
import { AssessmentUpdateManyWithWhereNestedInput } from "../inputs/AssessmentUpdateManyWithWhereNestedInput";
import { AssessmentUpdateWithWhereUniqueWithoutProjectInput } from "../inputs/AssessmentUpdateWithWhereUniqueWithoutProjectInput";
import { AssessmentUpsertWithWhereUniqueWithoutProjectInput } from "../inputs/AssessmentUpsertWithWhereUniqueWithoutProjectInput";
import { AssessmentWhereUniqueInput } from "../inputs/AssessmentWhereUniqueInput";

@InputType({
  isAbstract: true,
  description: undefined,
})
export class AssessmentUpdateManyWithoutProjectInput {
  @ClassTransformer__Type(() => AssessmentCreateWithoutProjectInput)
  @Field(() => [AssessmentCreateWithoutProjectInput], {
    nullable: true,
    description: undefined
  })
  create?: AssessmentCreateWithoutProjectInput[] | undefined;

  @ClassTransformer__Type(() => AssessmentWhereUniqueInput)
  @Field(() => [AssessmentWhereUniqueInput], {
    nullable: true,
    description: undefined
  })
  connect?: AssessmentWhereUniqueInput[] | undefined;

  @ClassTransformer__Type(() => AssessmentWhereUniqueInput)
  @Field(() => [AssessmentWhereUniqueInput], {
    nullable: true,
    description: undefined
  })
  set?: AssessmentWhereUniqueInput[] | undefined;

  @ClassTransformer__Type(() => AssessmentWhereUniqueInput)
  @Field(() => [AssessmentWhereUniqueInput], {
    nullable: true,
    description: undefined
  })
  disconnect?: AssessmentWhereUniqueInput[] | undefined;

  @ClassTransformer__Type(() => AssessmentWhereUniqueInput)
  @Field(() => [AssessmentWhereUniqueInput], {
    nullable: true,
    description: undefined
  })
  delete?: AssessmentWhereUniqueInput[] | undefined;

  @ClassTransformer__Type(() => AssessmentUpdateWithWhereUniqueWithoutProjectInput)
  @Field(() => [AssessmentUpdateWithWhereUniqueWithoutProjectInput], {
    nullable: true,
    description: undefined
  })
  update?: AssessmentUpdateWithWhereUniqueWithoutProjectInput[] | undefined;

  @ClassTransformer__Type(() => AssessmentUpdateManyWithWhereNestedInput)
  @Field(() => [AssessmentUpdateManyWithWhereNestedInput], {
    nullable: true,
    description: undefined
  })
  updateMany?: AssessmentUpdateManyWithWhereNestedInput[] | undefined;

  @ClassTransformer__Type(() => AssessmentScalarWhereInput)
  @Field(() => [AssessmentScalarWhereInput], {
    nullable: true,
    description: undefined
  })
  deleteMany?: AssessmentScalarWhereInput[] | undefined;

  @ClassTransformer__Type(() => AssessmentUpsertWithWhereUniqueWithoutProjectInput)
  @Field(() => [AssessmentUpsertWithWhereUniqueWithoutProjectInput], {
    nullable: true,
    description: undefined
  })
  upsert?: AssessmentUpsertWithWhereUniqueWithoutProjectInput[] | undefined;
}
