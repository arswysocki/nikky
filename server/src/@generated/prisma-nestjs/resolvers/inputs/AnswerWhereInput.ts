import { Field, Float, ID, InputType, Int } from "@nestjs/graphql";
import GraphQLJSON from "graphql-type-json";
import { JsonValue, InputJsonValue } from "../../../../../node_modules/@prisma/client";
import { Type as ClassTransformer__Type } from "class-transformer";
import { AnswerValueWhereInput } from "../inputs/AnswerValueWhereInput";
import { BoolFilter } from "../inputs/BoolFilter";
import { EnumAnswerStatusFilter } from "../inputs/EnumAnswerStatusFilter";
import { QuestionWhereInput } from "../inputs/QuestionWhereInput";
import { StringFilter } from "../inputs/StringFilter";
import { UserWhereInput } from "../inputs/UserWhereInput";

@InputType({
  isAbstract: true,
  description: undefined,
})
export class AnswerWhereInput {
  @ClassTransformer__Type(() => AnswerWhereInput)
  @Field(() => [AnswerWhereInput], {
    nullable: true,
    description: undefined
  })
  AND?: AnswerWhereInput[] | undefined;

  @ClassTransformer__Type(() => AnswerWhereInput)
  @Field(() => [AnswerWhereInput], {
    nullable: true,
    description: undefined
  })
  OR?: AnswerWhereInput[] | undefined;

  @ClassTransformer__Type(() => AnswerWhereInput)
  @Field(() => [AnswerWhereInput], {
    nullable: true,
    description: undefined
  })
  NOT?: AnswerWhereInput[] | undefined;

  @ClassTransformer__Type(() => StringFilter)
  @Field(() => StringFilter, {
    nullable: true,
    description: undefined
  })
  id?: StringFilter | undefined;

  @ClassTransformer__Type(() => QuestionWhereInput)
  @Field(() => QuestionWhereInput, {
    nullable: true,
    description: undefined
  })
  question?: QuestionWhereInput | undefined;

  @ClassTransformer__Type(() => BoolFilter)
  @Field(() => BoolFilter, {
    nullable: true,
    description: undefined
  })
  canSkip?: BoolFilter | undefined;

  @ClassTransformer__Type(() => UserWhereInput)
  @Field(() => UserWhereInput, {
    nullable: true,
    description: undefined
  })
  user?: UserWhereInput | undefined;

  @ClassTransformer__Type(() => StringFilter)
  @Field(() => StringFilter, {
    nullable: true,
    description: undefined
  })
  userId?: StringFilter | undefined;

  @ClassTransformer__Type(() => EnumAnswerStatusFilter)
  @Field(() => EnumAnswerStatusFilter, {
    nullable: true,
    description: undefined
  })
  status?: EnumAnswerStatusFilter | undefined;

  @ClassTransformer__Type(() => BoolFilter)
  @Field(() => BoolFilter, {
    nullable: true,
    description: undefined
  })
  isAnonymous?: BoolFilter | undefined;

  @ClassTransformer__Type(() => AnswerValueWhereInput)
  @Field(() => AnswerValueWhereInput, {
    nullable: true,
    description: undefined
  })
  answerValue?: AnswerValueWhereInput | undefined;

  @ClassTransformer__Type(() => StringFilter)
  @Field(() => StringFilter, {
    nullable: true,
    description: undefined
  })
  answerText?: StringFilter | undefined;

  @ClassTransformer__Type(() => StringFilter)
  @Field(() => StringFilter, {
    nullable: true,
    description: undefined
  })
  questionId?: StringFilter | undefined;

  @ClassTransformer__Type(() => StringFilter)
  @Field(() => StringFilter, {
    nullable: true,
    description: undefined
  })
  answerValueId?: StringFilter | undefined;
}
