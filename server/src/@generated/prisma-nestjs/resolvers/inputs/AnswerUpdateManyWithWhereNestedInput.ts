import { Field, Float, ID, InputType, Int } from "@nestjs/graphql";
import GraphQLJSON from "graphql-type-json";
import { JsonValue, InputJsonValue } from "../../../../../node_modules/@prisma/client";
import { Type as ClassTransformer__Type } from "class-transformer";
import { AnswerScalarWhereInput } from "../inputs/AnswerScalarWhereInput";
import { AnswerUpdateManyDataInput } from "../inputs/AnswerUpdateManyDataInput";

@InputType({
  isAbstract: true,
  description: undefined,
})
export class AnswerUpdateManyWithWhereNestedInput {
  @ClassTransformer__Type(() => AnswerScalarWhereInput)
  @Field(() => AnswerScalarWhereInput, {
    nullable: false,
    description: undefined
  })
  where!: AnswerScalarWhereInput;

  @ClassTransformer__Type(() => AnswerUpdateManyDataInput)
  @Field(() => AnswerUpdateManyDataInput, {
    nullable: false,
    description: undefined
  })
  data!: AnswerUpdateManyDataInput;
}
