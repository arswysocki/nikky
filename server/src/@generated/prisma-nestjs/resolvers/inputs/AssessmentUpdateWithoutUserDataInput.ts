import { Field, Float, ID, InputType, Int } from "@nestjs/graphql";
import GraphQLJSON from "graphql-type-json";
import { JsonValue, InputJsonValue } from "../../../../../node_modules/@prisma/client";
import { Type as ClassTransformer__Type } from "class-transformer";
import { FormUpdateManyWithoutAssessmentInput } from "../inputs/FormUpdateManyWithoutAssessmentInput";
import { ProjectUpdateOneRequiredWithoutAssessmentsInput } from "../inputs/ProjectUpdateOneRequiredWithoutAssessmentsInput";
import { StringFieldUpdateOperationsInput } from "../inputs/StringFieldUpdateOperationsInput";
import { UserUpdateOneRequiredWithoutRecipientInput } from "../inputs/UserUpdateOneRequiredWithoutRecipientInput";

@InputType({
  isAbstract: true,
  description: undefined,
})
export class AssessmentUpdateWithoutUserDataInput {
  @ClassTransformer__Type(() => StringFieldUpdateOperationsInput)
  @Field(() => StringFieldUpdateOperationsInput, {
    nullable: true,
    description: undefined
  })
  id?: StringFieldUpdateOperationsInput | undefined;

  @ClassTransformer__Type(() => FormUpdateManyWithoutAssessmentInput)
  @Field(() => FormUpdateManyWithoutAssessmentInput, {
    nullable: true,
    description: undefined
  })
  forms?: FormUpdateManyWithoutAssessmentInput | undefined;

  @ClassTransformer__Type(() => UserUpdateOneRequiredWithoutRecipientInput)
  @Field(() => UserUpdateOneRequiredWithoutRecipientInput, {
    nullable: true,
    description: undefined
  })
  recipient?: UserUpdateOneRequiredWithoutRecipientInput | undefined;

  @ClassTransformer__Type(() => ProjectUpdateOneRequiredWithoutAssessmentsInput)
  @Field(() => ProjectUpdateOneRequiredWithoutAssessmentsInput, {
    nullable: true,
    description: undefined
  })
  Project?: ProjectUpdateOneRequiredWithoutAssessmentsInput | undefined;
}
