import { Field, Float, ID, InputType, Int } from "@nestjs/graphql";
import GraphQLJSON from "graphql-type-json";
import { JsonValue, InputJsonValue } from "../../../../../node_modules/@prisma/client";
import { Type as ClassTransformer__Type } from "class-transformer";
import { AssessmentListRelationFilter } from "../inputs/AssessmentListRelationFilter";
import { StringFilter } from "../inputs/StringFilter";

@InputType({
  isAbstract: true,
  description: undefined,
})
export class ProjectWhereInput {
  @ClassTransformer__Type(() => ProjectWhereInput)
  @Field(() => [ProjectWhereInput], {
    nullable: true,
    description: undefined
  })
  AND?: ProjectWhereInput[] | undefined;

  @ClassTransformer__Type(() => ProjectWhereInput)
  @Field(() => [ProjectWhereInput], {
    nullable: true,
    description: undefined
  })
  OR?: ProjectWhereInput[] | undefined;

  @ClassTransformer__Type(() => ProjectWhereInput)
  @Field(() => [ProjectWhereInput], {
    nullable: true,
    description: undefined
  })
  NOT?: ProjectWhereInput[] | undefined;

  @ClassTransformer__Type(() => StringFilter)
  @Field(() => StringFilter, {
    nullable: true,
    description: undefined
  })
  id?: StringFilter | undefined;

  @ClassTransformer__Type(() => AssessmentListRelationFilter)
  @Field(() => AssessmentListRelationFilter, {
    nullable: true,
    description: undefined
  })
  assessments?: AssessmentListRelationFilter | undefined;
}
