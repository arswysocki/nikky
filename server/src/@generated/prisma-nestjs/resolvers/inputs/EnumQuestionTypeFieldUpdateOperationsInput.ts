import { Field, Float, ID, InputType, Int } from "@nestjs/graphql";
import GraphQLJSON from "graphql-type-json";
import { JsonValue, InputJsonValue } from "../../../../../node_modules/@prisma/client";
import { Type as ClassTransformer__Type } from "class-transformer";
import { QuestionType } from "../../enums/QuestionType";

@InputType({
  isAbstract: true,
  description: undefined,
})
export class EnumQuestionTypeFieldUpdateOperationsInput {
  @Field(() => QuestionType, {
    nullable: true,
    description: undefined
  })
  set?: keyof typeof QuestionType | undefined;
}
