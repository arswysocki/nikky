import { Field, Float, ID, InputType, Int } from "@nestjs/graphql";
import GraphQLJSON from "graphql-type-json";
import { JsonValue, InputJsonValue } from "../../../../../node_modules/@prisma/client";
import { Type as ClassTransformer__Type } from "class-transformer";
import { ProjectCreateWithoutAssessmentsInput } from "../inputs/ProjectCreateWithoutAssessmentsInput";
import { ProjectUpdateWithoutAssessmentsDataInput } from "../inputs/ProjectUpdateWithoutAssessmentsDataInput";

@InputType({
  isAbstract: true,
  description: undefined,
})
export class ProjectUpsertWithoutAssessmentsInput {
  @ClassTransformer__Type(() => ProjectUpdateWithoutAssessmentsDataInput)
  @Field(() => ProjectUpdateWithoutAssessmentsDataInput, {
    nullable: false,
    description: undefined
  })
  update!: ProjectUpdateWithoutAssessmentsDataInput;

  @ClassTransformer__Type(() => ProjectCreateWithoutAssessmentsInput)
  @Field(() => ProjectCreateWithoutAssessmentsInput, {
    nullable: false,
    description: undefined
  })
  create!: ProjectCreateWithoutAssessmentsInput;
}
