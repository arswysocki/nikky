import { Field, Float, ID, InputType, Int } from "@nestjs/graphql";
import GraphQLJSON from "graphql-type-json";
import { JsonValue, InputJsonValue } from "../../../../../node_modules/@prisma/client";
import { Type as ClassTransformer__Type } from "class-transformer";
import { FormWhereInput } from "../inputs/FormWhereInput";

@InputType({
  isAbstract: true,
  description: undefined,
})
export class FormListRelationFilter {
  @ClassTransformer__Type(() => FormWhereInput)
  @Field(() => FormWhereInput, {
    nullable: true,
    description: undefined
  })
  every?: FormWhereInput | undefined;

  @ClassTransformer__Type(() => FormWhereInput)
  @Field(() => FormWhereInput, {
    nullable: true,
    description: undefined
  })
  some?: FormWhereInput | undefined;

  @ClassTransformer__Type(() => FormWhereInput)
  @Field(() => FormWhereInput, {
    nullable: true,
    description: undefined
  })
  none?: FormWhereInput | undefined;
}
