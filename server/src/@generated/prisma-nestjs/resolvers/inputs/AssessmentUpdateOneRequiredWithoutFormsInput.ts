import { Field, Float, ID, InputType, Int } from "@nestjs/graphql";
import GraphQLJSON from "graphql-type-json";
import { JsonValue, InputJsonValue } from "../../../../../node_modules/@prisma/client";
import { Type as ClassTransformer__Type } from "class-transformer";
import { AssessmentCreateWithoutFormsInput } from "../inputs/AssessmentCreateWithoutFormsInput";
import { AssessmentUpdateWithoutFormsDataInput } from "../inputs/AssessmentUpdateWithoutFormsDataInput";
import { AssessmentUpsertWithoutFormsInput } from "../inputs/AssessmentUpsertWithoutFormsInput";
import { AssessmentWhereUniqueInput } from "../inputs/AssessmentWhereUniqueInput";

@InputType({
  isAbstract: true,
  description: undefined,
})
export class AssessmentUpdateOneRequiredWithoutFormsInput {
  @ClassTransformer__Type(() => AssessmentCreateWithoutFormsInput)
  @Field(() => AssessmentCreateWithoutFormsInput, {
    nullable: true,
    description: undefined
  })
  create?: AssessmentCreateWithoutFormsInput | undefined;

  @ClassTransformer__Type(() => AssessmentWhereUniqueInput)
  @Field(() => AssessmentWhereUniqueInput, {
    nullable: true,
    description: undefined
  })
  connect?: AssessmentWhereUniqueInput | undefined;

  @ClassTransformer__Type(() => AssessmentUpdateWithoutFormsDataInput)
  @Field(() => AssessmentUpdateWithoutFormsDataInput, {
    nullable: true,
    description: undefined
  })
  update?: AssessmentUpdateWithoutFormsDataInput | undefined;

  @ClassTransformer__Type(() => AssessmentUpsertWithoutFormsInput)
  @Field(() => AssessmentUpsertWithoutFormsInput, {
    nullable: true,
    description: undefined
  })
  upsert?: AssessmentUpsertWithoutFormsInput | undefined;
}
