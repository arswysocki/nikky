import { Field, Float, ID, InputType, Int } from "@nestjs/graphql";
import GraphQLJSON from "graphql-type-json";
import { JsonValue, InputJsonValue } from "../../../../../node_modules/@prisma/client";
import { Type as ClassTransformer__Type } from "class-transformer";
import { AssessmentUpdateManyWithoutProjectInput } from "../inputs/AssessmentUpdateManyWithoutProjectInput";
import { StringFieldUpdateOperationsInput } from "../inputs/StringFieldUpdateOperationsInput";

@InputType({
  isAbstract: true,
  description: undefined,
})
export class ProjectUpdateInput {
  @ClassTransformer__Type(() => StringFieldUpdateOperationsInput)
  @Field(() => StringFieldUpdateOperationsInput, {
    nullable: true,
    description: undefined
  })
  id?: StringFieldUpdateOperationsInput | undefined;

  @ClassTransformer__Type(() => AssessmentUpdateManyWithoutProjectInput)
  @Field(() => AssessmentUpdateManyWithoutProjectInput, {
    nullable: true,
    description: undefined
  })
  assessments?: AssessmentUpdateManyWithoutProjectInput | undefined;
}
