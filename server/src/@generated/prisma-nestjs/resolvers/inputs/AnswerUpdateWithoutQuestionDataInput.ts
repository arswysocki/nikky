import { Field, Float, ID, InputType, Int } from "@nestjs/graphql";
import GraphQLJSON from "graphql-type-json";
import { JsonValue, InputJsonValue } from "../../../../../node_modules/@prisma/client";
import { Type as ClassTransformer__Type } from "class-transformer";
import { AnswerValueUpdateOneRequiredWithoutAnswerInput } from "../inputs/AnswerValueUpdateOneRequiredWithoutAnswerInput";
import { BoolFieldUpdateOperationsInput } from "../inputs/BoolFieldUpdateOperationsInput";
import { EnumAnswerStatusFieldUpdateOperationsInput } from "../inputs/EnumAnswerStatusFieldUpdateOperationsInput";
import { StringFieldUpdateOperationsInput } from "../inputs/StringFieldUpdateOperationsInput";
import { UserUpdateOneRequiredWithoutAnswerInput } from "../inputs/UserUpdateOneRequiredWithoutAnswerInput";

@InputType({
  isAbstract: true,
  description: undefined,
})
export class AnswerUpdateWithoutQuestionDataInput {
  @ClassTransformer__Type(() => StringFieldUpdateOperationsInput)
  @Field(() => StringFieldUpdateOperationsInput, {
    nullable: true,
    description: undefined
  })
  id?: StringFieldUpdateOperationsInput | undefined;

  @ClassTransformer__Type(() => BoolFieldUpdateOperationsInput)
  @Field(() => BoolFieldUpdateOperationsInput, {
    nullable: true,
    description: undefined
  })
  canSkip?: BoolFieldUpdateOperationsInput | undefined;

  @ClassTransformer__Type(() => EnumAnswerStatusFieldUpdateOperationsInput)
  @Field(() => EnumAnswerStatusFieldUpdateOperationsInput, {
    nullable: true,
    description: undefined
  })
  status?: EnumAnswerStatusFieldUpdateOperationsInput | undefined;

  @ClassTransformer__Type(() => BoolFieldUpdateOperationsInput)
  @Field(() => BoolFieldUpdateOperationsInput, {
    nullable: true,
    description: undefined
  })
  isAnonymous?: BoolFieldUpdateOperationsInput | undefined;

  @ClassTransformer__Type(() => StringFieldUpdateOperationsInput)
  @Field(() => StringFieldUpdateOperationsInput, {
    nullable: true,
    description: undefined
  })
  answerText?: StringFieldUpdateOperationsInput | undefined;

  @ClassTransformer__Type(() => UserUpdateOneRequiredWithoutAnswerInput)
  @Field(() => UserUpdateOneRequiredWithoutAnswerInput, {
    nullable: true,
    description: undefined
  })
  user?: UserUpdateOneRequiredWithoutAnswerInput | undefined;

  @ClassTransformer__Type(() => AnswerValueUpdateOneRequiredWithoutAnswerInput)
  @Field(() => AnswerValueUpdateOneRequiredWithoutAnswerInput, {
    nullable: true,
    description: undefined
  })
  answerValue?: AnswerValueUpdateOneRequiredWithoutAnswerInput | undefined;
}
