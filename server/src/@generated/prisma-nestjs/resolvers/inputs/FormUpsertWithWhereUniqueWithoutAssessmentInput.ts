import { Field, Float, ID, InputType, Int } from "@nestjs/graphql";
import GraphQLJSON from "graphql-type-json";
import { JsonValue, InputJsonValue } from "../../../../../node_modules/@prisma/client";
import { Type as ClassTransformer__Type } from "class-transformer";
import { FormCreateWithoutAssessmentInput } from "../inputs/FormCreateWithoutAssessmentInput";
import { FormUpdateWithoutAssessmentDataInput } from "../inputs/FormUpdateWithoutAssessmentDataInput";
import { FormWhereUniqueInput } from "../inputs/FormWhereUniqueInput";

@InputType({
  isAbstract: true,
  description: undefined,
})
export class FormUpsertWithWhereUniqueWithoutAssessmentInput {
  @ClassTransformer__Type(() => FormWhereUniqueInput)
  @Field(() => FormWhereUniqueInput, {
    nullable: false,
    description: undefined
  })
  where!: FormWhereUniqueInput;

  @ClassTransformer__Type(() => FormUpdateWithoutAssessmentDataInput)
  @Field(() => FormUpdateWithoutAssessmentDataInput, {
    nullable: false,
    description: undefined
  })
  update!: FormUpdateWithoutAssessmentDataInput;

  @ClassTransformer__Type(() => FormCreateWithoutAssessmentInput)
  @Field(() => FormCreateWithoutAssessmentInput, {
    nullable: false,
    description: undefined
  })
  create!: FormCreateWithoutAssessmentInput;
}
