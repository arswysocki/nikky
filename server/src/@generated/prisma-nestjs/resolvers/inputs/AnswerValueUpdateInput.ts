import { Field, Float, ID, InputType, Int } from "@nestjs/graphql";
import GraphQLJSON from "graphql-type-json";
import { JsonValue, InputJsonValue } from "../../../../../node_modules/@prisma/client";
import { Type as ClassTransformer__Type } from "class-transformer";
import { AnswerUpdateOneWithoutAnswerValueInput } from "../inputs/AnswerUpdateOneWithoutAnswerValueInput";
import { QuestionUpdateOneRequiredWithoutAnswerValuesInput } from "../inputs/QuestionUpdateOneRequiredWithoutAnswerValuesInput";
import { StringFieldUpdateOperationsInput } from "../inputs/StringFieldUpdateOperationsInput";

@InputType({
  isAbstract: true,
  description: undefined,
})
export class AnswerValueUpdateInput {
  @ClassTransformer__Type(() => StringFieldUpdateOperationsInput)
  @Field(() => StringFieldUpdateOperationsInput, {
    nullable: true,
    description: undefined
  })
  id?: StringFieldUpdateOperationsInput | undefined;

  @ClassTransformer__Type(() => StringFieldUpdateOperationsInput)
  @Field(() => StringFieldUpdateOperationsInput, {
    nullable: true,
    description: undefined
  })
  desc?: StringFieldUpdateOperationsInput | undefined;

  @ClassTransformer__Type(() => QuestionUpdateOneRequiredWithoutAnswerValuesInput)
  @Field(() => QuestionUpdateOneRequiredWithoutAnswerValuesInput, {
    nullable: true,
    description: undefined
  })
  Question?: QuestionUpdateOneRequiredWithoutAnswerValuesInput | undefined;

  @ClassTransformer__Type(() => AnswerUpdateOneWithoutAnswerValueInput)
  @Field(() => AnswerUpdateOneWithoutAnswerValueInput, {
    nullable: true,
    description: undefined
  })
  answer?: AnswerUpdateOneWithoutAnswerValueInput | undefined;
}
