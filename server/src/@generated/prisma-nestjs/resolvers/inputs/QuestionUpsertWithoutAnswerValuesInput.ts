import { Field, Float, ID, InputType, Int } from "@nestjs/graphql";
import GraphQLJSON from "graphql-type-json";
import { JsonValue, InputJsonValue } from "../../../../../node_modules/@prisma/client";
import { Type as ClassTransformer__Type } from "class-transformer";
import { QuestionCreateWithoutAnswerValuesInput } from "../inputs/QuestionCreateWithoutAnswerValuesInput";
import { QuestionUpdateWithoutAnswerValuesDataInput } from "../inputs/QuestionUpdateWithoutAnswerValuesDataInput";

@InputType({
  isAbstract: true,
  description: undefined,
})
export class QuestionUpsertWithoutAnswerValuesInput {
  @ClassTransformer__Type(() => QuestionUpdateWithoutAnswerValuesDataInput)
  @Field(() => QuestionUpdateWithoutAnswerValuesDataInput, {
    nullable: false,
    description: undefined
  })
  update!: QuestionUpdateWithoutAnswerValuesDataInput;

  @ClassTransformer__Type(() => QuestionCreateWithoutAnswerValuesInput)
  @Field(() => QuestionCreateWithoutAnswerValuesInput, {
    nullable: false,
    description: undefined
  })
  create!: QuestionCreateWithoutAnswerValuesInput;
}
