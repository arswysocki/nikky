import { Field, Float, ID, InputType, Int } from "@nestjs/graphql";
import GraphQLJSON from "graphql-type-json";
import { JsonValue, InputJsonValue } from "../../../../../node_modules/@prisma/client";
import { Type as ClassTransformer__Type } from "class-transformer";
import { AnswerValueScalarWhereInput } from "../inputs/AnswerValueScalarWhereInput";
import { AnswerValueUpdateManyDataInput } from "../inputs/AnswerValueUpdateManyDataInput";

@InputType({
  isAbstract: true,
  description: undefined,
})
export class AnswerValueUpdateManyWithWhereNestedInput {
  @ClassTransformer__Type(() => AnswerValueScalarWhereInput)
  @Field(() => AnswerValueScalarWhereInput, {
    nullable: false,
    description: undefined
  })
  where!: AnswerValueScalarWhereInput;

  @ClassTransformer__Type(() => AnswerValueUpdateManyDataInput)
  @Field(() => AnswerValueUpdateManyDataInput, {
    nullable: false,
    description: undefined
  })
  data!: AnswerValueUpdateManyDataInput;
}
