import { Field, Float, ID, InputType, Int } from "@nestjs/graphql";
import GraphQLJSON from "graphql-type-json";
import { JsonValue, InputJsonValue } from "../../../../../node_modules/@prisma/client";
import { Type as ClassTransformer__Type } from "class-transformer";
import { QuestionCreateWithoutAnswerValuesInput } from "../inputs/QuestionCreateWithoutAnswerValuesInput";
import { QuestionUpdateWithoutAnswerValuesDataInput } from "../inputs/QuestionUpdateWithoutAnswerValuesDataInput";
import { QuestionUpsertWithoutAnswerValuesInput } from "../inputs/QuestionUpsertWithoutAnswerValuesInput";
import { QuestionWhereUniqueInput } from "../inputs/QuestionWhereUniqueInput";

@InputType({
  isAbstract: true,
  description: undefined,
})
export class QuestionUpdateOneRequiredWithoutAnswerValuesInput {
  @ClassTransformer__Type(() => QuestionCreateWithoutAnswerValuesInput)
  @Field(() => QuestionCreateWithoutAnswerValuesInput, {
    nullable: true,
    description: undefined
  })
  create?: QuestionCreateWithoutAnswerValuesInput | undefined;

  @ClassTransformer__Type(() => QuestionWhereUniqueInput)
  @Field(() => QuestionWhereUniqueInput, {
    nullable: true,
    description: undefined
  })
  connect?: QuestionWhereUniqueInput | undefined;

  @ClassTransformer__Type(() => QuestionUpdateWithoutAnswerValuesDataInput)
  @Field(() => QuestionUpdateWithoutAnswerValuesDataInput, {
    nullable: true,
    description: undefined
  })
  update?: QuestionUpdateWithoutAnswerValuesDataInput | undefined;

  @ClassTransformer__Type(() => QuestionUpsertWithoutAnswerValuesInput)
  @Field(() => QuestionUpsertWithoutAnswerValuesInput, {
    nullable: true,
    description: undefined
  })
  upsert?: QuestionUpsertWithoutAnswerValuesInput | undefined;
}
