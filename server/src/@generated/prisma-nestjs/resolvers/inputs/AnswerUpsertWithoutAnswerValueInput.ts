import { Field, Float, ID, InputType, Int } from "@nestjs/graphql";
import GraphQLJSON from "graphql-type-json";
import { JsonValue, InputJsonValue } from "../../../../../node_modules/@prisma/client";
import { Type as ClassTransformer__Type } from "class-transformer";
import { AnswerCreateWithoutAnswerValueInput } from "../inputs/AnswerCreateWithoutAnswerValueInput";
import { AnswerUpdateWithoutAnswerValueDataInput } from "../inputs/AnswerUpdateWithoutAnswerValueDataInput";

@InputType({
  isAbstract: true,
  description: undefined,
})
export class AnswerUpsertWithoutAnswerValueInput {
  @ClassTransformer__Type(() => AnswerUpdateWithoutAnswerValueDataInput)
  @Field(() => AnswerUpdateWithoutAnswerValueDataInput, {
    nullable: false,
    description: undefined
  })
  update!: AnswerUpdateWithoutAnswerValueDataInput;

  @ClassTransformer__Type(() => AnswerCreateWithoutAnswerValueInput)
  @Field(() => AnswerCreateWithoutAnswerValueInput, {
    nullable: false,
    description: undefined
  })
  create!: AnswerCreateWithoutAnswerValueInput;
}
