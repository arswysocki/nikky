import { Field, Float, ID, InputType, Int } from "@nestjs/graphql";
import GraphQLJSON from "graphql-type-json";
import { JsonValue, InputJsonValue } from "../../../../../node_modules/@prisma/client";
import { Type as ClassTransformer__Type } from "class-transformer";
import { AnswerCreateWithoutQuestionInput } from "../inputs/AnswerCreateWithoutQuestionInput";
import { AnswerScalarWhereInput } from "../inputs/AnswerScalarWhereInput";
import { AnswerUpdateManyWithWhereNestedInput } from "../inputs/AnswerUpdateManyWithWhereNestedInput";
import { AnswerUpdateWithWhereUniqueWithoutQuestionInput } from "../inputs/AnswerUpdateWithWhereUniqueWithoutQuestionInput";
import { AnswerUpsertWithWhereUniqueWithoutQuestionInput } from "../inputs/AnswerUpsertWithWhereUniqueWithoutQuestionInput";
import { AnswerWhereUniqueInput } from "../inputs/AnswerWhereUniqueInput";

@InputType({
  isAbstract: true,
  description: undefined,
})
export class AnswerUpdateManyWithoutQuestionInput {
  @ClassTransformer__Type(() => AnswerCreateWithoutQuestionInput)
  @Field(() => [AnswerCreateWithoutQuestionInput], {
    nullable: true,
    description: undefined
  })
  create?: AnswerCreateWithoutQuestionInput[] | undefined;

  @ClassTransformer__Type(() => AnswerWhereUniqueInput)
  @Field(() => [AnswerWhereUniqueInput], {
    nullable: true,
    description: undefined
  })
  connect?: AnswerWhereUniqueInput[] | undefined;

  @ClassTransformer__Type(() => AnswerWhereUniqueInput)
  @Field(() => [AnswerWhereUniqueInput], {
    nullable: true,
    description: undefined
  })
  set?: AnswerWhereUniqueInput[] | undefined;

  @ClassTransformer__Type(() => AnswerWhereUniqueInput)
  @Field(() => [AnswerWhereUniqueInput], {
    nullable: true,
    description: undefined
  })
  disconnect?: AnswerWhereUniqueInput[] | undefined;

  @ClassTransformer__Type(() => AnswerWhereUniqueInput)
  @Field(() => [AnswerWhereUniqueInput], {
    nullable: true,
    description: undefined
  })
  delete?: AnswerWhereUniqueInput[] | undefined;

  @ClassTransformer__Type(() => AnswerUpdateWithWhereUniqueWithoutQuestionInput)
  @Field(() => [AnswerUpdateWithWhereUniqueWithoutQuestionInput], {
    nullable: true,
    description: undefined
  })
  update?: AnswerUpdateWithWhereUniqueWithoutQuestionInput[] | undefined;

  @ClassTransformer__Type(() => AnswerUpdateManyWithWhereNestedInput)
  @Field(() => [AnswerUpdateManyWithWhereNestedInput], {
    nullable: true,
    description: undefined
  })
  updateMany?: AnswerUpdateManyWithWhereNestedInput[] | undefined;

  @ClassTransformer__Type(() => AnswerScalarWhereInput)
  @Field(() => [AnswerScalarWhereInput], {
    nullable: true,
    description: undefined
  })
  deleteMany?: AnswerScalarWhereInput[] | undefined;

  @ClassTransformer__Type(() => AnswerUpsertWithWhereUniqueWithoutQuestionInput)
  @Field(() => [AnswerUpsertWithWhereUniqueWithoutQuestionInput], {
    nullable: true,
    description: undefined
  })
  upsert?: AnswerUpsertWithWhereUniqueWithoutQuestionInput[] | undefined;
}
