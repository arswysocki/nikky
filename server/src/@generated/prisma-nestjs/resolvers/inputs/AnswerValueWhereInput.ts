import { Field, Float, ID, InputType, Int } from "@nestjs/graphql";
import GraphQLJSON from "graphql-type-json";
import { JsonValue, InputJsonValue } from "../../../../../node_modules/@prisma/client";
import { Type as ClassTransformer__Type } from "class-transformer";
import { AnswerWhereInput } from "../inputs/AnswerWhereInput";
import { QuestionWhereInput } from "../inputs/QuestionWhereInput";
import { StringFilter } from "../inputs/StringFilter";

@InputType({
  isAbstract: true,
  description: undefined,
})
export class AnswerValueWhereInput {
  @ClassTransformer__Type(() => AnswerValueWhereInput)
  @Field(() => [AnswerValueWhereInput], {
    nullable: true,
    description: undefined
  })
  AND?: AnswerValueWhereInput[] | undefined;

  @ClassTransformer__Type(() => AnswerValueWhereInput)
  @Field(() => [AnswerValueWhereInput], {
    nullable: true,
    description: undefined
  })
  OR?: AnswerValueWhereInput[] | undefined;

  @ClassTransformer__Type(() => AnswerValueWhereInput)
  @Field(() => [AnswerValueWhereInput], {
    nullable: true,
    description: undefined
  })
  NOT?: AnswerValueWhereInput[] | undefined;

  @ClassTransformer__Type(() => StringFilter)
  @Field(() => StringFilter, {
    nullable: true,
    description: undefined
  })
  id?: StringFilter | undefined;

  @ClassTransformer__Type(() => QuestionWhereInput)
  @Field(() => QuestionWhereInput, {
    nullable: true,
    description: undefined
  })
  Question?: QuestionWhereInput | undefined;

  @ClassTransformer__Type(() => StringFilter)
  @Field(() => StringFilter, {
    nullable: true,
    description: undefined
  })
  questionId?: StringFilter | undefined;

  @ClassTransformer__Type(() => AnswerWhereInput)
  @Field(() => AnswerWhereInput, {
    nullable: true,
    description: undefined
  })
  answer?: AnswerWhereInput | undefined;

  @ClassTransformer__Type(() => StringFilter)
  @Field(() => StringFilter, {
    nullable: true,
    description: undefined
  })
  desc?: StringFilter | undefined;
}
