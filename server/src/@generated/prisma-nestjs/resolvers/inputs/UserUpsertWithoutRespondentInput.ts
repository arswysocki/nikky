import { Field, Float, ID, InputType, Int } from "@nestjs/graphql";
import GraphQLJSON from "graphql-type-json";
import { JsonValue, InputJsonValue } from "../../../../../node_modules/@prisma/client";
import { Type as ClassTransformer__Type } from "class-transformer";
import { UserCreateWithoutRespondentInput } from "../inputs/UserCreateWithoutRespondentInput";
import { UserUpdateWithoutRespondentDataInput } from "../inputs/UserUpdateWithoutRespondentDataInput";

@InputType({
  isAbstract: true,
  description: undefined,
})
export class UserUpsertWithoutRespondentInput {
  @ClassTransformer__Type(() => UserUpdateWithoutRespondentDataInput)
  @Field(() => UserUpdateWithoutRespondentDataInput, {
    nullable: false,
    description: undefined
  })
  update!: UserUpdateWithoutRespondentDataInput;

  @ClassTransformer__Type(() => UserCreateWithoutRespondentInput)
  @Field(() => UserCreateWithoutRespondentInput, {
    nullable: false,
    description: undefined
  })
  create!: UserCreateWithoutRespondentInput;
}
