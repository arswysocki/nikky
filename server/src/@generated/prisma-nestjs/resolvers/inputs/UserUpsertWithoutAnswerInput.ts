import { Field, Float, ID, InputType, Int } from "@nestjs/graphql";
import GraphQLJSON from "graphql-type-json";
import { JsonValue, InputJsonValue } from "../../../../../node_modules/@prisma/client";
import { Type as ClassTransformer__Type } from "class-transformer";
import { UserCreateWithoutAnswerInput } from "../inputs/UserCreateWithoutAnswerInput";
import { UserUpdateWithoutAnswerDataInput } from "../inputs/UserUpdateWithoutAnswerDataInput";

@InputType({
  isAbstract: true,
  description: undefined,
})
export class UserUpsertWithoutAnswerInput {
  @ClassTransformer__Type(() => UserUpdateWithoutAnswerDataInput)
  @Field(() => UserUpdateWithoutAnswerDataInput, {
    nullable: false,
    description: undefined
  })
  update!: UserUpdateWithoutAnswerDataInput;

  @ClassTransformer__Type(() => UserCreateWithoutAnswerInput)
  @Field(() => UserCreateWithoutAnswerInput, {
    nullable: false,
    description: undefined
  })
  create!: UserCreateWithoutAnswerInput;
}
