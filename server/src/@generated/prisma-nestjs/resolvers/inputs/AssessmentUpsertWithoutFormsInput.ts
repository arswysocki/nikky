import { Field, Float, ID, InputType, Int } from "@nestjs/graphql";
import GraphQLJSON from "graphql-type-json";
import { JsonValue, InputJsonValue } from "../../../../../node_modules/@prisma/client";
import { Type as ClassTransformer__Type } from "class-transformer";
import { AssessmentCreateWithoutFormsInput } from "../inputs/AssessmentCreateWithoutFormsInput";
import { AssessmentUpdateWithoutFormsDataInput } from "../inputs/AssessmentUpdateWithoutFormsDataInput";

@InputType({
  isAbstract: true,
  description: undefined,
})
export class AssessmentUpsertWithoutFormsInput {
  @ClassTransformer__Type(() => AssessmentUpdateWithoutFormsDataInput)
  @Field(() => AssessmentUpdateWithoutFormsDataInput, {
    nullable: false,
    description: undefined
  })
  update!: AssessmentUpdateWithoutFormsDataInput;

  @ClassTransformer__Type(() => AssessmentCreateWithoutFormsInput)
  @Field(() => AssessmentCreateWithoutFormsInput, {
    nullable: false,
    description: undefined
  })
  create!: AssessmentCreateWithoutFormsInput;
}
