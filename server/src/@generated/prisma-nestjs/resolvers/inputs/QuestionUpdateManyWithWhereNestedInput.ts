import { Field, Float, ID, InputType, Int } from "@nestjs/graphql";
import GraphQLJSON from "graphql-type-json";
import { JsonValue, InputJsonValue } from "../../../../../node_modules/@prisma/client";
import { Type as ClassTransformer__Type } from "class-transformer";
import { QuestionScalarWhereInput } from "../inputs/QuestionScalarWhereInput";
import { QuestionUpdateManyDataInput } from "../inputs/QuestionUpdateManyDataInput";

@InputType({
  isAbstract: true,
  description: undefined,
})
export class QuestionUpdateManyWithWhereNestedInput {
  @ClassTransformer__Type(() => QuestionScalarWhereInput)
  @Field(() => QuestionScalarWhereInput, {
    nullable: false,
    description: undefined
  })
  where!: QuestionScalarWhereInput;

  @ClassTransformer__Type(() => QuestionUpdateManyDataInput)
  @Field(() => QuestionUpdateManyDataInput, {
    nullable: false,
    description: undefined
  })
  data!: QuestionUpdateManyDataInput;
}
