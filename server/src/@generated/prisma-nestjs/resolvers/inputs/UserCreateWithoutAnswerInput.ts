import { Field, Float, ID, InputType, Int } from "@nestjs/graphql";
import GraphQLJSON from "graphql-type-json";
import { JsonValue, InputJsonValue } from "../../../../../node_modules/@prisma/client";
import { Type as ClassTransformer__Type } from "class-transformer";
import { AssessmentCreateManyWithoutRecipientInput } from "../inputs/AssessmentCreateManyWithoutRecipientInput";
import { AssessmentCreateManyWithoutUserInput } from "../inputs/AssessmentCreateManyWithoutUserInput";
import { Gender } from "../../enums/Gender";
import { Role } from "../../enums/Role";

@InputType({
  isAbstract: true,
  description: undefined,
})
export class UserCreateWithoutAnswerInput {
  @Field(() => String, {
    nullable: true,
    description: undefined
  })
  id?: string | undefined;

  @Field(() => String, {
    nullable: true,
    description: undefined
  })
  firstName?: string | undefined;

  @Field(() => String, {
    nullable: true,
    description: undefined
  })
  lastName?: string | undefined;

  @Field(() => String, {
    nullable: true,
    description: undefined
  })
  email?: string | undefined;

  @Field(() => String, {
    nullable: true,
    description: undefined
  })
  picture?: string | undefined;

  @Field(() => Gender, {
    nullable: false,
    description: undefined
  })
  gender!: keyof typeof Gender;

  @Field(() => Role, {
    nullable: false,
    description: undefined
  })
  role!: keyof typeof Role;

  @ClassTransformer__Type(() => AssessmentCreateManyWithoutUserInput)
  @Field(() => AssessmentCreateManyWithoutUserInput, {
    nullable: true,
    description: undefined
  })
  Respondent?: AssessmentCreateManyWithoutUserInput | undefined;

  @ClassTransformer__Type(() => AssessmentCreateManyWithoutRecipientInput)
  @Field(() => AssessmentCreateManyWithoutRecipientInput, {
    nullable: true,
    description: undefined
  })
  Recipient?: AssessmentCreateManyWithoutRecipientInput | undefined;
}
