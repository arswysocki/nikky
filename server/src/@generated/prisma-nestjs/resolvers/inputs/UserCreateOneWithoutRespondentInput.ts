import { Field, Float, ID, InputType, Int } from "@nestjs/graphql";
import GraphQLJSON from "graphql-type-json";
import { JsonValue, InputJsonValue } from "../../../../../node_modules/@prisma/client";
import { Type as ClassTransformer__Type } from "class-transformer";
import { UserCreateWithoutRespondentInput } from "../inputs/UserCreateWithoutRespondentInput";
import { UserWhereUniqueInput } from "../inputs/UserWhereUniqueInput";

@InputType({
  isAbstract: true,
  description: undefined,
})
export class UserCreateOneWithoutRespondentInput {
  @ClassTransformer__Type(() => UserCreateWithoutRespondentInput)
  @Field(() => UserCreateWithoutRespondentInput, {
    nullable: true,
    description: undefined
  })
  create?: UserCreateWithoutRespondentInput | undefined;

  @ClassTransformer__Type(() => UserWhereUniqueInput)
  @Field(() => UserWhereUniqueInput, {
    nullable: true,
    description: undefined
  })
  connect?: UserWhereUniqueInput | undefined;
}
