import { Field, Float, ID, InputType, Int } from "@nestjs/graphql";
import GraphQLJSON from "graphql-type-json";
import { JsonValue, InputJsonValue } from "../../../../../node_modules/@prisma/client";
import { Type as ClassTransformer__Type } from "class-transformer";
import { StringFilter } from "../inputs/StringFilter";

@InputType({
  isAbstract: true,
  description: undefined,
})
export class FormScalarWhereInput {
  @ClassTransformer__Type(() => FormScalarWhereInput)
  @Field(() => [FormScalarWhereInput], {
    nullable: true,
    description: undefined
  })
  AND?: FormScalarWhereInput[] | undefined;

  @ClassTransformer__Type(() => FormScalarWhereInput)
  @Field(() => [FormScalarWhereInput], {
    nullable: true,
    description: undefined
  })
  OR?: FormScalarWhereInput[] | undefined;

  @ClassTransformer__Type(() => FormScalarWhereInput)
  @Field(() => [FormScalarWhereInput], {
    nullable: true,
    description: undefined
  })
  NOT?: FormScalarWhereInput[] | undefined;

  @ClassTransformer__Type(() => StringFilter)
  @Field(() => StringFilter, {
    nullable: true,
    description: undefined
  })
  id?: StringFilter | undefined;

  @ClassTransformer__Type(() => StringFilter)
  @Field(() => StringFilter, {
    nullable: true,
    description: undefined
  })
  desc?: StringFilter | undefined;

  @ClassTransformer__Type(() => StringFilter)
  @Field(() => StringFilter, {
    nullable: true,
    description: undefined
  })
  assessmentId?: StringFilter | undefined;
}
