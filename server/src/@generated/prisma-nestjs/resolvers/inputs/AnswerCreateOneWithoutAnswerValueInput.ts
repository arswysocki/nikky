import { Field, Float, ID, InputType, Int } from "@nestjs/graphql";
import GraphQLJSON from "graphql-type-json";
import { JsonValue, InputJsonValue } from "../../../../../node_modules/@prisma/client";
import { Type as ClassTransformer__Type } from "class-transformer";
import { AnswerCreateWithoutAnswerValueInput } from "../inputs/AnswerCreateWithoutAnswerValueInput";
import { AnswerWhereUniqueInput } from "../inputs/AnswerWhereUniqueInput";

@InputType({
  isAbstract: true,
  description: undefined,
})
export class AnswerCreateOneWithoutAnswerValueInput {
  @ClassTransformer__Type(() => AnswerCreateWithoutAnswerValueInput)
  @Field(() => AnswerCreateWithoutAnswerValueInput, {
    nullable: true,
    description: undefined
  })
  create?: AnswerCreateWithoutAnswerValueInput | undefined;

  @ClassTransformer__Type(() => AnswerWhereUniqueInput)
  @Field(() => AnswerWhereUniqueInput, {
    nullable: true,
    description: undefined
  })
  connect?: AnswerWhereUniqueInput | undefined;
}
