import { Field, Float, ID, InputType, Int } from "@nestjs/graphql";
import GraphQLJSON from "graphql-type-json";
import { JsonValue, InputJsonValue } from "../../../../../node_modules/@prisma/client";
import { Type as ClassTransformer__Type } from "class-transformer";
import { AssessmentCreateManyWithoutProjectInput } from "../inputs/AssessmentCreateManyWithoutProjectInput";

@InputType({
  isAbstract: true,
  description: undefined,
})
export class ProjectCreateInput {
  @Field(() => String, {
    nullable: true,
    description: undefined
  })
  id?: string | undefined;

  @ClassTransformer__Type(() => AssessmentCreateManyWithoutProjectInput)
  @Field(() => AssessmentCreateManyWithoutProjectInput, {
    nullable: true,
    description: undefined
  })
  assessments?: AssessmentCreateManyWithoutProjectInput | undefined;
}
