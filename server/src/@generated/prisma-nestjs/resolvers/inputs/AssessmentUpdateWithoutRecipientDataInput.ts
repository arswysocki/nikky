import { Field, Float, ID, InputType, Int } from "@nestjs/graphql";
import GraphQLJSON from "graphql-type-json";
import { JsonValue, InputJsonValue } from "../../../../../node_modules/@prisma/client";
import { Type as ClassTransformer__Type } from "class-transformer";
import { FormUpdateManyWithoutAssessmentInput } from "../inputs/FormUpdateManyWithoutAssessmentInput";
import { ProjectUpdateOneRequiredWithoutAssessmentsInput } from "../inputs/ProjectUpdateOneRequiredWithoutAssessmentsInput";
import { StringFieldUpdateOperationsInput } from "../inputs/StringFieldUpdateOperationsInput";
import { UserUpdateOneRequiredWithoutRespondentInput } from "../inputs/UserUpdateOneRequiredWithoutRespondentInput";

@InputType({
  isAbstract: true,
  description: undefined,
})
export class AssessmentUpdateWithoutRecipientDataInput {
  @ClassTransformer__Type(() => StringFieldUpdateOperationsInput)
  @Field(() => StringFieldUpdateOperationsInput, {
    nullable: true,
    description: undefined
  })
  id?: StringFieldUpdateOperationsInput | undefined;

  @ClassTransformer__Type(() => UserUpdateOneRequiredWithoutRespondentInput)
  @Field(() => UserUpdateOneRequiredWithoutRespondentInput, {
    nullable: true,
    description: undefined
  })
  user?: UserUpdateOneRequiredWithoutRespondentInput | undefined;

  @ClassTransformer__Type(() => FormUpdateManyWithoutAssessmentInput)
  @Field(() => FormUpdateManyWithoutAssessmentInput, {
    nullable: true,
    description: undefined
  })
  forms?: FormUpdateManyWithoutAssessmentInput | undefined;

  @ClassTransformer__Type(() => ProjectUpdateOneRequiredWithoutAssessmentsInput)
  @Field(() => ProjectUpdateOneRequiredWithoutAssessmentsInput, {
    nullable: true,
    description: undefined
  })
  Project?: ProjectUpdateOneRequiredWithoutAssessmentsInput | undefined;
}
