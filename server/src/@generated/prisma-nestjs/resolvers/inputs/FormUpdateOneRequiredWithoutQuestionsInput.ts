import { Field, Float, ID, InputType, Int } from "@nestjs/graphql";
import GraphQLJSON from "graphql-type-json";
import { JsonValue, InputJsonValue } from "../../../../../node_modules/@prisma/client";
import { Type as ClassTransformer__Type } from "class-transformer";
import { FormCreateWithoutQuestionsInput } from "../inputs/FormCreateWithoutQuestionsInput";
import { FormUpdateWithoutQuestionsDataInput } from "../inputs/FormUpdateWithoutQuestionsDataInput";
import { FormUpsertWithoutQuestionsInput } from "../inputs/FormUpsertWithoutQuestionsInput";
import { FormWhereUniqueInput } from "../inputs/FormWhereUniqueInput";

@InputType({
  isAbstract: true,
  description: undefined,
})
export class FormUpdateOneRequiredWithoutQuestionsInput {
  @ClassTransformer__Type(() => FormCreateWithoutQuestionsInput)
  @Field(() => FormCreateWithoutQuestionsInput, {
    nullable: true,
    description: undefined
  })
  create?: FormCreateWithoutQuestionsInput | undefined;

  @ClassTransformer__Type(() => FormWhereUniqueInput)
  @Field(() => FormWhereUniqueInput, {
    nullable: true,
    description: undefined
  })
  connect?: FormWhereUniqueInput | undefined;

  @ClassTransformer__Type(() => FormUpdateWithoutQuestionsDataInput)
  @Field(() => FormUpdateWithoutQuestionsDataInput, {
    nullable: true,
    description: undefined
  })
  update?: FormUpdateWithoutQuestionsDataInput | undefined;

  @ClassTransformer__Type(() => FormUpsertWithoutQuestionsInput)
  @Field(() => FormUpsertWithoutQuestionsInput, {
    nullable: true,
    description: undefined
  })
  upsert?: FormUpsertWithoutQuestionsInput | undefined;
}
