import { Field, Float, ID, InputType, Int } from "@nestjs/graphql";
import GraphQLJSON from "graphql-type-json";
import { JsonValue, InputJsonValue } from "../../../../../node_modules/@prisma/client";
import { Type as ClassTransformer__Type } from "class-transformer";
import { AnswerCreateWithoutUserInput } from "../inputs/AnswerCreateWithoutUserInput";
import { AnswerUpdateWithoutUserDataInput } from "../inputs/AnswerUpdateWithoutUserDataInput";
import { AnswerWhereUniqueInput } from "../inputs/AnswerWhereUniqueInput";

@InputType({
  isAbstract: true,
  description: undefined,
})
export class AnswerUpsertWithWhereUniqueWithoutUserInput {
  @ClassTransformer__Type(() => AnswerWhereUniqueInput)
  @Field(() => AnswerWhereUniqueInput, {
    nullable: false,
    description: undefined
  })
  where!: AnswerWhereUniqueInput;

  @ClassTransformer__Type(() => AnswerUpdateWithoutUserDataInput)
  @Field(() => AnswerUpdateWithoutUserDataInput, {
    nullable: false,
    description: undefined
  })
  update!: AnswerUpdateWithoutUserDataInput;

  @ClassTransformer__Type(() => AnswerCreateWithoutUserInput)
  @Field(() => AnswerCreateWithoutUserInput, {
    nullable: false,
    description: undefined
  })
  create!: AnswerCreateWithoutUserInput;
}
