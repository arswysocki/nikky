import { Field, Float, ID, InputType, Int } from "@nestjs/graphql";
import GraphQLJSON from "graphql-type-json";
import { JsonValue, InputJsonValue } from "../../../../../node_modules/@prisma/client";
import { Type as ClassTransformer__Type } from "class-transformer";
import { FormWhereInput } from "../inputs/FormWhereInput";

@InputType({
  isAbstract: true,
  description: undefined,
})
export class FormRelationFilter {
  @ClassTransformer__Type(() => FormWhereInput)
  @Field(() => FormWhereInput, {
    nullable: true,
    description: undefined
  })
  is?: FormWhereInput | undefined;

  @ClassTransformer__Type(() => FormWhereInput)
  @Field(() => FormWhereInput, {
    nullable: true,
    description: undefined
  })
  isNot?: FormWhereInput | undefined;
}
