import { Field, Float, ID, InputType, Int } from "@nestjs/graphql";
import GraphQLJSON from "graphql-type-json";
import { JsonValue, InputJsonValue } from "../../../../../node_modules/@prisma/client";
import { Type as ClassTransformer__Type } from "class-transformer";
import { AnswerUpdateManyWithoutUserInput } from "../inputs/AnswerUpdateManyWithoutUserInput";
import { AssessmentUpdateManyWithoutRecipientInput } from "../inputs/AssessmentUpdateManyWithoutRecipientInput";
import { AssessmentUpdateManyWithoutUserInput } from "../inputs/AssessmentUpdateManyWithoutUserInput";
import { EnumGenderFieldUpdateOperationsInput } from "../inputs/EnumGenderFieldUpdateOperationsInput";
import { EnumRoleFieldUpdateOperationsInput } from "../inputs/EnumRoleFieldUpdateOperationsInput";
import { StringFieldUpdateOperationsInput } from "../inputs/StringFieldUpdateOperationsInput";

@InputType({
  isAbstract: true,
  description: undefined,
})
export class UserUpdateInput {
  @ClassTransformer__Type(() => StringFieldUpdateOperationsInput)
  @Field(() => StringFieldUpdateOperationsInput, {
    nullable: true,
    description: undefined
  })
  id?: StringFieldUpdateOperationsInput | undefined;

  @ClassTransformer__Type(() => StringFieldUpdateOperationsInput)
  @Field(() => StringFieldUpdateOperationsInput, {
    nullable: true,
    description: undefined
  })
  firstName?: StringFieldUpdateOperationsInput | undefined;

  @ClassTransformer__Type(() => StringFieldUpdateOperationsInput)
  @Field(() => StringFieldUpdateOperationsInput, {
    nullable: true,
    description: undefined
  })
  lastName?: StringFieldUpdateOperationsInput | undefined;

  @ClassTransformer__Type(() => StringFieldUpdateOperationsInput)
  @Field(() => StringFieldUpdateOperationsInput, {
    nullable: true,
    description: undefined
  })
  email?: StringFieldUpdateOperationsInput | undefined;

  @ClassTransformer__Type(() => StringFieldUpdateOperationsInput)
  @Field(() => StringFieldUpdateOperationsInput, {
    nullable: true,
    description: undefined
  })
  picture?: StringFieldUpdateOperationsInput | undefined;

  @ClassTransformer__Type(() => EnumGenderFieldUpdateOperationsInput)
  @Field(() => EnumGenderFieldUpdateOperationsInput, {
    nullable: true,
    description: undefined
  })
  gender?: EnumGenderFieldUpdateOperationsInput | undefined;

  @ClassTransformer__Type(() => EnumRoleFieldUpdateOperationsInput)
  @Field(() => EnumRoleFieldUpdateOperationsInput, {
    nullable: true,
    description: undefined
  })
  role?: EnumRoleFieldUpdateOperationsInput | undefined;

  @ClassTransformer__Type(() => AssessmentUpdateManyWithoutUserInput)
  @Field(() => AssessmentUpdateManyWithoutUserInput, {
    nullable: true,
    description: undefined
  })
  Respondent?: AssessmentUpdateManyWithoutUserInput | undefined;

  @ClassTransformer__Type(() => AssessmentUpdateManyWithoutRecipientInput)
  @Field(() => AssessmentUpdateManyWithoutRecipientInput, {
    nullable: true,
    description: undefined
  })
  Recipient?: AssessmentUpdateManyWithoutRecipientInput | undefined;

  @ClassTransformer__Type(() => AnswerUpdateManyWithoutUserInput)
  @Field(() => AnswerUpdateManyWithoutUserInput, {
    nullable: true,
    description: undefined
  })
  Answer?: AnswerUpdateManyWithoutUserInput | undefined;
}
