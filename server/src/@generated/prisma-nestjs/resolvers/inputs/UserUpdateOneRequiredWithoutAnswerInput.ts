import { Field, Float, ID, InputType, Int } from "@nestjs/graphql";
import GraphQLJSON from "graphql-type-json";
import { JsonValue, InputJsonValue } from "../../../../../node_modules/@prisma/client";
import { Type as ClassTransformer__Type } from "class-transformer";
import { UserCreateWithoutAnswerInput } from "../inputs/UserCreateWithoutAnswerInput";
import { UserUpdateWithoutAnswerDataInput } from "../inputs/UserUpdateWithoutAnswerDataInput";
import { UserUpsertWithoutAnswerInput } from "../inputs/UserUpsertWithoutAnswerInput";
import { UserWhereUniqueInput } from "../inputs/UserWhereUniqueInput";

@InputType({
  isAbstract: true,
  description: undefined,
})
export class UserUpdateOneRequiredWithoutAnswerInput {
  @ClassTransformer__Type(() => UserCreateWithoutAnswerInput)
  @Field(() => UserCreateWithoutAnswerInput, {
    nullable: true,
    description: undefined
  })
  create?: UserCreateWithoutAnswerInput | undefined;

  @ClassTransformer__Type(() => UserWhereUniqueInput)
  @Field(() => UserWhereUniqueInput, {
    nullable: true,
    description: undefined
  })
  connect?: UserWhereUniqueInput | undefined;

  @ClassTransformer__Type(() => UserUpdateWithoutAnswerDataInput)
  @Field(() => UserUpdateWithoutAnswerDataInput, {
    nullable: true,
    description: undefined
  })
  update?: UserUpdateWithoutAnswerDataInput | undefined;

  @ClassTransformer__Type(() => UserUpsertWithoutAnswerInput)
  @Field(() => UserUpsertWithoutAnswerInput, {
    nullable: true,
    description: undefined
  })
  upsert?: UserUpsertWithoutAnswerInput | undefined;
}
