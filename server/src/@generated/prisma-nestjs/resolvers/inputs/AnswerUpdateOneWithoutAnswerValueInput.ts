import { Field, Float, ID, InputType, Int } from "@nestjs/graphql";
import GraphQLJSON from "graphql-type-json";
import { JsonValue, InputJsonValue } from "../../../../../node_modules/@prisma/client";
import { Type as ClassTransformer__Type } from "class-transformer";
import { AnswerCreateWithoutAnswerValueInput } from "../inputs/AnswerCreateWithoutAnswerValueInput";
import { AnswerUpdateWithoutAnswerValueDataInput } from "../inputs/AnswerUpdateWithoutAnswerValueDataInput";
import { AnswerUpsertWithoutAnswerValueInput } from "../inputs/AnswerUpsertWithoutAnswerValueInput";
import { AnswerWhereUniqueInput } from "../inputs/AnswerWhereUniqueInput";

@InputType({
  isAbstract: true,
  description: undefined,
})
export class AnswerUpdateOneWithoutAnswerValueInput {
  @ClassTransformer__Type(() => AnswerCreateWithoutAnswerValueInput)
  @Field(() => AnswerCreateWithoutAnswerValueInput, {
    nullable: true,
    description: undefined
  })
  create?: AnswerCreateWithoutAnswerValueInput | undefined;

  @ClassTransformer__Type(() => AnswerWhereUniqueInput)
  @Field(() => AnswerWhereUniqueInput, {
    nullable: true,
    description: undefined
  })
  connect?: AnswerWhereUniqueInput | undefined;

  @Field(() => Boolean, {
    nullable: true,
    description: undefined
  })
  disconnect?: boolean | undefined;

  @Field(() => Boolean, {
    nullable: true,
    description: undefined
  })
  delete?: boolean | undefined;

  @ClassTransformer__Type(() => AnswerUpdateWithoutAnswerValueDataInput)
  @Field(() => AnswerUpdateWithoutAnswerValueDataInput, {
    nullable: true,
    description: undefined
  })
  update?: AnswerUpdateWithoutAnswerValueDataInput | undefined;

  @ClassTransformer__Type(() => AnswerUpsertWithoutAnswerValueInput)
  @Field(() => AnswerUpsertWithoutAnswerValueInput, {
    nullable: true,
    description: undefined
  })
  upsert?: AnswerUpsertWithoutAnswerValueInput | undefined;
}
