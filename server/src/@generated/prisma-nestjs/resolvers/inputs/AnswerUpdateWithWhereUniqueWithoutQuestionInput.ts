import { Field, Float, ID, InputType, Int } from "@nestjs/graphql";
import GraphQLJSON from "graphql-type-json";
import { JsonValue, InputJsonValue } from "../../../../../node_modules/@prisma/client";
import { Type as ClassTransformer__Type } from "class-transformer";
import { AnswerUpdateWithoutQuestionDataInput } from "../inputs/AnswerUpdateWithoutQuestionDataInput";
import { AnswerWhereUniqueInput } from "../inputs/AnswerWhereUniqueInput";

@InputType({
  isAbstract: true,
  description: undefined,
})
export class AnswerUpdateWithWhereUniqueWithoutQuestionInput {
  @ClassTransformer__Type(() => AnswerWhereUniqueInput)
  @Field(() => AnswerWhereUniqueInput, {
    nullable: false,
    description: undefined
  })
  where!: AnswerWhereUniqueInput;

  @ClassTransformer__Type(() => AnswerUpdateWithoutQuestionDataInput)
  @Field(() => AnswerUpdateWithoutQuestionDataInput, {
    nullable: false,
    description: undefined
  })
  data!: AnswerUpdateWithoutQuestionDataInput;
}
