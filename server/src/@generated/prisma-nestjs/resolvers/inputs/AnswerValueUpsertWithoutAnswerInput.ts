import { Field, Float, ID, InputType, Int } from "@nestjs/graphql";
import GraphQLJSON from "graphql-type-json";
import { JsonValue, InputJsonValue } from "../../../../../node_modules/@prisma/client";
import { Type as ClassTransformer__Type } from "class-transformer";
import { AnswerValueCreateWithoutAnswerInput } from "../inputs/AnswerValueCreateWithoutAnswerInput";
import { AnswerValueUpdateWithoutAnswerDataInput } from "../inputs/AnswerValueUpdateWithoutAnswerDataInput";

@InputType({
  isAbstract: true,
  description: undefined,
})
export class AnswerValueUpsertWithoutAnswerInput {
  @ClassTransformer__Type(() => AnswerValueUpdateWithoutAnswerDataInput)
  @Field(() => AnswerValueUpdateWithoutAnswerDataInput, {
    nullable: false,
    description: undefined
  })
  update!: AnswerValueUpdateWithoutAnswerDataInput;

  @ClassTransformer__Type(() => AnswerValueCreateWithoutAnswerInput)
  @Field(() => AnswerValueCreateWithoutAnswerInput, {
    nullable: false,
    description: undefined
  })
  create!: AnswerValueCreateWithoutAnswerInput;
}
