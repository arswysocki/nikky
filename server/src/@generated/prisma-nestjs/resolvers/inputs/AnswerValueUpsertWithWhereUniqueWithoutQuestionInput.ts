import { Field, Float, ID, InputType, Int } from "@nestjs/graphql";
import GraphQLJSON from "graphql-type-json";
import { JsonValue, InputJsonValue } from "../../../../../node_modules/@prisma/client";
import { Type as ClassTransformer__Type } from "class-transformer";
import { AnswerValueCreateWithoutQuestionInput } from "../inputs/AnswerValueCreateWithoutQuestionInput";
import { AnswerValueUpdateWithoutQuestionDataInput } from "../inputs/AnswerValueUpdateWithoutQuestionDataInput";
import { AnswerValueWhereUniqueInput } from "../inputs/AnswerValueWhereUniqueInput";

@InputType({
  isAbstract: true,
  description: undefined,
})
export class AnswerValueUpsertWithWhereUniqueWithoutQuestionInput {
  @ClassTransformer__Type(() => AnswerValueWhereUniqueInput)
  @Field(() => AnswerValueWhereUniqueInput, {
    nullable: false,
    description: undefined
  })
  where!: AnswerValueWhereUniqueInput;

  @ClassTransformer__Type(() => AnswerValueUpdateWithoutQuestionDataInput)
  @Field(() => AnswerValueUpdateWithoutQuestionDataInput, {
    nullable: false,
    description: undefined
  })
  update!: AnswerValueUpdateWithoutQuestionDataInput;

  @ClassTransformer__Type(() => AnswerValueCreateWithoutQuestionInput)
  @Field(() => AnswerValueCreateWithoutQuestionInput, {
    nullable: false,
    description: undefined
  })
  create!: AnswerValueCreateWithoutQuestionInput;
}
