import { Field, Float, ID, InputType, Int } from "@nestjs/graphql";
import GraphQLJSON from "graphql-type-json";
import { JsonValue, InputJsonValue } from "../../../../../node_modules/@prisma/client";
import { Type as ClassTransformer__Type } from "class-transformer";
import { AssessmentCreateOneWithoutFormsInput } from "../inputs/AssessmentCreateOneWithoutFormsInput";

@InputType({
  isAbstract: true,
  description: undefined,
})
export class FormCreateWithoutQuestionsInput {
  @Field(() => String, {
    nullable: true,
    description: undefined
  })
  id?: string | undefined;

  @Field(() => String, {
    nullable: false,
    description: undefined
  })
  desc!: string;

  @ClassTransformer__Type(() => AssessmentCreateOneWithoutFormsInput)
  @Field(() => AssessmentCreateOneWithoutFormsInput, {
    nullable: false,
    description: undefined
  })
  Assessment!: AssessmentCreateOneWithoutFormsInput;
}
