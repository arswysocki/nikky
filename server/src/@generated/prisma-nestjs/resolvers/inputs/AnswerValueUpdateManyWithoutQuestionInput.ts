import { Field, Float, ID, InputType, Int } from "@nestjs/graphql";
import GraphQLJSON from "graphql-type-json";
import { JsonValue, InputJsonValue } from "../../../../../node_modules/@prisma/client";
import { Type as ClassTransformer__Type } from "class-transformer";
import { AnswerValueCreateWithoutQuestionInput } from "../inputs/AnswerValueCreateWithoutQuestionInput";
import { AnswerValueScalarWhereInput } from "../inputs/AnswerValueScalarWhereInput";
import { AnswerValueUpdateManyWithWhereNestedInput } from "../inputs/AnswerValueUpdateManyWithWhereNestedInput";
import { AnswerValueUpdateWithWhereUniqueWithoutQuestionInput } from "../inputs/AnswerValueUpdateWithWhereUniqueWithoutQuestionInput";
import { AnswerValueUpsertWithWhereUniqueWithoutQuestionInput } from "../inputs/AnswerValueUpsertWithWhereUniqueWithoutQuestionInput";
import { AnswerValueWhereUniqueInput } from "../inputs/AnswerValueWhereUniqueInput";

@InputType({
  isAbstract: true,
  description: undefined,
})
export class AnswerValueUpdateManyWithoutQuestionInput {
  @ClassTransformer__Type(() => AnswerValueCreateWithoutQuestionInput)
  @Field(() => [AnswerValueCreateWithoutQuestionInput], {
    nullable: true,
    description: undefined
  })
  create?: AnswerValueCreateWithoutQuestionInput[] | undefined;

  @ClassTransformer__Type(() => AnswerValueWhereUniqueInput)
  @Field(() => [AnswerValueWhereUniqueInput], {
    nullable: true,
    description: undefined
  })
  connect?: AnswerValueWhereUniqueInput[] | undefined;

  @ClassTransformer__Type(() => AnswerValueWhereUniqueInput)
  @Field(() => [AnswerValueWhereUniqueInput], {
    nullable: true,
    description: undefined
  })
  set?: AnswerValueWhereUniqueInput[] | undefined;

  @ClassTransformer__Type(() => AnswerValueWhereUniqueInput)
  @Field(() => [AnswerValueWhereUniqueInput], {
    nullable: true,
    description: undefined
  })
  disconnect?: AnswerValueWhereUniqueInput[] | undefined;

  @ClassTransformer__Type(() => AnswerValueWhereUniqueInput)
  @Field(() => [AnswerValueWhereUniqueInput], {
    nullable: true,
    description: undefined
  })
  delete?: AnswerValueWhereUniqueInput[] | undefined;

  @ClassTransformer__Type(() => AnswerValueUpdateWithWhereUniqueWithoutQuestionInput)
  @Field(() => [AnswerValueUpdateWithWhereUniqueWithoutQuestionInput], {
    nullable: true,
    description: undefined
  })
  update?: AnswerValueUpdateWithWhereUniqueWithoutQuestionInput[] | undefined;

  @ClassTransformer__Type(() => AnswerValueUpdateManyWithWhereNestedInput)
  @Field(() => [AnswerValueUpdateManyWithWhereNestedInput], {
    nullable: true,
    description: undefined
  })
  updateMany?: AnswerValueUpdateManyWithWhereNestedInput[] | undefined;

  @ClassTransformer__Type(() => AnswerValueScalarWhereInput)
  @Field(() => [AnswerValueScalarWhereInput], {
    nullable: true,
    description: undefined
  })
  deleteMany?: AnswerValueScalarWhereInput[] | undefined;

  @ClassTransformer__Type(() => AnswerValueUpsertWithWhereUniqueWithoutQuestionInput)
  @Field(() => [AnswerValueUpsertWithWhereUniqueWithoutQuestionInput], {
    nullable: true,
    description: undefined
  })
  upsert?: AnswerValueUpsertWithWhereUniqueWithoutQuestionInput[] | undefined;
}
