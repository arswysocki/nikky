import { Field, Float, ID, InputType, Int } from "@nestjs/graphql";
import GraphQLJSON from "graphql-type-json";
import { JsonValue, InputJsonValue } from "../../../../../node_modules/@prisma/client";
import { Type as ClassTransformer__Type } from "class-transformer";
import { StringFilter } from "../inputs/StringFilter";

@InputType({
  isAbstract: true,
  description: undefined,
})
export class AnswerValueScalarWhereInput {
  @ClassTransformer__Type(() => AnswerValueScalarWhereInput)
  @Field(() => [AnswerValueScalarWhereInput], {
    nullable: true,
    description: undefined
  })
  AND?: AnswerValueScalarWhereInput[] | undefined;

  @ClassTransformer__Type(() => AnswerValueScalarWhereInput)
  @Field(() => [AnswerValueScalarWhereInput], {
    nullable: true,
    description: undefined
  })
  OR?: AnswerValueScalarWhereInput[] | undefined;

  @ClassTransformer__Type(() => AnswerValueScalarWhereInput)
  @Field(() => [AnswerValueScalarWhereInput], {
    nullable: true,
    description: undefined
  })
  NOT?: AnswerValueScalarWhereInput[] | undefined;

  @ClassTransformer__Type(() => StringFilter)
  @Field(() => StringFilter, {
    nullable: true,
    description: undefined
  })
  id?: StringFilter | undefined;

  @ClassTransformer__Type(() => StringFilter)
  @Field(() => StringFilter, {
    nullable: true,
    description: undefined
  })
  questionId?: StringFilter | undefined;

  @ClassTransformer__Type(() => StringFilter)
  @Field(() => StringFilter, {
    nullable: true,
    description: undefined
  })
  desc?: StringFilter | undefined;
}
