import { Field, Float, ID, InputType, Int } from "@nestjs/graphql";
import GraphQLJSON from "graphql-type-json";
import { JsonValue, InputJsonValue } from "../../../../../node_modules/@prisma/client";
import { Type as ClassTransformer__Type } from "class-transformer";
import { AnswerValueCreateWithoutAnswerInput } from "../inputs/AnswerValueCreateWithoutAnswerInput";
import { AnswerValueUpdateWithoutAnswerDataInput } from "../inputs/AnswerValueUpdateWithoutAnswerDataInput";
import { AnswerValueUpsertWithoutAnswerInput } from "../inputs/AnswerValueUpsertWithoutAnswerInput";
import { AnswerValueWhereUniqueInput } from "../inputs/AnswerValueWhereUniqueInput";

@InputType({
  isAbstract: true,
  description: undefined,
})
export class AnswerValueUpdateOneRequiredWithoutAnswerInput {
  @ClassTransformer__Type(() => AnswerValueCreateWithoutAnswerInput)
  @Field(() => AnswerValueCreateWithoutAnswerInput, {
    nullable: true,
    description: undefined
  })
  create?: AnswerValueCreateWithoutAnswerInput | undefined;

  @ClassTransformer__Type(() => AnswerValueWhereUniqueInput)
  @Field(() => AnswerValueWhereUniqueInput, {
    nullable: true,
    description: undefined
  })
  connect?: AnswerValueWhereUniqueInput | undefined;

  @ClassTransformer__Type(() => AnswerValueUpdateWithoutAnswerDataInput)
  @Field(() => AnswerValueUpdateWithoutAnswerDataInput, {
    nullable: true,
    description: undefined
  })
  update?: AnswerValueUpdateWithoutAnswerDataInput | undefined;

  @ClassTransformer__Type(() => AnswerValueUpsertWithoutAnswerInput)
  @Field(() => AnswerValueUpsertWithoutAnswerInput, {
    nullable: true,
    description: undefined
  })
  upsert?: AnswerValueUpsertWithoutAnswerInput | undefined;
}
