import { Field, Float, ID, InputType, Int } from "@nestjs/graphql";
import GraphQLJSON from "graphql-type-json";
import { JsonValue, InputJsonValue } from "../../../../../node_modules/@prisma/client";
import { Type as ClassTransformer__Type } from "class-transformer";
import { AnswerUpdateManyWithoutQuestionInput } from "../inputs/AnswerUpdateManyWithoutQuestionInput";
import { AnswerValueUpdateManyWithoutQuestionInput } from "../inputs/AnswerValueUpdateManyWithoutQuestionInput";
import { EnumQuestionTypeFieldUpdateOperationsInput } from "../inputs/EnumQuestionTypeFieldUpdateOperationsInput";
import { FormUpdateOneRequiredWithoutQuestionsInput } from "../inputs/FormUpdateOneRequiredWithoutQuestionsInput";
import { StringFieldUpdateOperationsInput } from "../inputs/StringFieldUpdateOperationsInput";

@InputType({
  isAbstract: true,
  description: undefined,
})
export class QuestionUpdateInput {
  @ClassTransformer__Type(() => StringFieldUpdateOperationsInput)
  @Field(() => StringFieldUpdateOperationsInput, {
    nullable: true,
    description: undefined
  })
  id?: StringFieldUpdateOperationsInput | undefined;

  @ClassTransformer__Type(() => EnumQuestionTypeFieldUpdateOperationsInput)
  @Field(() => EnumQuestionTypeFieldUpdateOperationsInput, {
    nullable: true,
    description: undefined
  })
  type?: EnumQuestionTypeFieldUpdateOperationsInput | undefined;

  @ClassTransformer__Type(() => FormUpdateOneRequiredWithoutQuestionsInput)
  @Field(() => FormUpdateOneRequiredWithoutQuestionsInput, {
    nullable: true,
    description: undefined
  })
  Form?: FormUpdateOneRequiredWithoutQuestionsInput | undefined;

  @ClassTransformer__Type(() => AnswerValueUpdateManyWithoutQuestionInput)
  @Field(() => AnswerValueUpdateManyWithoutQuestionInput, {
    nullable: true,
    description: undefined
  })
  answerValues?: AnswerValueUpdateManyWithoutQuestionInput | undefined;

  @ClassTransformer__Type(() => AnswerUpdateManyWithoutQuestionInput)
  @Field(() => AnswerUpdateManyWithoutQuestionInput, {
    nullable: true,
    description: undefined
  })
  answer?: AnswerUpdateManyWithoutQuestionInput | undefined;
}
