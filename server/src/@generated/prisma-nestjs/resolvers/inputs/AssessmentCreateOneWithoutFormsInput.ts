import { Field, Float, ID, InputType, Int } from "@nestjs/graphql";
import GraphQLJSON from "graphql-type-json";
import { JsonValue, InputJsonValue } from "../../../../../node_modules/@prisma/client";
import { Type as ClassTransformer__Type } from "class-transformer";
import { AssessmentCreateWithoutFormsInput } from "../inputs/AssessmentCreateWithoutFormsInput";
import { AssessmentWhereUniqueInput } from "../inputs/AssessmentWhereUniqueInput";

@InputType({
  isAbstract: true,
  description: undefined,
})
export class AssessmentCreateOneWithoutFormsInput {
  @ClassTransformer__Type(() => AssessmentCreateWithoutFormsInput)
  @Field(() => AssessmentCreateWithoutFormsInput, {
    nullable: true,
    description: undefined
  })
  create?: AssessmentCreateWithoutFormsInput | undefined;

  @ClassTransformer__Type(() => AssessmentWhereUniqueInput)
  @Field(() => AssessmentWhereUniqueInput, {
    nullable: true,
    description: undefined
  })
  connect?: AssessmentWhereUniqueInput | undefined;
}
