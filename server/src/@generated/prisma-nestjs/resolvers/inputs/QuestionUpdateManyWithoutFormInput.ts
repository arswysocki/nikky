import { Field, Float, ID, InputType, Int } from "@nestjs/graphql";
import GraphQLJSON from "graphql-type-json";
import { JsonValue, InputJsonValue } from "../../../../../node_modules/@prisma/client";
import { Type as ClassTransformer__Type } from "class-transformer";
import { QuestionCreateWithoutFormInput } from "../inputs/QuestionCreateWithoutFormInput";
import { QuestionScalarWhereInput } from "../inputs/QuestionScalarWhereInput";
import { QuestionUpdateManyWithWhereNestedInput } from "../inputs/QuestionUpdateManyWithWhereNestedInput";
import { QuestionUpdateWithWhereUniqueWithoutFormInput } from "../inputs/QuestionUpdateWithWhereUniqueWithoutFormInput";
import { QuestionUpsertWithWhereUniqueWithoutFormInput } from "../inputs/QuestionUpsertWithWhereUniqueWithoutFormInput";
import { QuestionWhereUniqueInput } from "../inputs/QuestionWhereUniqueInput";

@InputType({
  isAbstract: true,
  description: undefined,
})
export class QuestionUpdateManyWithoutFormInput {
  @ClassTransformer__Type(() => QuestionCreateWithoutFormInput)
  @Field(() => [QuestionCreateWithoutFormInput], {
    nullable: true,
    description: undefined
  })
  create?: QuestionCreateWithoutFormInput[] | undefined;

  @ClassTransformer__Type(() => QuestionWhereUniqueInput)
  @Field(() => [QuestionWhereUniqueInput], {
    nullable: true,
    description: undefined
  })
  connect?: QuestionWhereUniqueInput[] | undefined;

  @ClassTransformer__Type(() => QuestionWhereUniqueInput)
  @Field(() => [QuestionWhereUniqueInput], {
    nullable: true,
    description: undefined
  })
  set?: QuestionWhereUniqueInput[] | undefined;

  @ClassTransformer__Type(() => QuestionWhereUniqueInput)
  @Field(() => [QuestionWhereUniqueInput], {
    nullable: true,
    description: undefined
  })
  disconnect?: QuestionWhereUniqueInput[] | undefined;

  @ClassTransformer__Type(() => QuestionWhereUniqueInput)
  @Field(() => [QuestionWhereUniqueInput], {
    nullable: true,
    description: undefined
  })
  delete?: QuestionWhereUniqueInput[] | undefined;

  @ClassTransformer__Type(() => QuestionUpdateWithWhereUniqueWithoutFormInput)
  @Field(() => [QuestionUpdateWithWhereUniqueWithoutFormInput], {
    nullable: true,
    description: undefined
  })
  update?: QuestionUpdateWithWhereUniqueWithoutFormInput[] | undefined;

  @ClassTransformer__Type(() => QuestionUpdateManyWithWhereNestedInput)
  @Field(() => [QuestionUpdateManyWithWhereNestedInput], {
    nullable: true,
    description: undefined
  })
  updateMany?: QuestionUpdateManyWithWhereNestedInput[] | undefined;

  @ClassTransformer__Type(() => QuestionScalarWhereInput)
  @Field(() => [QuestionScalarWhereInput], {
    nullable: true,
    description: undefined
  })
  deleteMany?: QuestionScalarWhereInput[] | undefined;

  @ClassTransformer__Type(() => QuestionUpsertWithWhereUniqueWithoutFormInput)
  @Field(() => [QuestionUpsertWithWhereUniqueWithoutFormInput], {
    nullable: true,
    description: undefined
  })
  upsert?: QuestionUpsertWithWhereUniqueWithoutFormInput[] | undefined;
}
