import { Field, Float, ID, InputType, Int } from "@nestjs/graphql";
import GraphQLJSON from "graphql-type-json";
import { JsonValue, InputJsonValue } from "../../../../../node_modules/@prisma/client";
import { Type as ClassTransformer__Type } from "class-transformer";
import { Gender } from "../../enums/Gender";

@InputType({
  isAbstract: true,
  description: undefined,
})
export class EnumGenderFieldUpdateOperationsInput {
  @Field(() => Gender, {
    nullable: true,
    description: undefined
  })
  set?: keyof typeof Gender | undefined;
}
