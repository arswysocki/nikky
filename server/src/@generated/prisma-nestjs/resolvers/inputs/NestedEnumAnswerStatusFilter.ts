import { Field, Float, ID, InputType, Int } from "@nestjs/graphql";
import GraphQLJSON from "graphql-type-json";
import { JsonValue, InputJsonValue } from "../../../../../node_modules/@prisma/client";
import { Type as ClassTransformer__Type } from "class-transformer";
import { AnswerStatus } from "../../enums/AnswerStatus";

@InputType({
  isAbstract: true,
  description: undefined,
})
export class NestedEnumAnswerStatusFilter {
  @Field(() => AnswerStatus, {
    nullable: true,
    description: undefined
  })
  equals?: keyof typeof AnswerStatus | undefined;

  @Field(() => [AnswerStatus], {
    nullable: true,
    description: undefined
  })
  in?: Array<keyof typeof AnswerStatus> | undefined;

  @Field(() => [AnswerStatus], {
    nullable: true,
    description: undefined
  })
  notIn?: Array<keyof typeof AnswerStatus> | undefined;

  @ClassTransformer__Type(() => NestedEnumAnswerStatusFilter)
  @Field(() => NestedEnumAnswerStatusFilter, {
    nullable: true,
    description: undefined
  })
  not?: NestedEnumAnswerStatusFilter | undefined;
}
