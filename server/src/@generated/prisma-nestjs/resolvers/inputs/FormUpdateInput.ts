import { Field, Float, ID, InputType, Int } from "@nestjs/graphql";
import GraphQLJSON from "graphql-type-json";
import { JsonValue, InputJsonValue } from "../../../../../node_modules/@prisma/client";
import { Type as ClassTransformer__Type } from "class-transformer";
import { AssessmentUpdateOneRequiredWithoutFormsInput } from "../inputs/AssessmentUpdateOneRequiredWithoutFormsInput";
import { QuestionUpdateManyWithoutFormInput } from "../inputs/QuestionUpdateManyWithoutFormInput";
import { StringFieldUpdateOperationsInput } from "../inputs/StringFieldUpdateOperationsInput";

@InputType({
  isAbstract: true,
  description: undefined,
})
export class FormUpdateInput {
  @ClassTransformer__Type(() => StringFieldUpdateOperationsInput)
  @Field(() => StringFieldUpdateOperationsInput, {
    nullable: true,
    description: undefined
  })
  id?: StringFieldUpdateOperationsInput | undefined;

  @ClassTransformer__Type(() => StringFieldUpdateOperationsInput)
  @Field(() => StringFieldUpdateOperationsInput, {
    nullable: true,
    description: undefined
  })
  desc?: StringFieldUpdateOperationsInput | undefined;

  @ClassTransformer__Type(() => QuestionUpdateManyWithoutFormInput)
  @Field(() => QuestionUpdateManyWithoutFormInput, {
    nullable: true,
    description: undefined
  })
  questions?: QuestionUpdateManyWithoutFormInput | undefined;

  @ClassTransformer__Type(() => AssessmentUpdateOneRequiredWithoutFormsInput)
  @Field(() => AssessmentUpdateOneRequiredWithoutFormsInput, {
    nullable: true,
    description: undefined
  })
  Assessment?: AssessmentUpdateOneRequiredWithoutFormsInput | undefined;
}
