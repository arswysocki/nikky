import { Field, Float, ID, InputType, Int } from "@nestjs/graphql";
import GraphQLJSON from "graphql-type-json";
import { JsonValue, InputJsonValue } from "../../../../../node_modules/@prisma/client";
import { Type as ClassTransformer__Type } from "class-transformer";
import { AnswerCreateWithoutUserInput } from "../inputs/AnswerCreateWithoutUserInput";
import { AnswerScalarWhereInput } from "../inputs/AnswerScalarWhereInput";
import { AnswerUpdateManyWithWhereNestedInput } from "../inputs/AnswerUpdateManyWithWhereNestedInput";
import { AnswerUpdateWithWhereUniqueWithoutUserInput } from "../inputs/AnswerUpdateWithWhereUniqueWithoutUserInput";
import { AnswerUpsertWithWhereUniqueWithoutUserInput } from "../inputs/AnswerUpsertWithWhereUniqueWithoutUserInput";
import { AnswerWhereUniqueInput } from "../inputs/AnswerWhereUniqueInput";

@InputType({
  isAbstract: true,
  description: undefined,
})
export class AnswerUpdateManyWithoutUserInput {
  @ClassTransformer__Type(() => AnswerCreateWithoutUserInput)
  @Field(() => [AnswerCreateWithoutUserInput], {
    nullable: true,
    description: undefined
  })
  create?: AnswerCreateWithoutUserInput[] | undefined;

  @ClassTransformer__Type(() => AnswerWhereUniqueInput)
  @Field(() => [AnswerWhereUniqueInput], {
    nullable: true,
    description: undefined
  })
  connect?: AnswerWhereUniqueInput[] | undefined;

  @ClassTransformer__Type(() => AnswerWhereUniqueInput)
  @Field(() => [AnswerWhereUniqueInput], {
    nullable: true,
    description: undefined
  })
  set?: AnswerWhereUniqueInput[] | undefined;

  @ClassTransformer__Type(() => AnswerWhereUniqueInput)
  @Field(() => [AnswerWhereUniqueInput], {
    nullable: true,
    description: undefined
  })
  disconnect?: AnswerWhereUniqueInput[] | undefined;

  @ClassTransformer__Type(() => AnswerWhereUniqueInput)
  @Field(() => [AnswerWhereUniqueInput], {
    nullable: true,
    description: undefined
  })
  delete?: AnswerWhereUniqueInput[] | undefined;

  @ClassTransformer__Type(() => AnswerUpdateWithWhereUniqueWithoutUserInput)
  @Field(() => [AnswerUpdateWithWhereUniqueWithoutUserInput], {
    nullable: true,
    description: undefined
  })
  update?: AnswerUpdateWithWhereUniqueWithoutUserInput[] | undefined;

  @ClassTransformer__Type(() => AnswerUpdateManyWithWhereNestedInput)
  @Field(() => [AnswerUpdateManyWithWhereNestedInput], {
    nullable: true,
    description: undefined
  })
  updateMany?: AnswerUpdateManyWithWhereNestedInput[] | undefined;

  @ClassTransformer__Type(() => AnswerScalarWhereInput)
  @Field(() => [AnswerScalarWhereInput], {
    nullable: true,
    description: undefined
  })
  deleteMany?: AnswerScalarWhereInput[] | undefined;

  @ClassTransformer__Type(() => AnswerUpsertWithWhereUniqueWithoutUserInput)
  @Field(() => [AnswerUpsertWithWhereUniqueWithoutUserInput], {
    nullable: true,
    description: undefined
  })
  upsert?: AnswerUpsertWithWhereUniqueWithoutUserInput[] | undefined;
}
