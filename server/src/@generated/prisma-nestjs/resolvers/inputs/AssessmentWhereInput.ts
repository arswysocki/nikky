import { Field, Float, ID, InputType, Int } from "@nestjs/graphql";
import GraphQLJSON from "graphql-type-json";
import { JsonValue, InputJsonValue } from "../../../../../node_modules/@prisma/client";
import { Type as ClassTransformer__Type } from "class-transformer";
import { FormListRelationFilter } from "../inputs/FormListRelationFilter";
import { ProjectWhereInput } from "../inputs/ProjectWhereInput";
import { StringFilter } from "../inputs/StringFilter";
import { UserWhereInput } from "../inputs/UserWhereInput";

@InputType({
  isAbstract: true,
  description: undefined,
})
export class AssessmentWhereInput {
  @ClassTransformer__Type(() => AssessmentWhereInput)
  @Field(() => [AssessmentWhereInput], {
    nullable: true,
    description: undefined
  })
  AND?: AssessmentWhereInput[] | undefined;

  @ClassTransformer__Type(() => AssessmentWhereInput)
  @Field(() => [AssessmentWhereInput], {
    nullable: true,
    description: undefined
  })
  OR?: AssessmentWhereInput[] | undefined;

  @ClassTransformer__Type(() => AssessmentWhereInput)
  @Field(() => [AssessmentWhereInput], {
    nullable: true,
    description: undefined
  })
  NOT?: AssessmentWhereInput[] | undefined;

  @ClassTransformer__Type(() => StringFilter)
  @Field(() => StringFilter, {
    nullable: true,
    description: undefined
  })
  id?: StringFilter | undefined;

  @ClassTransformer__Type(() => UserWhereInput)
  @Field(() => UserWhereInput, {
    nullable: true,
    description: undefined
  })
  user?: UserWhereInput | undefined;

  @ClassTransformer__Type(() => FormListRelationFilter)
  @Field(() => FormListRelationFilter, {
    nullable: true,
    description: undefined
  })
  forms?: FormListRelationFilter | undefined;

  @ClassTransformer__Type(() => StringFilter)
  @Field(() => StringFilter, {
    nullable: true,
    description: undefined
  })
  userId?: StringFilter | undefined;

  @ClassTransformer__Type(() => UserWhereInput)
  @Field(() => UserWhereInput, {
    nullable: true,
    description: undefined
  })
  recipient?: UserWhereInput | undefined;

  @ClassTransformer__Type(() => StringFilter)
  @Field(() => StringFilter, {
    nullable: true,
    description: undefined
  })
  recipientId?: StringFilter | undefined;

  @ClassTransformer__Type(() => ProjectWhereInput)
  @Field(() => ProjectWhereInput, {
    nullable: true,
    description: undefined
  })
  Project?: ProjectWhereInput | undefined;

  @ClassTransformer__Type(() => StringFilter)
  @Field(() => StringFilter, {
    nullable: true,
    description: undefined
  })
  projectId?: StringFilter | undefined;
}
