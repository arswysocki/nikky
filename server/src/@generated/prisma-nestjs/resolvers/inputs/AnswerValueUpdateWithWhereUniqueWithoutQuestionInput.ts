import { Field, Float, ID, InputType, Int } from "@nestjs/graphql";
import GraphQLJSON from "graphql-type-json";
import { JsonValue, InputJsonValue } from "../../../../../node_modules/@prisma/client";
import { Type as ClassTransformer__Type } from "class-transformer";
import { AnswerValueUpdateWithoutQuestionDataInput } from "../inputs/AnswerValueUpdateWithoutQuestionDataInput";
import { AnswerValueWhereUniqueInput } from "../inputs/AnswerValueWhereUniqueInput";

@InputType({
  isAbstract: true,
  description: undefined,
})
export class AnswerValueUpdateWithWhereUniqueWithoutQuestionInput {
  @ClassTransformer__Type(() => AnswerValueWhereUniqueInput)
  @Field(() => AnswerValueWhereUniqueInput, {
    nullable: false,
    description: undefined
  })
  where!: AnswerValueWhereUniqueInput;

  @ClassTransformer__Type(() => AnswerValueUpdateWithoutQuestionDataInput)
  @Field(() => AnswerValueUpdateWithoutQuestionDataInput, {
    nullable: false,
    description: undefined
  })
  data!: AnswerValueUpdateWithoutQuestionDataInput;
}
