import { Field, Float, ID, InputType, Int } from "@nestjs/graphql";
import GraphQLJSON from "graphql-type-json";
import { JsonValue, InputJsonValue } from "../../../../../node_modules/@prisma/client";
import { Type as ClassTransformer__Type } from "class-transformer";
import { UserCreateWithoutRecipientInput } from "../inputs/UserCreateWithoutRecipientInput";
import { UserUpdateWithoutRecipientDataInput } from "../inputs/UserUpdateWithoutRecipientDataInput";

@InputType({
  isAbstract: true,
  description: undefined,
})
export class UserUpsertWithoutRecipientInput {
  @ClassTransformer__Type(() => UserUpdateWithoutRecipientDataInput)
  @Field(() => UserUpdateWithoutRecipientDataInput, {
    nullable: false,
    description: undefined
  })
  update!: UserUpdateWithoutRecipientDataInput;

  @ClassTransformer__Type(() => UserCreateWithoutRecipientInput)
  @Field(() => UserCreateWithoutRecipientInput, {
    nullable: false,
    description: undefined
  })
  create!: UserCreateWithoutRecipientInput;
}
