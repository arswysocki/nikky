import { Field, Float, ID, InputType, Int } from "@nestjs/graphql";
import GraphQLJSON from "graphql-type-json";
import { JsonValue, InputJsonValue } from "../../../../../node_modules/@prisma/client";
import { Type as ClassTransformer__Type } from "class-transformer";
import { QuestionCreateWithoutAnswerInput } from "../inputs/QuestionCreateWithoutAnswerInput";
import { QuestionWhereUniqueInput } from "../inputs/QuestionWhereUniqueInput";

@InputType({
  isAbstract: true,
  description: undefined,
})
export class QuestionCreateOneWithoutAnswerInput {
  @ClassTransformer__Type(() => QuestionCreateWithoutAnswerInput)
  @Field(() => QuestionCreateWithoutAnswerInput, {
    nullable: true,
    description: undefined
  })
  create?: QuestionCreateWithoutAnswerInput | undefined;

  @ClassTransformer__Type(() => QuestionWhereUniqueInput)
  @Field(() => QuestionWhereUniqueInput, {
    nullable: true,
    description: undefined
  })
  connect?: QuestionWhereUniqueInput | undefined;
}
