import { Field, Float, ID, InputType, Int } from "@nestjs/graphql";
import GraphQLJSON from "graphql-type-json";
import { JsonValue, InputJsonValue } from "../../../../../node_modules/@prisma/client";
import { Type as ClassTransformer__Type } from "class-transformer";
import { AnswerListRelationFilter } from "../inputs/AnswerListRelationFilter";
import { AssessmentListRelationFilter } from "../inputs/AssessmentListRelationFilter";
import { EnumGenderFilter } from "../inputs/EnumGenderFilter";
import { EnumRoleFilter } from "../inputs/EnumRoleFilter";
import { StringFilter } from "../inputs/StringFilter";

@InputType({
  isAbstract: true,
  description: undefined,
})
export class UserWhereInput {
  @ClassTransformer__Type(() => UserWhereInput)
  @Field(() => [UserWhereInput], {
    nullable: true,
    description: undefined
  })
  AND?: UserWhereInput[] | undefined;

  @ClassTransformer__Type(() => UserWhereInput)
  @Field(() => [UserWhereInput], {
    nullable: true,
    description: undefined
  })
  OR?: UserWhereInput[] | undefined;

  @ClassTransformer__Type(() => UserWhereInput)
  @Field(() => [UserWhereInput], {
    nullable: true,
    description: undefined
  })
  NOT?: UserWhereInput[] | undefined;

  @ClassTransformer__Type(() => StringFilter)
  @Field(() => StringFilter, {
    nullable: true,
    description: undefined
  })
  id?: StringFilter | undefined;

  @ClassTransformer__Type(() => StringFilter)
  @Field(() => StringFilter, {
    nullable: true,
    description: undefined
  })
  firstName?: StringFilter | undefined;

  @ClassTransformer__Type(() => StringFilter)
  @Field(() => StringFilter, {
    nullable: true,
    description: undefined
  })
  lastName?: StringFilter | undefined;

  @ClassTransformer__Type(() => StringFilter)
  @Field(() => StringFilter, {
    nullable: true,
    description: undefined
  })
  email?: StringFilter | undefined;

  @ClassTransformer__Type(() => StringFilter)
  @Field(() => StringFilter, {
    nullable: true,
    description: undefined
  })
  picture?: StringFilter | undefined;

  @ClassTransformer__Type(() => EnumGenderFilter)
  @Field(() => EnumGenderFilter, {
    nullable: true,
    description: undefined
  })
  gender?: EnumGenderFilter | undefined;

  @ClassTransformer__Type(() => EnumRoleFilter)
  @Field(() => EnumRoleFilter, {
    nullable: true,
    description: undefined
  })
  role?: EnumRoleFilter | undefined;

  @ClassTransformer__Type(() => AssessmentListRelationFilter)
  @Field(() => AssessmentListRelationFilter, {
    nullable: true,
    description: undefined
  })
  Respondent?: AssessmentListRelationFilter | undefined;

  @ClassTransformer__Type(() => AssessmentListRelationFilter)
  @Field(() => AssessmentListRelationFilter, {
    nullable: true,
    description: undefined
  })
  Recipient?: AssessmentListRelationFilter | undefined;

  @ClassTransformer__Type(() => AnswerListRelationFilter)
  @Field(() => AnswerListRelationFilter, {
    nullable: true,
    description: undefined
  })
  Answer?: AnswerListRelationFilter | undefined;
}
