import { Field, Float, ID, InputType, Int } from "@nestjs/graphql";
import GraphQLJSON from "graphql-type-json";
import { JsonValue, InputJsonValue } from "../../../../../node_modules/@prisma/client";
import { Type as ClassTransformer__Type } from "class-transformer";
import { NestedEnumGenderFilter } from "../inputs/NestedEnumGenderFilter";
import { Gender } from "../../enums/Gender";

@InputType({
  isAbstract: true,
  description: undefined,
})
export class EnumGenderFilter {
  @Field(() => Gender, {
    nullable: true,
    description: undefined
  })
  equals?: keyof typeof Gender | undefined;

  @Field(() => [Gender], {
    nullable: true,
    description: undefined
  })
  in?: Array<keyof typeof Gender> | undefined;

  @Field(() => [Gender], {
    nullable: true,
    description: undefined
  })
  notIn?: Array<keyof typeof Gender> | undefined;

  @ClassTransformer__Type(() => NestedEnumGenderFilter)
  @Field(() => NestedEnumGenderFilter, {
    nullable: true,
    description: undefined
  })
  not?: NestedEnumGenderFilter | undefined;
}
