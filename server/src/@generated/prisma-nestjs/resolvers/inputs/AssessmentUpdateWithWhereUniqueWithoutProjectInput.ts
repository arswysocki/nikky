import { Field, Float, ID, InputType, Int } from "@nestjs/graphql";
import GraphQLJSON from "graphql-type-json";
import { JsonValue, InputJsonValue } from "../../../../../node_modules/@prisma/client";
import { Type as ClassTransformer__Type } from "class-transformer";
import { AssessmentUpdateWithoutProjectDataInput } from "../inputs/AssessmentUpdateWithoutProjectDataInput";
import { AssessmentWhereUniqueInput } from "../inputs/AssessmentWhereUniqueInput";

@InputType({
  isAbstract: true,
  description: undefined,
})
export class AssessmentUpdateWithWhereUniqueWithoutProjectInput {
  @ClassTransformer__Type(() => AssessmentWhereUniqueInput)
  @Field(() => AssessmentWhereUniqueInput, {
    nullable: false,
    description: undefined
  })
  where!: AssessmentWhereUniqueInput;

  @ClassTransformer__Type(() => AssessmentUpdateWithoutProjectDataInput)
  @Field(() => AssessmentUpdateWithoutProjectDataInput, {
    nullable: false,
    description: undefined
  })
  data!: AssessmentUpdateWithoutProjectDataInput;
}
