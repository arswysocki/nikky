import { Field, Float, ID, InputType, Int } from "@nestjs/graphql";
import GraphQLJSON from "graphql-type-json";
import { JsonValue, InputJsonValue } from "../../../../../node_modules/@prisma/client";
import { Type as ClassTransformer__Type } from "class-transformer";
import { EnumQuestionTypeFieldUpdateOperationsInput } from "../inputs/EnumQuestionTypeFieldUpdateOperationsInput";
import { StringFieldUpdateOperationsInput } from "../inputs/StringFieldUpdateOperationsInput";

@InputType({
  isAbstract: true,
  description: undefined,
})
export class QuestionUpdateManyDataInput {
  @ClassTransformer__Type(() => StringFieldUpdateOperationsInput)
  @Field(() => StringFieldUpdateOperationsInput, {
    nullable: true,
    description: undefined
  })
  id?: StringFieldUpdateOperationsInput | undefined;

  @ClassTransformer__Type(() => EnumQuestionTypeFieldUpdateOperationsInput)
  @Field(() => EnumQuestionTypeFieldUpdateOperationsInput, {
    nullable: true,
    description: undefined
  })
  type?: EnumQuestionTypeFieldUpdateOperationsInput | undefined;
}
