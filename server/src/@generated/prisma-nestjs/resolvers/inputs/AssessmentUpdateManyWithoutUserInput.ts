import { Field, Float, ID, InputType, Int } from "@nestjs/graphql";
import GraphQLJSON from "graphql-type-json";
import { JsonValue, InputJsonValue } from "../../../../../node_modules/@prisma/client";
import { Type as ClassTransformer__Type } from "class-transformer";
import { AssessmentCreateWithoutUserInput } from "../inputs/AssessmentCreateWithoutUserInput";
import { AssessmentScalarWhereInput } from "../inputs/AssessmentScalarWhereInput";
import { AssessmentUpdateManyWithWhereNestedInput } from "../inputs/AssessmentUpdateManyWithWhereNestedInput";
import { AssessmentUpdateWithWhereUniqueWithoutUserInput } from "../inputs/AssessmentUpdateWithWhereUniqueWithoutUserInput";
import { AssessmentUpsertWithWhereUniqueWithoutUserInput } from "../inputs/AssessmentUpsertWithWhereUniqueWithoutUserInput";
import { AssessmentWhereUniqueInput } from "../inputs/AssessmentWhereUniqueInput";

@InputType({
  isAbstract: true,
  description: undefined,
})
export class AssessmentUpdateManyWithoutUserInput {
  @ClassTransformer__Type(() => AssessmentCreateWithoutUserInput)
  @Field(() => [AssessmentCreateWithoutUserInput], {
    nullable: true,
    description: undefined
  })
  create?: AssessmentCreateWithoutUserInput[] | undefined;

  @ClassTransformer__Type(() => AssessmentWhereUniqueInput)
  @Field(() => [AssessmentWhereUniqueInput], {
    nullable: true,
    description: undefined
  })
  connect?: AssessmentWhereUniqueInput[] | undefined;

  @ClassTransformer__Type(() => AssessmentWhereUniqueInput)
  @Field(() => [AssessmentWhereUniqueInput], {
    nullable: true,
    description: undefined
  })
  set?: AssessmentWhereUniqueInput[] | undefined;

  @ClassTransformer__Type(() => AssessmentWhereUniqueInput)
  @Field(() => [AssessmentWhereUniqueInput], {
    nullable: true,
    description: undefined
  })
  disconnect?: AssessmentWhereUniqueInput[] | undefined;

  @ClassTransformer__Type(() => AssessmentWhereUniqueInput)
  @Field(() => [AssessmentWhereUniqueInput], {
    nullable: true,
    description: undefined
  })
  delete?: AssessmentWhereUniqueInput[] | undefined;

  @ClassTransformer__Type(() => AssessmentUpdateWithWhereUniqueWithoutUserInput)
  @Field(() => [AssessmentUpdateWithWhereUniqueWithoutUserInput], {
    nullable: true,
    description: undefined
  })
  update?: AssessmentUpdateWithWhereUniqueWithoutUserInput[] | undefined;

  @ClassTransformer__Type(() => AssessmentUpdateManyWithWhereNestedInput)
  @Field(() => [AssessmentUpdateManyWithWhereNestedInput], {
    nullable: true,
    description: undefined
  })
  updateMany?: AssessmentUpdateManyWithWhereNestedInput[] | undefined;

  @ClassTransformer__Type(() => AssessmentScalarWhereInput)
  @Field(() => [AssessmentScalarWhereInput], {
    nullable: true,
    description: undefined
  })
  deleteMany?: AssessmentScalarWhereInput[] | undefined;

  @ClassTransformer__Type(() => AssessmentUpsertWithWhereUniqueWithoutUserInput)
  @Field(() => [AssessmentUpsertWithWhereUniqueWithoutUserInput], {
    nullable: true,
    description: undefined
  })
  upsert?: AssessmentUpsertWithWhereUniqueWithoutUserInput[] | undefined;
}
