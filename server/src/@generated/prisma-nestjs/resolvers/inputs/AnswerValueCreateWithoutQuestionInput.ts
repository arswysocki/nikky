import { Field, Float, ID, InputType, Int } from "@nestjs/graphql";
import GraphQLJSON from "graphql-type-json";
import { JsonValue, InputJsonValue } from "../../../../../node_modules/@prisma/client";
import { Type as ClassTransformer__Type } from "class-transformer";
import { AnswerCreateOneWithoutAnswerValueInput } from "../inputs/AnswerCreateOneWithoutAnswerValueInput";

@InputType({
  isAbstract: true,
  description: undefined,
})
export class AnswerValueCreateWithoutQuestionInput {
  @Field(() => String, {
    nullable: true,
    description: undefined
  })
  id?: string | undefined;

  @Field(() => String, {
    nullable: false,
    description: undefined
  })
  desc!: string;

  @ClassTransformer__Type(() => AnswerCreateOneWithoutAnswerValueInput)
  @Field(() => AnswerCreateOneWithoutAnswerValueInput, {
    nullable: true,
    description: undefined
  })
  answer?: AnswerCreateOneWithoutAnswerValueInput | undefined;
}
