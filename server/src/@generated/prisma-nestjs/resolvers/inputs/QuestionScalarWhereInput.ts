import { Field, Float, ID, InputType, Int } from "@nestjs/graphql";
import GraphQLJSON from "graphql-type-json";
import { JsonValue, InputJsonValue } from "../../../../../node_modules/@prisma/client";
import { Type as ClassTransformer__Type } from "class-transformer";
import { EnumQuestionTypeFilter } from "../inputs/EnumQuestionTypeFilter";
import { StringFilter } from "../inputs/StringFilter";

@InputType({
  isAbstract: true,
  description: undefined,
})
export class QuestionScalarWhereInput {
  @ClassTransformer__Type(() => QuestionScalarWhereInput)
  @Field(() => [QuestionScalarWhereInput], {
    nullable: true,
    description: undefined
  })
  AND?: QuestionScalarWhereInput[] | undefined;

  @ClassTransformer__Type(() => QuestionScalarWhereInput)
  @Field(() => [QuestionScalarWhereInput], {
    nullable: true,
    description: undefined
  })
  OR?: QuestionScalarWhereInput[] | undefined;

  @ClassTransformer__Type(() => QuestionScalarWhereInput)
  @Field(() => [QuestionScalarWhereInput], {
    nullable: true,
    description: undefined
  })
  NOT?: QuestionScalarWhereInput[] | undefined;

  @ClassTransformer__Type(() => StringFilter)
  @Field(() => StringFilter, {
    nullable: true,
    description: undefined
  })
  id?: StringFilter | undefined;

  @ClassTransformer__Type(() => EnumQuestionTypeFilter)
  @Field(() => EnumQuestionTypeFilter, {
    nullable: true,
    description: undefined
  })
  type?: EnumQuestionTypeFilter | undefined;

  @ClassTransformer__Type(() => StringFilter)
  @Field(() => StringFilter, {
    nullable: true,
    description: undefined
  })
  formId?: StringFilter | undefined;
}
