import { Field, Float, ID, InputType, Int } from "@nestjs/graphql";
import GraphQLJSON from "graphql-type-json";
import { JsonValue, InputJsonValue } from "../../../../../node_modules/@prisma/client";
import { Type as ClassTransformer__Type } from "class-transformer";
import { FormCreateManyWithoutAssessmentInput } from "../inputs/FormCreateManyWithoutAssessmentInput";
import { ProjectCreateOneWithoutAssessmentsInput } from "../inputs/ProjectCreateOneWithoutAssessmentsInput";
import { UserCreateOneWithoutRespondentInput } from "../inputs/UserCreateOneWithoutRespondentInput";

@InputType({
  isAbstract: true,
  description: undefined,
})
export class AssessmentCreateWithoutRecipientInput {
  @Field(() => String, {
    nullable: true,
    description: undefined
  })
  id?: string | undefined;

  @ClassTransformer__Type(() => UserCreateOneWithoutRespondentInput)
  @Field(() => UserCreateOneWithoutRespondentInput, {
    nullable: false,
    description: undefined
  })
  user!: UserCreateOneWithoutRespondentInput;

  @ClassTransformer__Type(() => FormCreateManyWithoutAssessmentInput)
  @Field(() => FormCreateManyWithoutAssessmentInput, {
    nullable: true,
    description: undefined
  })
  forms?: FormCreateManyWithoutAssessmentInput | undefined;

  @ClassTransformer__Type(() => ProjectCreateOneWithoutAssessmentsInput)
  @Field(() => ProjectCreateOneWithoutAssessmentsInput, {
    nullable: false,
    description: undefined
  })
  Project!: ProjectCreateOneWithoutAssessmentsInput;
}
