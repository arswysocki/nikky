import { Field, Float, ID, InputType, Int } from "@nestjs/graphql";
import GraphQLJSON from "graphql-type-json";
import { JsonValue, InputJsonValue } from "../../../../../node_modules/@prisma/client";
import { Type as ClassTransformer__Type } from "class-transformer";
import { AnswerWhereInput } from "../inputs/AnswerWhereInput";

@InputType({
  isAbstract: true,
  description: undefined,
})
export class AnswerListRelationFilter {
  @ClassTransformer__Type(() => AnswerWhereInput)
  @Field(() => AnswerWhereInput, {
    nullable: true,
    description: undefined
  })
  every?: AnswerWhereInput | undefined;

  @ClassTransformer__Type(() => AnswerWhereInput)
  @Field(() => AnswerWhereInput, {
    nullable: true,
    description: undefined
  })
  some?: AnswerWhereInput | undefined;

  @ClassTransformer__Type(() => AnswerWhereInput)
  @Field(() => AnswerWhereInput, {
    nullable: true,
    description: undefined
  })
  none?: AnswerWhereInput | undefined;
}
