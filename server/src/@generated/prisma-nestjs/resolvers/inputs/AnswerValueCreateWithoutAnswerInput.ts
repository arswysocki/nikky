import { Field, Float, ID, InputType, Int } from "@nestjs/graphql";
import GraphQLJSON from "graphql-type-json";
import { JsonValue, InputJsonValue } from "../../../../../node_modules/@prisma/client";
import { Type as ClassTransformer__Type } from "class-transformer";
import { QuestionCreateOneWithoutAnswerValuesInput } from "../inputs/QuestionCreateOneWithoutAnswerValuesInput";

@InputType({
  isAbstract: true,
  description: undefined,
})
export class AnswerValueCreateWithoutAnswerInput {
  @Field(() => String, {
    nullable: true,
    description: undefined
  })
  id?: string | undefined;

  @Field(() => String, {
    nullable: false,
    description: undefined
  })
  desc!: string;

  @ClassTransformer__Type(() => QuestionCreateOneWithoutAnswerValuesInput)
  @Field(() => QuestionCreateOneWithoutAnswerValuesInput, {
    nullable: false,
    description: undefined
  })
  Question!: QuestionCreateOneWithoutAnswerValuesInput;
}
