import { Field, Float, ID, InputType, Int } from "@nestjs/graphql";
import GraphQLJSON from "graphql-type-json";
import { JsonValue, InputJsonValue } from "../../../../../node_modules/@prisma/client";
import { Type as ClassTransformer__Type } from "class-transformer";
import { QuestionWhereInput } from "../inputs/QuestionWhereInput";

@InputType({
  isAbstract: true,
  description: undefined,
})
export class QuestionRelationFilter {
  @ClassTransformer__Type(() => QuestionWhereInput)
  @Field(() => QuestionWhereInput, {
    nullable: true,
    description: undefined
  })
  is?: QuestionWhereInput | undefined;

  @ClassTransformer__Type(() => QuestionWhereInput)
  @Field(() => QuestionWhereInput, {
    nullable: true,
    description: undefined
  })
  isNot?: QuestionWhereInput | undefined;
}
