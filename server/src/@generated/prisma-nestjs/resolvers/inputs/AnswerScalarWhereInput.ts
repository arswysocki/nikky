import { Field, Float, ID, InputType, Int } from "@nestjs/graphql";
import GraphQLJSON from "graphql-type-json";
import { JsonValue, InputJsonValue } from "../../../../../node_modules/@prisma/client";
import { Type as ClassTransformer__Type } from "class-transformer";
import { BoolFilter } from "../inputs/BoolFilter";
import { EnumAnswerStatusFilter } from "../inputs/EnumAnswerStatusFilter";
import { StringFilter } from "../inputs/StringFilter";

@InputType({
  isAbstract: true,
  description: undefined,
})
export class AnswerScalarWhereInput {
  @ClassTransformer__Type(() => AnswerScalarWhereInput)
  @Field(() => [AnswerScalarWhereInput], {
    nullable: true,
    description: undefined
  })
  AND?: AnswerScalarWhereInput[] | undefined;

  @ClassTransformer__Type(() => AnswerScalarWhereInput)
  @Field(() => [AnswerScalarWhereInput], {
    nullable: true,
    description: undefined
  })
  OR?: AnswerScalarWhereInput[] | undefined;

  @ClassTransformer__Type(() => AnswerScalarWhereInput)
  @Field(() => [AnswerScalarWhereInput], {
    nullable: true,
    description: undefined
  })
  NOT?: AnswerScalarWhereInput[] | undefined;

  @ClassTransformer__Type(() => StringFilter)
  @Field(() => StringFilter, {
    nullable: true,
    description: undefined
  })
  id?: StringFilter | undefined;

  @ClassTransformer__Type(() => BoolFilter)
  @Field(() => BoolFilter, {
    nullable: true,
    description: undefined
  })
  canSkip?: BoolFilter | undefined;

  @ClassTransformer__Type(() => StringFilter)
  @Field(() => StringFilter, {
    nullable: true,
    description: undefined
  })
  userId?: StringFilter | undefined;

  @ClassTransformer__Type(() => EnumAnswerStatusFilter)
  @Field(() => EnumAnswerStatusFilter, {
    nullable: true,
    description: undefined
  })
  status?: EnumAnswerStatusFilter | undefined;

  @ClassTransformer__Type(() => BoolFilter)
  @Field(() => BoolFilter, {
    nullable: true,
    description: undefined
  })
  isAnonymous?: BoolFilter | undefined;

  @ClassTransformer__Type(() => StringFilter)
  @Field(() => StringFilter, {
    nullable: true,
    description: undefined
  })
  answerText?: StringFilter | undefined;

  @ClassTransformer__Type(() => StringFilter)
  @Field(() => StringFilter, {
    nullable: true,
    description: undefined
  })
  questionId?: StringFilter | undefined;

  @ClassTransformer__Type(() => StringFilter)
  @Field(() => StringFilter, {
    nullable: true,
    description: undefined
  })
  answerValueId?: StringFilter | undefined;
}
