import { Field, Float, ID, InputType, Int } from "@nestjs/graphql";
import GraphQLJSON from "graphql-type-json";
import { JsonValue, InputJsonValue } from "../../../../../node_modules/@prisma/client";
import { Type as ClassTransformer__Type } from "class-transformer";
import { ProjectUpdateOneRequiredWithoutAssessmentsInput } from "../inputs/ProjectUpdateOneRequiredWithoutAssessmentsInput";
import { StringFieldUpdateOperationsInput } from "../inputs/StringFieldUpdateOperationsInput";
import { UserUpdateOneRequiredWithoutRecipientInput } from "../inputs/UserUpdateOneRequiredWithoutRecipientInput";
import { UserUpdateOneRequiredWithoutRespondentInput } from "../inputs/UserUpdateOneRequiredWithoutRespondentInput";

@InputType({
  isAbstract: true,
  description: undefined,
})
export class AssessmentUpdateWithoutFormsDataInput {
  @ClassTransformer__Type(() => StringFieldUpdateOperationsInput)
  @Field(() => StringFieldUpdateOperationsInput, {
    nullable: true,
    description: undefined
  })
  id?: StringFieldUpdateOperationsInput | undefined;

  @ClassTransformer__Type(() => UserUpdateOneRequiredWithoutRespondentInput)
  @Field(() => UserUpdateOneRequiredWithoutRespondentInput, {
    nullable: true,
    description: undefined
  })
  user?: UserUpdateOneRequiredWithoutRespondentInput | undefined;

  @ClassTransformer__Type(() => UserUpdateOneRequiredWithoutRecipientInput)
  @Field(() => UserUpdateOneRequiredWithoutRecipientInput, {
    nullable: true,
    description: undefined
  })
  recipient?: UserUpdateOneRequiredWithoutRecipientInput | undefined;

  @ClassTransformer__Type(() => ProjectUpdateOneRequiredWithoutAssessmentsInput)
  @Field(() => ProjectUpdateOneRequiredWithoutAssessmentsInput, {
    nullable: true,
    description: undefined
  })
  Project?: ProjectUpdateOneRequiredWithoutAssessmentsInput | undefined;
}
