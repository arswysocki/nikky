import { Field, Float, ID, InputType, Int } from "@nestjs/graphql";
import GraphQLJSON from "graphql-type-json";
import { JsonValue, InputJsonValue } from "../../../../../node_modules/@prisma/client";
import { Type as ClassTransformer__Type } from "class-transformer";
import { AnswerCreateManyWithoutQuestionInput } from "../inputs/AnswerCreateManyWithoutQuestionInput";
import { AnswerValueCreateManyWithoutQuestionInput } from "../inputs/AnswerValueCreateManyWithoutQuestionInput";
import { QuestionType } from "../../enums/QuestionType";

@InputType({
  isAbstract: true,
  description: undefined,
})
export class QuestionCreateWithoutFormInput {
  @Field(() => String, {
    nullable: true,
    description: undefined
  })
  id?: string | undefined;

  @Field(() => QuestionType, {
    nullable: false,
    description: undefined
  })
  type!: keyof typeof QuestionType;

  @ClassTransformer__Type(() => AnswerValueCreateManyWithoutQuestionInput)
  @Field(() => AnswerValueCreateManyWithoutQuestionInput, {
    nullable: true,
    description: undefined
  })
  answerValues?: AnswerValueCreateManyWithoutQuestionInput | undefined;

  @ClassTransformer__Type(() => AnswerCreateManyWithoutQuestionInput)
  @Field(() => AnswerCreateManyWithoutQuestionInput, {
    nullable: true,
    description: undefined
  })
  answer?: AnswerCreateManyWithoutQuestionInput | undefined;
}
