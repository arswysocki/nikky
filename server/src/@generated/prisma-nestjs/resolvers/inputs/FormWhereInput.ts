import { Field, Float, ID, InputType, Int } from "@nestjs/graphql";
import GraphQLJSON from "graphql-type-json";
import { JsonValue, InputJsonValue } from "../../../../../node_modules/@prisma/client";
import { Type as ClassTransformer__Type } from "class-transformer";
import { AssessmentWhereInput } from "../inputs/AssessmentWhereInput";
import { QuestionListRelationFilter } from "../inputs/QuestionListRelationFilter";
import { StringFilter } from "../inputs/StringFilter";

@InputType({
  isAbstract: true,
  description: undefined,
})
export class FormWhereInput {
  @ClassTransformer__Type(() => FormWhereInput)
  @Field(() => [FormWhereInput], {
    nullable: true,
    description: undefined
  })
  AND?: FormWhereInput[] | undefined;

  @ClassTransformer__Type(() => FormWhereInput)
  @Field(() => [FormWhereInput], {
    nullable: true,
    description: undefined
  })
  OR?: FormWhereInput[] | undefined;

  @ClassTransformer__Type(() => FormWhereInput)
  @Field(() => [FormWhereInput], {
    nullable: true,
    description: undefined
  })
  NOT?: FormWhereInput[] | undefined;

  @ClassTransformer__Type(() => StringFilter)
  @Field(() => StringFilter, {
    nullable: true,
    description: undefined
  })
  id?: StringFilter | undefined;

  @ClassTransformer__Type(() => StringFilter)
  @Field(() => StringFilter, {
    nullable: true,
    description: undefined
  })
  desc?: StringFilter | undefined;

  @ClassTransformer__Type(() => QuestionListRelationFilter)
  @Field(() => QuestionListRelationFilter, {
    nullable: true,
    description: undefined
  })
  questions?: QuestionListRelationFilter | undefined;

  @ClassTransformer__Type(() => AssessmentWhereInput)
  @Field(() => AssessmentWhereInput, {
    nullable: true,
    description: undefined
  })
  Assessment?: AssessmentWhereInput | undefined;

  @ClassTransformer__Type(() => StringFilter)
  @Field(() => StringFilter, {
    nullable: true,
    description: undefined
  })
  assessmentId?: StringFilter | undefined;
}
