import { Args, Context, Info, Query, Mutation, Resolver } from "@nestjs/graphql";
import { DeleteManyFormArgs } from "./args/DeleteManyFormArgs";
import { Form } from "../../../models/Form";
import { plainToClass } from "class-transformer";
import { BatchPayload } from "../../outputs/BatchPayload";

@Resolver(() => Form)
export class DeleteManyFormResolver {
  @Mutation(() => BatchPayload, {
    nullable: false,
    description: undefined
  })
  async deleteManyForm(@Context() ctx: any, @Args() args: DeleteManyFormArgs): Promise<BatchPayload> {
    return plainToClass(BatchPayload, await ctx.prisma.form.deleteMany(args) as BatchPayload);
  }
}
