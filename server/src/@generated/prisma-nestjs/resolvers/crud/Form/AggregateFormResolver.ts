import { Args, Context, Info, Query, Mutation, Resolver } from "@nestjs/graphql";
import graphqlFields from "graphql-fields";
import { GraphQLResolveInfo } from "graphql";
import { AggregateFormArgs } from "./args/AggregateFormArgs";
import { Form } from "../../../models/Form";
import { plainToClass } from "class-transformer";
import { AggregateForm } from "../../outputs/AggregateForm";

@Resolver(() => Form)
export class AggregateFormResolver {
  @Query(() => AggregateForm, {
    nullable: false,
    description: undefined
  })
  async aggregateForm(@Context() ctx: any, @Info() info: GraphQLResolveInfo, @Args() args: AggregateFormArgs): Promise<AggregateForm> {
    const transformFields = (fields: Record<string, any>): Record<string, any> => {
      return Object.fromEntries(
        Object.entries(fields)
          .filter(([key, value]) => !key.startsWith("_"))
          .map<[string, any]>(([key, value]) => {
            if (Object.keys(value).length === 0) {
              return [key, true];
            }
            return [key, transformFields(value)];
          }),
      );
    }

    return ctx.prisma.form.aggregate({
      ...args,
      ...transformFields(graphqlFields(info as any)),
    });
  }
}
