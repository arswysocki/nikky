import { Args, Context, Info, Query, Mutation, Resolver } from "@nestjs/graphql";
import { UpdateFormArgs } from "./args/UpdateFormArgs";
import { Form } from "../../../models/Form";
import { plainToClass } from "class-transformer";

@Resolver(() => Form)
export class UpdateFormResolver {
  @Mutation(() => Form, {
    nullable: true,
    description: undefined
  })
  async updateForm(@Context() ctx: any, @Args() args: UpdateFormArgs): Promise<Form | undefined> {
    return plainToClass(Form, await ctx.prisma.form.update(args) as Form);
  }
}
