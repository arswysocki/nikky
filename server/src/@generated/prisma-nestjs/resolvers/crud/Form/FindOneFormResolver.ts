import { Args, Context, Info, Query, Mutation, Resolver } from "@nestjs/graphql";
import { FindOneFormArgs } from "./args/FindOneFormArgs";
import { Form } from "../../../models/Form";
import { plainToClass } from "class-transformer";

@Resolver(() => Form)
export class FindOneFormResolver {
  @Query(() => Form, {
    nullable: true,
    description: undefined
  })
  async form(@Context() ctx: any, @Args() args: FindOneFormArgs): Promise<Form | undefined> {
    return plainToClass(Form, await ctx.prisma.form.findOne(args) as Form);
  }
}
