import { Args, Context, Info, Query, Mutation, Resolver } from "@nestjs/graphql";
import { DeleteFormArgs } from "./args/DeleteFormArgs";
import { Form } from "../../../models/Form";
import { plainToClass } from "class-transformer";

@Resolver(() => Form)
export class DeleteFormResolver {
  @Mutation(() => Form, {
    nullable: true,
    description: undefined
  })
  async deleteForm(@Context() ctx: any, @Args() args: DeleteFormArgs): Promise<Form | undefined> {
    return plainToClass(Form, await ctx.prisma.form.delete(args) as Form);
  }
}
