import { Args, Context, Info, Query, Mutation, Resolver } from "@nestjs/graphql";
import { CreateFormArgs } from "./args/CreateFormArgs";
import { Form } from "../../../models/Form";
import { plainToClass } from "class-transformer";

@Resolver(() => Form)
export class CreateFormResolver {
  @Mutation(() => Form, {
    nullable: false,
    description: undefined
  })
  async createForm(@Context() ctx: any, @Args() args: CreateFormArgs): Promise<Form> {
    return plainToClass(Form, await ctx.prisma.form.create(args) as Form);
  }
}
