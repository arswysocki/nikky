import { Args, Context, Info, Query, Mutation, Resolver } from "@nestjs/graphql";
import { plainToClass } from "class-transformer";
import graphqlFields from "graphql-fields";
import { GraphQLResolveInfo } from "graphql";
import { AggregateFormArgs } from "./args/AggregateFormArgs";
import { CreateFormArgs } from "./args/CreateFormArgs";
import { DeleteFormArgs } from "./args/DeleteFormArgs";
import { DeleteManyFormArgs } from "./args/DeleteManyFormArgs";
import { FindManyFormArgs } from "./args/FindManyFormArgs";
import { FindOneFormArgs } from "./args/FindOneFormArgs";
import { UpdateFormArgs } from "./args/UpdateFormArgs";
import { UpdateManyFormArgs } from "./args/UpdateManyFormArgs";
import { UpsertFormArgs } from "./args/UpsertFormArgs";
import { Form } from "../../../models/Form";
import { AggregateForm } from "../../outputs/AggregateForm";
import { BatchPayload } from "../../outputs/BatchPayload";

@Resolver(() => Form)
export class FormCrudResolver {
  @Query(() => Form, {
    nullable: true,
    description: undefined
  })
  async form(@Context() ctx: any, @Args() args: FindOneFormArgs): Promise<Form | undefined> {
    return plainToClass(Form, await ctx.prisma.form.findOne(args) as Form);
  }

  @Query(() => [Form], {
    nullable: false,
    description: undefined
  })
  async forms(@Context() ctx: any, @Args() args: FindManyFormArgs): Promise<Form[]> {
    return plainToClass(Form, await ctx.prisma.form.findMany(args) as [Form]);
  }

  @Mutation(() => Form, {
    nullable: false,
    description: undefined
  })
  async createForm(@Context() ctx: any, @Args() args: CreateFormArgs): Promise<Form> {
    return plainToClass(Form, await ctx.prisma.form.create(args) as Form);
  }

  @Mutation(() => Form, {
    nullable: true,
    description: undefined
  })
  async deleteForm(@Context() ctx: any, @Args() args: DeleteFormArgs): Promise<Form | undefined> {
    return plainToClass(Form, await ctx.prisma.form.delete(args) as Form);
  }

  @Mutation(() => Form, {
    nullable: true,
    description: undefined
  })
  async updateForm(@Context() ctx: any, @Args() args: UpdateFormArgs): Promise<Form | undefined> {
    return plainToClass(Form, await ctx.prisma.form.update(args) as Form);
  }

  @Mutation(() => BatchPayload, {
    nullable: false,
    description: undefined
  })
  async deleteManyForm(@Context() ctx: any, @Args() args: DeleteManyFormArgs): Promise<BatchPayload> {
    return plainToClass(BatchPayload, await ctx.prisma.form.deleteMany(args) as BatchPayload);
  }

  @Mutation(() => BatchPayload, {
    nullable: false,
    description: undefined
  })
  async updateManyForm(@Context() ctx: any, @Args() args: UpdateManyFormArgs): Promise<BatchPayload> {
    return plainToClass(BatchPayload, await ctx.prisma.form.updateMany(args) as BatchPayload);
  }

  @Mutation(() => Form, {
    nullable: false,
    description: undefined
  })
  async upsertForm(@Context() ctx: any, @Args() args: UpsertFormArgs): Promise<Form> {
    return plainToClass(Form, await ctx.prisma.form.upsert(args) as Form);
  }

  @Query(() => AggregateForm, {
    nullable: false,
    description: undefined
  })
  async aggregateForm(@Context() ctx: any, @Info() info: GraphQLResolveInfo, @Args() args: AggregateFormArgs): Promise<AggregateForm> {
    const transformFields = (fields: Record<string, any>): Record<string, any> => {
      return Object.fromEntries(
        Object.entries(fields)
          .filter(([key, value]) => !key.startsWith("_"))
          .map<[string, any]>(([key, value]) => {
            if (Object.keys(value).length === 0) {
              return [key, true];
            }
            return [key, transformFields(value)];
          }),
      );
    }

    return ctx.prisma.form.aggregate({
      ...args,
      ...transformFields(graphqlFields(info as any)),
    });
  }
}
