import { ArgsType, Field, Int } from "@nestjs/graphql";
import { FormCreateInput } from "../../../inputs/FormCreateInput";
import { Type as ClassTransformer__Type } from "class-transformer";

@ArgsType()
export class CreateFormArgs {
  @ClassTransformer__Type(() => FormCreateInput)
  @Field(() => FormCreateInput, { nullable: false })
  data!: FormCreateInput;
}
