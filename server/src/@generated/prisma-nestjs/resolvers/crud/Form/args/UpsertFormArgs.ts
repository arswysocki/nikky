import { ArgsType, Field, Int } from "@nestjs/graphql";
import { FormCreateInput } from "../../../inputs/FormCreateInput";
import { FormUpdateInput } from "../../../inputs/FormUpdateInput";
import { FormWhereUniqueInput } from "../../../inputs/FormWhereUniqueInput";
import { Type as ClassTransformer__Type } from "class-transformer";

@ArgsType()
export class UpsertFormArgs {
  @ClassTransformer__Type(() => FormWhereUniqueInput)
  @Field(() => FormWhereUniqueInput, { nullable: false })
  where!: FormWhereUniqueInput;

  @ClassTransformer__Type(() => FormCreateInput)
  @Field(() => FormCreateInput, { nullable: false })
  create!: FormCreateInput;

  @ClassTransformer__Type(() => FormUpdateInput)
  @Field(() => FormUpdateInput, { nullable: false })
  update!: FormUpdateInput;
}
