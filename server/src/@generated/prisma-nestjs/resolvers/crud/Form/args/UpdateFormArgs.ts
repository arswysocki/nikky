import { ArgsType, Field, Int } from "@nestjs/graphql";
import { FormUpdateInput } from "../../../inputs/FormUpdateInput";
import { FormWhereUniqueInput } from "../../../inputs/FormWhereUniqueInput";
import { Type as ClassTransformer__Type } from "class-transformer";

@ArgsType()
export class UpdateFormArgs {
  @ClassTransformer__Type(() => FormUpdateInput)
  @Field(() => FormUpdateInput, { nullable: false })
  data!: FormUpdateInput;

  @ClassTransformer__Type(() => FormWhereUniqueInput)
  @Field(() => FormWhereUniqueInput, { nullable: false })
  where!: FormWhereUniqueInput;
}
