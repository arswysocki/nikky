import { ArgsType, Field, Int } from "@nestjs/graphql";
import { FormUpdateManyMutationInput } from "../../../inputs/FormUpdateManyMutationInput";
import { FormWhereInput } from "../../../inputs/FormWhereInput";
import { Type as ClassTransformer__Type } from "class-transformer";

@ArgsType()
export class UpdateManyFormArgs {
  @ClassTransformer__Type(() => FormUpdateManyMutationInput)
  @Field(() => FormUpdateManyMutationInput, { nullable: false })
  data!: FormUpdateManyMutationInput;

  @ClassTransformer__Type(() => FormWhereInput)
  @Field(() => FormWhereInput, { nullable: true })
  where?: FormWhereInput | undefined;
}
