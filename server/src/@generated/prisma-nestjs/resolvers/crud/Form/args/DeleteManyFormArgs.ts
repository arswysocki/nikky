import { ArgsType, Field, Int } from "@nestjs/graphql";
import { FormWhereInput } from "../../../inputs/FormWhereInput";
import { Type as ClassTransformer__Type } from "class-transformer";

@ArgsType()
export class DeleteManyFormArgs {
  @ClassTransformer__Type(() => FormWhereInput)
  @Field(() => FormWhereInput, { nullable: true })
  where?: FormWhereInput | undefined;
}
