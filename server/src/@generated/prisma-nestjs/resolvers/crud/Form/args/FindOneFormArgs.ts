import { ArgsType, Field, Int } from "@nestjs/graphql";
import { FormWhereUniqueInput } from "../../../inputs/FormWhereUniqueInput";
import { Type as ClassTransformer__Type } from "class-transformer";

@ArgsType()
export class FindOneFormArgs {
  @ClassTransformer__Type(() => FormWhereUniqueInput)
  @Field(() => FormWhereUniqueInput, { nullable: false })
  where!: FormWhereUniqueInput;
}
