import { Args, Context, Info, Query, Mutation, Resolver } from "@nestjs/graphql";
import { UpsertFormArgs } from "./args/UpsertFormArgs";
import { Form } from "../../../models/Form";
import { plainToClass } from "class-transformer";

@Resolver(() => Form)
export class UpsertFormResolver {
  @Mutation(() => Form, {
    nullable: false,
    description: undefined
  })
  async upsertForm(@Context() ctx: any, @Args() args: UpsertFormArgs): Promise<Form> {
    return plainToClass(Form, await ctx.prisma.form.upsert(args) as Form);
  }
}
