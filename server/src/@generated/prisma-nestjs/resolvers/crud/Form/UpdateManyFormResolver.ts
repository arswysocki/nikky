import { Args, Context, Info, Query, Mutation, Resolver } from "@nestjs/graphql";
import { UpdateManyFormArgs } from "./args/UpdateManyFormArgs";
import { Form } from "../../../models/Form";
import { plainToClass } from "class-transformer";
import { BatchPayload } from "../../outputs/BatchPayload";

@Resolver(() => Form)
export class UpdateManyFormResolver {
  @Mutation(() => BatchPayload, {
    nullable: false,
    description: undefined
  })
  async updateManyForm(@Context() ctx: any, @Args() args: UpdateManyFormArgs): Promise<BatchPayload> {
    return plainToClass(BatchPayload, await ctx.prisma.form.updateMany(args) as BatchPayload);
  }
}
