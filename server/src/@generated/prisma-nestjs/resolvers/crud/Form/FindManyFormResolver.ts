import { Args, Context, Info, Query, Mutation, Resolver } from "@nestjs/graphql";
import { FindManyFormArgs } from "./args/FindManyFormArgs";
import { Form } from "../../../models/Form";
import { plainToClass } from "class-transformer";

@Resolver(() => Form)
export class FindManyFormResolver {
  @Query(() => [Form], {
    nullable: false,
    description: undefined
  })
  async forms(@Context() ctx: any, @Args() args: FindManyFormArgs): Promise<Form[]> {
    return plainToClass(Form, await ctx.prisma.form.findMany(args) as [Form]);
  }
}
