import { Args, Context, Info, Query, Mutation, Resolver } from "@nestjs/graphql";
import { UpsertAnswerArgs } from "./args/UpsertAnswerArgs";
import { Answer } from "../../../models/Answer";
import { plainToClass } from "class-transformer";

@Resolver(() => Answer)
export class UpsertAnswerResolver {
  @Mutation(() => Answer, {
    nullable: false,
    description: undefined
  })
  async upsertAnswer(@Context() ctx: any, @Args() args: UpsertAnswerArgs): Promise<Answer> {
    return plainToClass(Answer, await ctx.prisma.answer.upsert(args) as Answer);
  }
}
