import { Args, Context, Info, Query, Mutation, Resolver } from "@nestjs/graphql";
import graphqlFields from "graphql-fields";
import { GraphQLResolveInfo } from "graphql";
import { AggregateAnswerArgs } from "./args/AggregateAnswerArgs";
import { Answer } from "../../../models/Answer";
import { plainToClass } from "class-transformer";
import { AggregateAnswer } from "../../outputs/AggregateAnswer";

@Resolver(() => Answer)
export class AggregateAnswerResolver {
  @Query(() => AggregateAnswer, {
    nullable: false,
    description: undefined
  })
  async aggregateAnswer(@Context() ctx: any, @Info() info: GraphQLResolveInfo, @Args() args: AggregateAnswerArgs): Promise<AggregateAnswer> {
    const transformFields = (fields: Record<string, any>): Record<string, any> => {
      return Object.fromEntries(
        Object.entries(fields)
          .filter(([key, value]) => !key.startsWith("_"))
          .map<[string, any]>(([key, value]) => {
            if (Object.keys(value).length === 0) {
              return [key, true];
            }
            return [key, transformFields(value)];
          }),
      );
    }

    return ctx.prisma.answer.aggregate({
      ...args,
      ...transformFields(graphqlFields(info as any)),
    });
  }
}
