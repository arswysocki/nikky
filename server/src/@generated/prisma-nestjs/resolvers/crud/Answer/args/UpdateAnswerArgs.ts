import { ArgsType, Field, Int } from "@nestjs/graphql";
import { AnswerUpdateInput } from "../../../inputs/AnswerUpdateInput";
import { AnswerWhereUniqueInput } from "../../../inputs/AnswerWhereUniqueInput";
import { Type as ClassTransformer__Type } from "class-transformer";

@ArgsType()
export class UpdateAnswerArgs {
  @ClassTransformer__Type(() => AnswerUpdateInput)
  @Field(() => AnswerUpdateInput, { nullable: false })
  data!: AnswerUpdateInput;

  @ClassTransformer__Type(() => AnswerWhereUniqueInput)
  @Field(() => AnswerWhereUniqueInput, { nullable: false })
  where!: AnswerWhereUniqueInput;
}
