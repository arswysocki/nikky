import { ArgsType, Field, Int } from "@nestjs/graphql";
import { AnswerCreateInput } from "../../../inputs/AnswerCreateInput";
import { AnswerUpdateInput } from "../../../inputs/AnswerUpdateInput";
import { AnswerWhereUniqueInput } from "../../../inputs/AnswerWhereUniqueInput";
import { Type as ClassTransformer__Type } from "class-transformer";

@ArgsType()
export class UpsertAnswerArgs {
  @ClassTransformer__Type(() => AnswerWhereUniqueInput)
  @Field(() => AnswerWhereUniqueInput, { nullable: false })
  where!: AnswerWhereUniqueInput;

  @ClassTransformer__Type(() => AnswerCreateInput)
  @Field(() => AnswerCreateInput, { nullable: false })
  create!: AnswerCreateInput;

  @ClassTransformer__Type(() => AnswerUpdateInput)
  @Field(() => AnswerUpdateInput, { nullable: false })
  update!: AnswerUpdateInput;
}
