import { ArgsType, Field, Int } from "@nestjs/graphql";
import { AnswerUpdateManyMutationInput } from "../../../inputs/AnswerUpdateManyMutationInput";
import { AnswerWhereInput } from "../../../inputs/AnswerWhereInput";
import { Type as ClassTransformer__Type } from "class-transformer";

@ArgsType()
export class UpdateManyAnswerArgs {
  @ClassTransformer__Type(() => AnswerUpdateManyMutationInput)
  @Field(() => AnswerUpdateManyMutationInput, { nullable: false })
  data!: AnswerUpdateManyMutationInput;

  @ClassTransformer__Type(() => AnswerWhereInput)
  @Field(() => AnswerWhereInput, { nullable: true })
  where?: AnswerWhereInput | undefined;
}
