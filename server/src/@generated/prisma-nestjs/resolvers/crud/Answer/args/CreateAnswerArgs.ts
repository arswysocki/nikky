import { ArgsType, Field, Int } from "@nestjs/graphql";
import { AnswerCreateInput } from "../../../inputs/AnswerCreateInput";
import { Type as ClassTransformer__Type } from "class-transformer";

@ArgsType()
export class CreateAnswerArgs {
  @ClassTransformer__Type(() => AnswerCreateInput)
  @Field(() => AnswerCreateInput, { nullable: false })
  data!: AnswerCreateInput;
}
