import { ArgsType, Field, Int } from "@nestjs/graphql";
import { AnswerWhereInput } from "../../../inputs/AnswerWhereInput";
import { Type as ClassTransformer__Type } from "class-transformer";

@ArgsType()
export class DeleteManyAnswerArgs {
  @ClassTransformer__Type(() => AnswerWhereInput)
  @Field(() => AnswerWhereInput, { nullable: true })
  where?: AnswerWhereInput | undefined;
}
