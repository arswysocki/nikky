import { ArgsType, Field, Int } from "@nestjs/graphql";
import { AnswerWhereUniqueInput } from "../../../inputs/AnswerWhereUniqueInput";
import { Type as ClassTransformer__Type } from "class-transformer";

@ArgsType()
export class FindOneAnswerArgs {
  @ClassTransformer__Type(() => AnswerWhereUniqueInput)
  @Field(() => AnswerWhereUniqueInput, { nullable: false })
  where!: AnswerWhereUniqueInput;
}
