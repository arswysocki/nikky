import { ArgsType, Field, Int } from "@nestjs/graphql";
import { AnswerOrderByInput } from "../../../inputs/AnswerOrderByInput";
import { AnswerWhereInput } from "../../../inputs/AnswerWhereInput";
import { AnswerWhereUniqueInput } from "../../../inputs/AnswerWhereUniqueInput";
import { Type as ClassTransformer__Type } from "class-transformer";
import { AnswerDistinctFieldEnum } from "../../../../enums/AnswerDistinctFieldEnum";

@ArgsType()
export class FindManyAnswerArgs {
  @ClassTransformer__Type(() => AnswerWhereInput)
  @Field(() => AnswerWhereInput, { nullable: true })
  where?: AnswerWhereInput | undefined;

  @ClassTransformer__Type(() => AnswerOrderByInput)
  @Field(() => [AnswerOrderByInput], { nullable: true })
  orderBy?: AnswerOrderByInput[] | undefined;

  @ClassTransformer__Type(() => AnswerWhereUniqueInput)
  @Field(() => AnswerWhereUniqueInput, { nullable: true })
  cursor?: AnswerWhereUniqueInput | undefined;

  @Field(() => Int, { nullable: true, defaultValue: 20 })
  take?: number | undefined;

  @Field(() => Int, { nullable: true })
  skip?: number | undefined;

  @Field(() => [AnswerDistinctFieldEnum], { nullable: true })
  distinct?: Array<keyof typeof AnswerDistinctFieldEnum> | undefined;
}
