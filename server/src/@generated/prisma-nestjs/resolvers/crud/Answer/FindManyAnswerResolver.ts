import { Args, Context, Info, Query, Mutation, Resolver } from "@nestjs/graphql";
import { FindManyAnswerArgs } from "./args/FindManyAnswerArgs";
import { Answer } from "../../../models/Answer";
import { plainToClass } from "class-transformer";

@Resolver(() => Answer)
export class FindManyAnswerResolver {
  @Query(() => [Answer], {
    nullable: false,
    description: undefined
  })
  async answers(@Context() ctx: any, @Args() args: FindManyAnswerArgs): Promise<Answer[]> {
    return plainToClass(Answer, await ctx.prisma.answer.findMany(args) as [Answer]);
  }
}
