import { Args, Context, Info, Query, Mutation, Resolver } from "@nestjs/graphql";
import { CreateAnswerArgs } from "./args/CreateAnswerArgs";
import { Answer } from "../../../models/Answer";
import { plainToClass } from "class-transformer";

@Resolver(() => Answer)
export class CreateAnswerResolver {
  @Mutation(() => Answer, {
    nullable: false,
    description: undefined
  })
  async createAnswer(@Context() ctx: any, @Args() args: CreateAnswerArgs): Promise<Answer> {
    return plainToClass(Answer, await ctx.prisma.answer.create(args) as Answer);
  }
}
