import { Args, Context, Info, Query, Mutation, Resolver } from "@nestjs/graphql";
import { plainToClass } from "class-transformer";
import graphqlFields from "graphql-fields";
import { GraphQLResolveInfo } from "graphql";
import { AggregateAnswerArgs } from "./args/AggregateAnswerArgs";
import { CreateAnswerArgs } from "./args/CreateAnswerArgs";
import { DeleteAnswerArgs } from "./args/DeleteAnswerArgs";
import { DeleteManyAnswerArgs } from "./args/DeleteManyAnswerArgs";
import { FindManyAnswerArgs } from "./args/FindManyAnswerArgs";
import { FindOneAnswerArgs } from "./args/FindOneAnswerArgs";
import { UpdateAnswerArgs } from "./args/UpdateAnswerArgs";
import { UpdateManyAnswerArgs } from "./args/UpdateManyAnswerArgs";
import { UpsertAnswerArgs } from "./args/UpsertAnswerArgs";
import { Answer } from "../../../models/Answer";
import { AggregateAnswer } from "../../outputs/AggregateAnswer";
import { BatchPayload } from "../../outputs/BatchPayload";

@Resolver(() => Answer)
export class AnswerCrudResolver {
  @Query(() => Answer, {
    nullable: true,
    description: undefined
  })
  async answer(@Context() ctx: any, @Args() args: FindOneAnswerArgs): Promise<Answer | undefined> {
    return plainToClass(Answer, await ctx.prisma.answer.findOne(args) as Answer);
  }

  @Query(() => [Answer], {
    nullable: false,
    description: undefined
  })
  async answers(@Context() ctx: any, @Args() args: FindManyAnswerArgs): Promise<Answer[]> {
    return plainToClass(Answer, await ctx.prisma.answer.findMany(args) as [Answer]);
  }

  @Mutation(() => Answer, {
    nullable: false,
    description: undefined
  })
  async createAnswer(@Context() ctx: any, @Args() args: CreateAnswerArgs): Promise<Answer> {
    return plainToClass(Answer, await ctx.prisma.answer.create(args) as Answer);
  }

  @Mutation(() => Answer, {
    nullable: true,
    description: undefined
  })
  async deleteAnswer(@Context() ctx: any, @Args() args: DeleteAnswerArgs): Promise<Answer | undefined> {
    return plainToClass(Answer, await ctx.prisma.answer.delete(args) as Answer);
  }

  @Mutation(() => Answer, {
    nullable: true,
    description: undefined
  })
  async updateAnswer(@Context() ctx: any, @Args() args: UpdateAnswerArgs): Promise<Answer | undefined> {
    return plainToClass(Answer, await ctx.prisma.answer.update(args) as Answer);
  }

  @Mutation(() => BatchPayload, {
    nullable: false,
    description: undefined
  })
  async deleteManyAnswer(@Context() ctx: any, @Args() args: DeleteManyAnswerArgs): Promise<BatchPayload> {
    return plainToClass(BatchPayload, await ctx.prisma.answer.deleteMany(args) as BatchPayload);
  }

  @Mutation(() => BatchPayload, {
    nullable: false,
    description: undefined
  })
  async updateManyAnswer(@Context() ctx: any, @Args() args: UpdateManyAnswerArgs): Promise<BatchPayload> {
    return plainToClass(BatchPayload, await ctx.prisma.answer.updateMany(args) as BatchPayload);
  }

  @Mutation(() => Answer, {
    nullable: false,
    description: undefined
  })
  async upsertAnswer(@Context() ctx: any, @Args() args: UpsertAnswerArgs): Promise<Answer> {
    return plainToClass(Answer, await ctx.prisma.answer.upsert(args) as Answer);
  }

  @Query(() => AggregateAnswer, {
    nullable: false,
    description: undefined
  })
  async aggregateAnswer(@Context() ctx: any, @Info() info: GraphQLResolveInfo, @Args() args: AggregateAnswerArgs): Promise<AggregateAnswer> {
    const transformFields = (fields: Record<string, any>): Record<string, any> => {
      return Object.fromEntries(
        Object.entries(fields)
          .filter(([key, value]) => !key.startsWith("_"))
          .map<[string, any]>(([key, value]) => {
            if (Object.keys(value).length === 0) {
              return [key, true];
            }
            return [key, transformFields(value)];
          }),
      );
    }

    return ctx.prisma.answer.aggregate({
      ...args,
      ...transformFields(graphqlFields(info as any)),
    });
  }
}
