import { Args, Context, Info, Query, Mutation, Resolver } from "@nestjs/graphql";
import { FindOneAnswerArgs } from "./args/FindOneAnswerArgs";
import { Answer } from "../../../models/Answer";
import { plainToClass } from "class-transformer";

@Resolver(() => Answer)
export class FindOneAnswerResolver {
  @Query(() => Answer, {
    nullable: true,
    description: undefined
  })
  async answer(@Context() ctx: any, @Args() args: FindOneAnswerArgs): Promise<Answer | undefined> {
    return plainToClass(Answer, await ctx.prisma.answer.findOne(args) as Answer);
  }
}
