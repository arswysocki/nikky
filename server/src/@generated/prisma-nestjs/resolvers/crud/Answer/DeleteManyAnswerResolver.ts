import { Args, Context, Info, Query, Mutation, Resolver } from "@nestjs/graphql";
import { DeleteManyAnswerArgs } from "./args/DeleteManyAnswerArgs";
import { Answer } from "../../../models/Answer";
import { plainToClass } from "class-transformer";
import { BatchPayload } from "../../outputs/BatchPayload";

@Resolver(() => Answer)
export class DeleteManyAnswerResolver {
  @Mutation(() => BatchPayload, {
    nullable: false,
    description: undefined
  })
  async deleteManyAnswer(@Context() ctx: any, @Args() args: DeleteManyAnswerArgs): Promise<BatchPayload> {
    return plainToClass(BatchPayload, await ctx.prisma.answer.deleteMany(args) as BatchPayload);
  }
}
