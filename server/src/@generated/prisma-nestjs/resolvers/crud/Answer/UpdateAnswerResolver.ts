import { Args, Context, Info, Query, Mutation, Resolver } from "@nestjs/graphql";
import { UpdateAnswerArgs } from "./args/UpdateAnswerArgs";
import { Answer } from "../../../models/Answer";
import { plainToClass } from "class-transformer";

@Resolver(() => Answer)
export class UpdateAnswerResolver {
  @Mutation(() => Answer, {
    nullable: true,
    description: undefined
  })
  async updateAnswer(@Context() ctx: any, @Args() args: UpdateAnswerArgs): Promise<Answer | undefined> {
    return plainToClass(Answer, await ctx.prisma.answer.update(args) as Answer);
  }
}
