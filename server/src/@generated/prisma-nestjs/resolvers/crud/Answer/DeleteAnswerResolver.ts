import { Args, Context, Info, Query, Mutation, Resolver } from "@nestjs/graphql";
import { DeleteAnswerArgs } from "./args/DeleteAnswerArgs";
import { Answer } from "../../../models/Answer";
import { plainToClass } from "class-transformer";

@Resolver(() => Answer)
export class DeleteAnswerResolver {
  @Mutation(() => Answer, {
    nullable: true,
    description: undefined
  })
  async deleteAnswer(@Context() ctx: any, @Args() args: DeleteAnswerArgs): Promise<Answer | undefined> {
    return plainToClass(Answer, await ctx.prisma.answer.delete(args) as Answer);
  }
}
