import { Args, Context, Info, Query, Mutation, Resolver } from "@nestjs/graphql";
import { UpdateManyAnswerArgs } from "./args/UpdateManyAnswerArgs";
import { Answer } from "../../../models/Answer";
import { plainToClass } from "class-transformer";
import { BatchPayload } from "../../outputs/BatchPayload";

@Resolver(() => Answer)
export class UpdateManyAnswerResolver {
  @Mutation(() => BatchPayload, {
    nullable: false,
    description: undefined
  })
  async updateManyAnswer(@Context() ctx: any, @Args() args: UpdateManyAnswerArgs): Promise<BatchPayload> {
    return plainToClass(BatchPayload, await ctx.prisma.answer.updateMany(args) as BatchPayload);
  }
}
