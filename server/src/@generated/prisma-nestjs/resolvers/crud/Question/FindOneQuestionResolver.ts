import { Args, Context, Info, Query, Mutation, Resolver } from "@nestjs/graphql";
import { FindOneQuestionArgs } from "./args/FindOneQuestionArgs";
import { Question } from "../../../models/Question";
import { plainToClass } from "class-transformer";

@Resolver(() => Question)
export class FindOneQuestionResolver {
  @Query(() => Question, {
    nullable: true,
    description: undefined
  })
  async question(@Context() ctx: any, @Args() args: FindOneQuestionArgs): Promise<Question | undefined> {
    return plainToClass(Question, await ctx.prisma.question.findOne(args) as Question);
  }
}
