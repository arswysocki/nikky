import { Args, Context, Info, Query, Mutation, Resolver } from "@nestjs/graphql";
import { CreateQuestionArgs } from "./args/CreateQuestionArgs";
import { Question } from "../../../models/Question";
import { plainToClass } from "class-transformer";

@Resolver(() => Question)
export class CreateQuestionResolver {
  @Mutation(() => Question, {
    nullable: false,
    description: undefined
  })
  async createQuestion(@Context() ctx: any, @Args() args: CreateQuestionArgs): Promise<Question> {
    return plainToClass(Question, await ctx.prisma.question.create(args) as Question);
  }
}
