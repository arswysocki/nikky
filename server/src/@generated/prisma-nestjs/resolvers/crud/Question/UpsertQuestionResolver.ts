import { Args, Context, Info, Query, Mutation, Resolver } from "@nestjs/graphql";
import { UpsertQuestionArgs } from "./args/UpsertQuestionArgs";
import { Question } from "../../../models/Question";
import { plainToClass } from "class-transformer";

@Resolver(() => Question)
export class UpsertQuestionResolver {
  @Mutation(() => Question, {
    nullable: false,
    description: undefined
  })
  async upsertQuestion(@Context() ctx: any, @Args() args: UpsertQuestionArgs): Promise<Question> {
    return plainToClass(Question, await ctx.prisma.question.upsert(args) as Question);
  }
}
