import { Args, Context, Info, Query, Mutation, Resolver } from "@nestjs/graphql";
import { UpdateQuestionArgs } from "./args/UpdateQuestionArgs";
import { Question } from "../../../models/Question";
import { plainToClass } from "class-transformer";

@Resolver(() => Question)
export class UpdateQuestionResolver {
  @Mutation(() => Question, {
    nullable: true,
    description: undefined
  })
  async updateQuestion(@Context() ctx: any, @Args() args: UpdateQuestionArgs): Promise<Question | undefined> {
    return plainToClass(Question, await ctx.prisma.question.update(args) as Question);
  }
}
