import { Args, Context, Info, Query, Mutation, Resolver } from "@nestjs/graphql";
import { FindManyQuestionArgs } from "./args/FindManyQuestionArgs";
import { Question } from "../../../models/Question";
import { plainToClass } from "class-transformer";

@Resolver(() => Question)
export class FindManyQuestionResolver {
  @Query(() => [Question], {
    nullable: false,
    description: undefined
  })
  async questions(@Context() ctx: any, @Args() args: FindManyQuestionArgs): Promise<Question[]> {
    return plainToClass(Question, await ctx.prisma.question.findMany(args) as [Question]);
  }
}
