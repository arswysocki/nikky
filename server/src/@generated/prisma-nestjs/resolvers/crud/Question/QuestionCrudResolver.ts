import { Args, Context, Info, Query, Mutation, Resolver } from "@nestjs/graphql";
import { plainToClass } from "class-transformer";
import graphqlFields from "graphql-fields";
import { GraphQLResolveInfo } from "graphql";
import { AggregateQuestionArgs } from "./args/AggregateQuestionArgs";
import { CreateQuestionArgs } from "./args/CreateQuestionArgs";
import { DeleteManyQuestionArgs } from "./args/DeleteManyQuestionArgs";
import { DeleteQuestionArgs } from "./args/DeleteQuestionArgs";
import { FindManyQuestionArgs } from "./args/FindManyQuestionArgs";
import { FindOneQuestionArgs } from "./args/FindOneQuestionArgs";
import { UpdateManyQuestionArgs } from "./args/UpdateManyQuestionArgs";
import { UpdateQuestionArgs } from "./args/UpdateQuestionArgs";
import { UpsertQuestionArgs } from "./args/UpsertQuestionArgs";
import { Question } from "../../../models/Question";
import { AggregateQuestion } from "../../outputs/AggregateQuestion";
import { BatchPayload } from "../../outputs/BatchPayload";

@Resolver(() => Question)
export class QuestionCrudResolver {
  @Query(() => Question, {
    nullable: true,
    description: undefined
  })
  async question(@Context() ctx: any, @Args() args: FindOneQuestionArgs): Promise<Question | undefined> {
    return plainToClass(Question, await ctx.prisma.question.findOne(args) as Question);
  }

  @Query(() => [Question], {
    nullable: false,
    description: undefined
  })
  async questions(@Context() ctx: any, @Args() args: FindManyQuestionArgs): Promise<Question[]> {
    return plainToClass(Question, await ctx.prisma.question.findMany(args) as [Question]);
  }

  @Mutation(() => Question, {
    nullable: false,
    description: undefined
  })
  async createQuestion(@Context() ctx: any, @Args() args: CreateQuestionArgs): Promise<Question> {
    return plainToClass(Question, await ctx.prisma.question.create(args) as Question);
  }

  @Mutation(() => Question, {
    nullable: true,
    description: undefined
  })
  async deleteQuestion(@Context() ctx: any, @Args() args: DeleteQuestionArgs): Promise<Question | undefined> {
    return plainToClass(Question, await ctx.prisma.question.delete(args) as Question);
  }

  @Mutation(() => Question, {
    nullable: true,
    description: undefined
  })
  async updateQuestion(@Context() ctx: any, @Args() args: UpdateQuestionArgs): Promise<Question | undefined> {
    return plainToClass(Question, await ctx.prisma.question.update(args) as Question);
  }

  @Mutation(() => BatchPayload, {
    nullable: false,
    description: undefined
  })
  async deleteManyQuestion(@Context() ctx: any, @Args() args: DeleteManyQuestionArgs): Promise<BatchPayload> {
    return plainToClass(BatchPayload, await ctx.prisma.question.deleteMany(args) as BatchPayload);
  }

  @Mutation(() => BatchPayload, {
    nullable: false,
    description: undefined
  })
  async updateManyQuestion(@Context() ctx: any, @Args() args: UpdateManyQuestionArgs): Promise<BatchPayload> {
    return plainToClass(BatchPayload, await ctx.prisma.question.updateMany(args) as BatchPayload);
  }

  @Mutation(() => Question, {
    nullable: false,
    description: undefined
  })
  async upsertQuestion(@Context() ctx: any, @Args() args: UpsertQuestionArgs): Promise<Question> {
    return plainToClass(Question, await ctx.prisma.question.upsert(args) as Question);
  }

  @Query(() => AggregateQuestion, {
    nullable: false,
    description: undefined
  })
  async aggregateQuestion(@Context() ctx: any, @Info() info: GraphQLResolveInfo, @Args() args: AggregateQuestionArgs): Promise<AggregateQuestion> {
    const transformFields = (fields: Record<string, any>): Record<string, any> => {
      return Object.fromEntries(
        Object.entries(fields)
          .filter(([key, value]) => !key.startsWith("_"))
          .map<[string, any]>(([key, value]) => {
            if (Object.keys(value).length === 0) {
              return [key, true];
            }
            return [key, transformFields(value)];
          }),
      );
    }

    return ctx.prisma.question.aggregate({
      ...args,
      ...transformFields(graphqlFields(info as any)),
    });
  }
}
