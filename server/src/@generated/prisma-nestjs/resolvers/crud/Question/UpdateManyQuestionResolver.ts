import { Args, Context, Info, Query, Mutation, Resolver } from "@nestjs/graphql";
import { UpdateManyQuestionArgs } from "./args/UpdateManyQuestionArgs";
import { Question } from "../../../models/Question";
import { plainToClass } from "class-transformer";
import { BatchPayload } from "../../outputs/BatchPayload";

@Resolver(() => Question)
export class UpdateManyQuestionResolver {
  @Mutation(() => BatchPayload, {
    nullable: false,
    description: undefined
  })
  async updateManyQuestion(@Context() ctx: any, @Args() args: UpdateManyQuestionArgs): Promise<BatchPayload> {
    return plainToClass(BatchPayload, await ctx.prisma.question.updateMany(args) as BatchPayload);
  }
}
