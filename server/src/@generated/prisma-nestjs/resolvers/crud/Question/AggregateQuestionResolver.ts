import { Args, Context, Info, Query, Mutation, Resolver } from "@nestjs/graphql";
import graphqlFields from "graphql-fields";
import { GraphQLResolveInfo } from "graphql";
import { AggregateQuestionArgs } from "./args/AggregateQuestionArgs";
import { Question } from "../../../models/Question";
import { plainToClass } from "class-transformer";
import { AggregateQuestion } from "../../outputs/AggregateQuestion";

@Resolver(() => Question)
export class AggregateQuestionResolver {
  @Query(() => AggregateQuestion, {
    nullable: false,
    description: undefined
  })
  async aggregateQuestion(@Context() ctx: any, @Info() info: GraphQLResolveInfo, @Args() args: AggregateQuestionArgs): Promise<AggregateQuestion> {
    const transformFields = (fields: Record<string, any>): Record<string, any> => {
      return Object.fromEntries(
        Object.entries(fields)
          .filter(([key, value]) => !key.startsWith("_"))
          .map<[string, any]>(([key, value]) => {
            if (Object.keys(value).length === 0) {
              return [key, true];
            }
            return [key, transformFields(value)];
          }),
      );
    }

    return ctx.prisma.question.aggregate({
      ...args,
      ...transformFields(graphqlFields(info as any)),
    });
  }
}
