import { ArgsType, Field, Int } from "@nestjs/graphql";
import { QuestionWhereUniqueInput } from "../../../inputs/QuestionWhereUniqueInput";
import { Type as ClassTransformer__Type } from "class-transformer";

@ArgsType()
export class FindOneQuestionArgs {
  @ClassTransformer__Type(() => QuestionWhereUniqueInput)
  @Field(() => QuestionWhereUniqueInput, { nullable: false })
  where!: QuestionWhereUniqueInput;
}
