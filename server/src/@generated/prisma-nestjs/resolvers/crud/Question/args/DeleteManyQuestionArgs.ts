import { ArgsType, Field, Int } from "@nestjs/graphql";
import { QuestionWhereInput } from "../../../inputs/QuestionWhereInput";
import { Type as ClassTransformer__Type } from "class-transformer";

@ArgsType()
export class DeleteManyQuestionArgs {
  @ClassTransformer__Type(() => QuestionWhereInput)
  @Field(() => QuestionWhereInput, { nullable: true })
  where?: QuestionWhereInput | undefined;
}
