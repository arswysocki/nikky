import { ArgsType, Field, Int } from "@nestjs/graphql";
import { QuestionCreateInput } from "../../../inputs/QuestionCreateInput";
import { Type as ClassTransformer__Type } from "class-transformer";

@ArgsType()
export class CreateQuestionArgs {
  @ClassTransformer__Type(() => QuestionCreateInput)
  @Field(() => QuestionCreateInput, { nullable: false })
  data!: QuestionCreateInput;
}
