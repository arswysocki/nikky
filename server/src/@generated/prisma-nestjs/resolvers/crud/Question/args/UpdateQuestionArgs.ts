import { ArgsType, Field, Int } from "@nestjs/graphql";
import { QuestionUpdateInput } from "../../../inputs/QuestionUpdateInput";
import { QuestionWhereUniqueInput } from "../../../inputs/QuestionWhereUniqueInput";
import { Type as ClassTransformer__Type } from "class-transformer";

@ArgsType()
export class UpdateQuestionArgs {
  @ClassTransformer__Type(() => QuestionUpdateInput)
  @Field(() => QuestionUpdateInput, { nullable: false })
  data!: QuestionUpdateInput;

  @ClassTransformer__Type(() => QuestionWhereUniqueInput)
  @Field(() => QuestionWhereUniqueInput, { nullable: false })
  where!: QuestionWhereUniqueInput;
}
