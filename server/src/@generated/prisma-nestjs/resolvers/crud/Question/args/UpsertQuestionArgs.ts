import { ArgsType, Field, Int } from "@nestjs/graphql";
import { QuestionCreateInput } from "../../../inputs/QuestionCreateInput";
import { QuestionUpdateInput } from "../../../inputs/QuestionUpdateInput";
import { QuestionWhereUniqueInput } from "../../../inputs/QuestionWhereUniqueInput";
import { Type as ClassTransformer__Type } from "class-transformer";

@ArgsType()
export class UpsertQuestionArgs {
  @ClassTransformer__Type(() => QuestionWhereUniqueInput)
  @Field(() => QuestionWhereUniqueInput, { nullable: false })
  where!: QuestionWhereUniqueInput;

  @ClassTransformer__Type(() => QuestionCreateInput)
  @Field(() => QuestionCreateInput, { nullable: false })
  create!: QuestionCreateInput;

  @ClassTransformer__Type(() => QuestionUpdateInput)
  @Field(() => QuestionUpdateInput, { nullable: false })
  update!: QuestionUpdateInput;
}
