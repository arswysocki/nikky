import { ArgsType, Field, Int } from "@nestjs/graphql";
import { QuestionUpdateManyMutationInput } from "../../../inputs/QuestionUpdateManyMutationInput";
import { QuestionWhereInput } from "../../../inputs/QuestionWhereInput";
import { Type as ClassTransformer__Type } from "class-transformer";

@ArgsType()
export class UpdateManyQuestionArgs {
  @ClassTransformer__Type(() => QuestionUpdateManyMutationInput)
  @Field(() => QuestionUpdateManyMutationInput, { nullable: false })
  data!: QuestionUpdateManyMutationInput;

  @ClassTransformer__Type(() => QuestionWhereInput)
  @Field(() => QuestionWhereInput, { nullable: true })
  where?: QuestionWhereInput | undefined;
}
