import { Args, Context, Info, Query, Mutation, Resolver } from "@nestjs/graphql";
import { DeleteManyQuestionArgs } from "./args/DeleteManyQuestionArgs";
import { Question } from "../../../models/Question";
import { plainToClass } from "class-transformer";
import { BatchPayload } from "../../outputs/BatchPayload";

@Resolver(() => Question)
export class DeleteManyQuestionResolver {
  @Mutation(() => BatchPayload, {
    nullable: false,
    description: undefined
  })
  async deleteManyQuestion(@Context() ctx: any, @Args() args: DeleteManyQuestionArgs): Promise<BatchPayload> {
    return plainToClass(BatchPayload, await ctx.prisma.question.deleteMany(args) as BatchPayload);
  }
}
