import { Args, Context, Info, Query, Mutation, Resolver } from "@nestjs/graphql";
import { DeleteQuestionArgs } from "./args/DeleteQuestionArgs";
import { Question } from "../../../models/Question";
import { plainToClass } from "class-transformer";

@Resolver(() => Question)
export class DeleteQuestionResolver {
  @Mutation(() => Question, {
    nullable: true,
    description: undefined
  })
  async deleteQuestion(@Context() ctx: any, @Args() args: DeleteQuestionArgs): Promise<Question | undefined> {
    return plainToClass(Question, await ctx.prisma.question.delete(args) as Question);
  }
}
