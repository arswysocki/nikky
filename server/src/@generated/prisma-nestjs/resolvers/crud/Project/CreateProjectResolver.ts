import { Args, Context, Info, Query, Mutation, Resolver } from "@nestjs/graphql";
import { CreateProjectArgs } from "./args/CreateProjectArgs";
import { Project } from "../../../models/Project";
import { plainToClass } from "class-transformer";

@Resolver(() => Project)
export class CreateProjectResolver {
  @Mutation(() => Project, {
    nullable: false,
    description: undefined
  })
  async createProject(@Context() ctx: any, @Args() args: CreateProjectArgs): Promise<Project> {
    return plainToClass(Project, await ctx.prisma.project.create(args) as Project);
  }
}
