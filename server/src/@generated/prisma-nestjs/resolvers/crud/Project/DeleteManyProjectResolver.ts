import { Args, Context, Info, Query, Mutation, Resolver } from "@nestjs/graphql";
import { DeleteManyProjectArgs } from "./args/DeleteManyProjectArgs";
import { Project } from "../../../models/Project";
import { plainToClass } from "class-transformer";
import { BatchPayload } from "../../outputs/BatchPayload";

@Resolver(() => Project)
export class DeleteManyProjectResolver {
  @Mutation(() => BatchPayload, {
    nullable: false,
    description: undefined
  })
  async deleteManyProject(@Context() ctx: any, @Args() args: DeleteManyProjectArgs): Promise<BatchPayload> {
    return plainToClass(BatchPayload, await ctx.prisma.project.deleteMany(args) as BatchPayload);
  }
}
