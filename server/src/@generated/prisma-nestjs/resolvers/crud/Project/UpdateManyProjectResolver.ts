import { Args, Context, Info, Query, Mutation, Resolver } from "@nestjs/graphql";
import { UpdateManyProjectArgs } from "./args/UpdateManyProjectArgs";
import { Project } from "../../../models/Project";
import { plainToClass } from "class-transformer";
import { BatchPayload } from "../../outputs/BatchPayload";

@Resolver(() => Project)
export class UpdateManyProjectResolver {
  @Mutation(() => BatchPayload, {
    nullable: false,
    description: undefined
  })
  async updateManyProject(@Context() ctx: any, @Args() args: UpdateManyProjectArgs): Promise<BatchPayload> {
    return plainToClass(BatchPayload, await ctx.prisma.project.updateMany(args) as BatchPayload);
  }
}
