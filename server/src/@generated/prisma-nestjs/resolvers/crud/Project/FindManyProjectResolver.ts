import { Args, Context, Info, Query, Mutation, Resolver } from "@nestjs/graphql";
import { FindManyProjectArgs } from "./args/FindManyProjectArgs";
import { Project } from "../../../models/Project";
import { plainToClass } from "class-transformer";

@Resolver(() => Project)
export class FindManyProjectResolver {
  @Query(() => [Project], {
    nullable: false,
    description: undefined
  })
  async projects(@Context() ctx: any, @Args() args: FindManyProjectArgs): Promise<Project[]> {
    return plainToClass(Project, await ctx.prisma.project.findMany(args) as [Project]);
  }
}
