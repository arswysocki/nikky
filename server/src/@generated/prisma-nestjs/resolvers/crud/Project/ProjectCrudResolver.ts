import { Args, Context, Info, Query, Mutation, Resolver } from "@nestjs/graphql";
import { plainToClass } from "class-transformer";
import graphqlFields from "graphql-fields";
import { GraphQLResolveInfo } from "graphql";
import { AggregateProjectArgs } from "./args/AggregateProjectArgs";
import { CreateProjectArgs } from "./args/CreateProjectArgs";
import { DeleteManyProjectArgs } from "./args/DeleteManyProjectArgs";
import { DeleteProjectArgs } from "./args/DeleteProjectArgs";
import { FindManyProjectArgs } from "./args/FindManyProjectArgs";
import { FindOneProjectArgs } from "./args/FindOneProjectArgs";
import { UpdateManyProjectArgs } from "./args/UpdateManyProjectArgs";
import { UpdateProjectArgs } from "./args/UpdateProjectArgs";
import { UpsertProjectArgs } from "./args/UpsertProjectArgs";
import { Project } from "../../../models/Project";
import { AggregateProject } from "../../outputs/AggregateProject";
import { BatchPayload } from "../../outputs/BatchPayload";

@Resolver(() => Project)
export class ProjectCrudResolver {
  @Query(() => Project, {
    nullable: true,
    description: undefined
  })
  async project(@Context() ctx: any, @Args() args: FindOneProjectArgs): Promise<Project | undefined> {
    return plainToClass(Project, await ctx.prisma.project.findOne(args) as Project);
  }

  @Query(() => [Project], {
    nullable: false,
    description: undefined
  })
  async projects(@Context() ctx: any, @Args() args: FindManyProjectArgs): Promise<Project[]> {
    return plainToClass(Project, await ctx.prisma.project.findMany(args) as [Project]);
  }

  @Mutation(() => Project, {
    nullable: false,
    description: undefined
  })
  async createProject(@Context() ctx: any, @Args() args: CreateProjectArgs): Promise<Project> {
    return plainToClass(Project, await ctx.prisma.project.create(args) as Project);
  }

  @Mutation(() => Project, {
    nullable: true,
    description: undefined
  })
  async deleteProject(@Context() ctx: any, @Args() args: DeleteProjectArgs): Promise<Project | undefined> {
    return plainToClass(Project, await ctx.prisma.project.delete(args) as Project);
  }

  @Mutation(() => Project, {
    nullable: true,
    description: undefined
  })
  async updateProject(@Context() ctx: any, @Args() args: UpdateProjectArgs): Promise<Project | undefined> {
    return plainToClass(Project, await ctx.prisma.project.update(args) as Project);
  }

  @Mutation(() => BatchPayload, {
    nullable: false,
    description: undefined
  })
  async deleteManyProject(@Context() ctx: any, @Args() args: DeleteManyProjectArgs): Promise<BatchPayload> {
    return plainToClass(BatchPayload, await ctx.prisma.project.deleteMany(args) as BatchPayload);
  }

  @Mutation(() => BatchPayload, {
    nullable: false,
    description: undefined
  })
  async updateManyProject(@Context() ctx: any, @Args() args: UpdateManyProjectArgs): Promise<BatchPayload> {
    return plainToClass(BatchPayload, await ctx.prisma.project.updateMany(args) as BatchPayload);
  }

  @Mutation(() => Project, {
    nullable: false,
    description: undefined
  })
  async upsertProject(@Context() ctx: any, @Args() args: UpsertProjectArgs): Promise<Project> {
    return plainToClass(Project, await ctx.prisma.project.upsert(args) as Project);
  }

  @Query(() => AggregateProject, {
    nullable: false,
    description: undefined
  })
  async aggregateProject(@Context() ctx: any, @Info() info: GraphQLResolveInfo, @Args() args: AggregateProjectArgs): Promise<AggregateProject> {
    const transformFields = (fields: Record<string, any>): Record<string, any> => {
      return Object.fromEntries(
        Object.entries(fields)
          .filter(([key, value]) => !key.startsWith("_"))
          .map<[string, any]>(([key, value]) => {
            if (Object.keys(value).length === 0) {
              return [key, true];
            }
            return [key, transformFields(value)];
          }),
      );
    }

    return ctx.prisma.project.aggregate({
      ...args,
      ...transformFields(graphqlFields(info as any)),
    });
  }
}
