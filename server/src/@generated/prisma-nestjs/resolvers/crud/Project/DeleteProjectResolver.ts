import { Args, Context, Info, Query, Mutation, Resolver } from "@nestjs/graphql";
import { DeleteProjectArgs } from "./args/DeleteProjectArgs";
import { Project } from "../../../models/Project";
import { plainToClass } from "class-transformer";

@Resolver(() => Project)
export class DeleteProjectResolver {
  @Mutation(() => Project, {
    nullable: true,
    description: undefined
  })
  async deleteProject(@Context() ctx: any, @Args() args: DeleteProjectArgs): Promise<Project | undefined> {
    return plainToClass(Project, await ctx.prisma.project.delete(args) as Project);
  }
}
