import { Args, Context, Info, Query, Mutation, Resolver } from "@nestjs/graphql";
import { UpdateProjectArgs } from "./args/UpdateProjectArgs";
import { Project } from "../../../models/Project";
import { plainToClass } from "class-transformer";

@Resolver(() => Project)
export class UpdateProjectResolver {
  @Mutation(() => Project, {
    nullable: true,
    description: undefined
  })
  async updateProject(@Context() ctx: any, @Args() args: UpdateProjectArgs): Promise<Project | undefined> {
    return plainToClass(Project, await ctx.prisma.project.update(args) as Project);
  }
}
