import { Args, Context, Info, Query, Mutation, Resolver } from "@nestjs/graphql";
import graphqlFields from "graphql-fields";
import { GraphQLResolveInfo } from "graphql";
import { AggregateProjectArgs } from "./args/AggregateProjectArgs";
import { Project } from "../../../models/Project";
import { plainToClass } from "class-transformer";
import { AggregateProject } from "../../outputs/AggregateProject";

@Resolver(() => Project)
export class AggregateProjectResolver {
  @Query(() => AggregateProject, {
    nullable: false,
    description: undefined
  })
  async aggregateProject(@Context() ctx: any, @Info() info: GraphQLResolveInfo, @Args() args: AggregateProjectArgs): Promise<AggregateProject> {
    const transformFields = (fields: Record<string, any>): Record<string, any> => {
      return Object.fromEntries(
        Object.entries(fields)
          .filter(([key, value]) => !key.startsWith("_"))
          .map<[string, any]>(([key, value]) => {
            if (Object.keys(value).length === 0) {
              return [key, true];
            }
            return [key, transformFields(value)];
          }),
      );
    }

    return ctx.prisma.project.aggregate({
      ...args,
      ...transformFields(graphqlFields(info as any)),
    });
  }
}
