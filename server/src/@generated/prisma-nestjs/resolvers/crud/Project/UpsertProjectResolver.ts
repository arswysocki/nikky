import { Args, Context, Info, Query, Mutation, Resolver } from "@nestjs/graphql";
import { UpsertProjectArgs } from "./args/UpsertProjectArgs";
import { Project } from "../../../models/Project";
import { plainToClass } from "class-transformer";

@Resolver(() => Project)
export class UpsertProjectResolver {
  @Mutation(() => Project, {
    nullable: false,
    description: undefined
  })
  async upsertProject(@Context() ctx: any, @Args() args: UpsertProjectArgs): Promise<Project> {
    return plainToClass(Project, await ctx.prisma.project.upsert(args) as Project);
  }
}
