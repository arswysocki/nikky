import { ArgsType, Field, Int } from "@nestjs/graphql";
import { ProjectWhereUniqueInput } from "../../../inputs/ProjectWhereUniqueInput";
import { Type as ClassTransformer__Type } from "class-transformer";

@ArgsType()
export class DeleteProjectArgs {
  @ClassTransformer__Type(() => ProjectWhereUniqueInput)
  @Field(() => ProjectWhereUniqueInput, { nullable: false })
  where!: ProjectWhereUniqueInput;
}
