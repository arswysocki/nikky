import { ArgsType, Field, Int } from "@nestjs/graphql";
import { ProjectUpdateManyMutationInput } from "../../../inputs/ProjectUpdateManyMutationInput";
import { ProjectWhereInput } from "../../../inputs/ProjectWhereInput";
import { Type as ClassTransformer__Type } from "class-transformer";

@ArgsType()
export class UpdateManyProjectArgs {
  @ClassTransformer__Type(() => ProjectUpdateManyMutationInput)
  @Field(() => ProjectUpdateManyMutationInput, { nullable: false })
  data!: ProjectUpdateManyMutationInput;

  @ClassTransformer__Type(() => ProjectWhereInput)
  @Field(() => ProjectWhereInput, { nullable: true })
  where?: ProjectWhereInput | undefined;
}
