import { ArgsType, Field, Int } from "@nestjs/graphql";
import { ProjectUpdateInput } from "../../../inputs/ProjectUpdateInput";
import { ProjectWhereUniqueInput } from "../../../inputs/ProjectWhereUniqueInput";
import { Type as ClassTransformer__Type } from "class-transformer";

@ArgsType()
export class UpdateProjectArgs {
  @ClassTransformer__Type(() => ProjectUpdateInput)
  @Field(() => ProjectUpdateInput, { nullable: false })
  data!: ProjectUpdateInput;

  @ClassTransformer__Type(() => ProjectWhereUniqueInput)
  @Field(() => ProjectWhereUniqueInput, { nullable: false })
  where!: ProjectWhereUniqueInput;
}
