import { ArgsType, Field, Int } from "@nestjs/graphql";
import { ProjectCreateInput } from "../../../inputs/ProjectCreateInput";
import { ProjectUpdateInput } from "../../../inputs/ProjectUpdateInput";
import { ProjectWhereUniqueInput } from "../../../inputs/ProjectWhereUniqueInput";
import { Type as ClassTransformer__Type } from "class-transformer";

@ArgsType()
export class UpsertProjectArgs {
  @ClassTransformer__Type(() => ProjectWhereUniqueInput)
  @Field(() => ProjectWhereUniqueInput, { nullable: false })
  where!: ProjectWhereUniqueInput;

  @ClassTransformer__Type(() => ProjectCreateInput)
  @Field(() => ProjectCreateInput, { nullable: false })
  create!: ProjectCreateInput;

  @ClassTransformer__Type(() => ProjectUpdateInput)
  @Field(() => ProjectUpdateInput, { nullable: false })
  update!: ProjectUpdateInput;
}
