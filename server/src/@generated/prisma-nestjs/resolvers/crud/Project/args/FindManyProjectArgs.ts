import { ArgsType, Field, Int } from "@nestjs/graphql";
import { ProjectOrderByInput } from "../../../inputs/ProjectOrderByInput";
import { ProjectWhereInput } from "../../../inputs/ProjectWhereInput";
import { ProjectWhereUniqueInput } from "../../../inputs/ProjectWhereUniqueInput";
import { Type as ClassTransformer__Type } from "class-transformer";
import { ProjectDistinctFieldEnum } from "../../../../enums/ProjectDistinctFieldEnum";

@ArgsType()
export class FindManyProjectArgs {
  @ClassTransformer__Type(() => ProjectWhereInput)
  @Field(() => ProjectWhereInput, { nullable: true })
  where?: ProjectWhereInput | undefined;

  @ClassTransformer__Type(() => ProjectOrderByInput)
  @Field(() => [ProjectOrderByInput], { nullable: true })
  orderBy?: ProjectOrderByInput[] | undefined;

  @ClassTransformer__Type(() => ProjectWhereUniqueInput)
  @Field(() => ProjectWhereUniqueInput, { nullable: true })
  cursor?: ProjectWhereUniqueInput | undefined;

  @Field(() => Int, { nullable: true, defaultValue: 20 })
  take?: number | undefined;

  @Field(() => Int, { nullable: true })
  skip?: number | undefined;

  @Field(() => [ProjectDistinctFieldEnum], { nullable: true })
  distinct?: Array<keyof typeof ProjectDistinctFieldEnum> | undefined;
}
