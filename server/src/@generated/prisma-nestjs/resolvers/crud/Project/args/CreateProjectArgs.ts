import { ArgsType, Field, Int } from "@nestjs/graphql";
import { ProjectCreateInput } from "../../../inputs/ProjectCreateInput";
import { Type as ClassTransformer__Type } from "class-transformer";

@ArgsType()
export class CreateProjectArgs {
  @ClassTransformer__Type(() => ProjectCreateInput)
  @Field(() => ProjectCreateInput, { nullable: false })
  data!: ProjectCreateInput;
}
