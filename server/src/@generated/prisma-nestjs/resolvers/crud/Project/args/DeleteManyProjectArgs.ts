import { ArgsType, Field, Int } from "@nestjs/graphql";
import { ProjectWhereInput } from "../../../inputs/ProjectWhereInput";
import { Type as ClassTransformer__Type } from "class-transformer";

@ArgsType()
export class DeleteManyProjectArgs {
  @ClassTransformer__Type(() => ProjectWhereInput)
  @Field(() => ProjectWhereInput, { nullable: true })
  where?: ProjectWhereInput | undefined;
}
