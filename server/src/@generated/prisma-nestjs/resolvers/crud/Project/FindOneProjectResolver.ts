import { Args, Context, Info, Query, Mutation, Resolver } from "@nestjs/graphql";
import { FindOneProjectArgs } from "./args/FindOneProjectArgs";
import { Project } from "../../../models/Project";
import { plainToClass } from "class-transformer";

@Resolver(() => Project)
export class FindOneProjectResolver {
  @Query(() => Project, {
    nullable: true,
    description: undefined
  })
  async project(@Context() ctx: any, @Args() args: FindOneProjectArgs): Promise<Project | undefined> {
    return plainToClass(Project, await ctx.prisma.project.findOne(args) as Project);
  }
}
