import { Args, Context, Info, Query, Mutation, Resolver } from "@nestjs/graphql";
import graphqlFields from "graphql-fields";
import { GraphQLResolveInfo } from "graphql";
import { AggregateAssessmentArgs } from "./args/AggregateAssessmentArgs";
import { Assessment } from "../../../models/Assessment";
import { plainToClass } from "class-transformer";
import { AggregateAssessment } from "../../outputs/AggregateAssessment";

@Resolver(() => Assessment)
export class AggregateAssessmentResolver {
  @Query(() => AggregateAssessment, {
    nullable: false,
    description: undefined
  })
  async aggregateAssessment(@Context() ctx: any, @Info() info: GraphQLResolveInfo, @Args() args: AggregateAssessmentArgs): Promise<AggregateAssessment> {
    const transformFields = (fields: Record<string, any>): Record<string, any> => {
      return Object.fromEntries(
        Object.entries(fields)
          .filter(([key, value]) => !key.startsWith("_"))
          .map<[string, any]>(([key, value]) => {
            if (Object.keys(value).length === 0) {
              return [key, true];
            }
            return [key, transformFields(value)];
          }),
      );
    }

    return ctx.prisma.assessment.aggregate({
      ...args,
      ...transformFields(graphqlFields(info as any)),
    });
  }
}
