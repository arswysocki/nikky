import { Args, Context, Info, Query, Mutation, Resolver } from "@nestjs/graphql";
import { DeleteAssessmentArgs } from "./args/DeleteAssessmentArgs";
import { Assessment } from "../../../models/Assessment";
import { plainToClass } from "class-transformer";

@Resolver(() => Assessment)
export class DeleteAssessmentResolver {
  @Mutation(() => Assessment, {
    nullable: true,
    description: undefined
  })
  async deleteAssessment(@Context() ctx: any, @Args() args: DeleteAssessmentArgs): Promise<Assessment | undefined> {
    return plainToClass(Assessment, await ctx.prisma.assessment.delete(args) as Assessment);
  }
}
