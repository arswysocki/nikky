import { Args, Context, Info, Query, Mutation, Resolver } from "@nestjs/graphql";
import { FindOneAssessmentArgs } from "./args/FindOneAssessmentArgs";
import { Assessment } from "../../../models/Assessment";
import { plainToClass } from "class-transformer";

@Resolver(() => Assessment)
export class FindOneAssessmentResolver {
  @Query(() => Assessment, {
    nullable: true,
    description: undefined
  })
  async assessment(@Context() ctx: any, @Args() args: FindOneAssessmentArgs): Promise<Assessment | undefined> {
    return plainToClass(Assessment, await ctx.prisma.assessment.findOne(args) as Assessment);
  }
}
