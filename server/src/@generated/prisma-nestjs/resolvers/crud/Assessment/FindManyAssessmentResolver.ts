import { Args, Context, Info, Query, Mutation, Resolver } from "@nestjs/graphql";
import { FindManyAssessmentArgs } from "./args/FindManyAssessmentArgs";
import { Assessment } from "../../../models/Assessment";
import { plainToClass } from "class-transformer";

@Resolver(() => Assessment)
export class FindManyAssessmentResolver {
  @Query(() => [Assessment], {
    nullable: false,
    description: undefined
  })
  async assessments(@Context() ctx: any, @Args() args: FindManyAssessmentArgs): Promise<Assessment[]> {
    return plainToClass(Assessment, await ctx.prisma.assessment.findMany(args) as [Assessment]);
  }
}
