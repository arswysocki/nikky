import { Args, Context, Info, Query, Mutation, Resolver } from "@nestjs/graphql";
import { plainToClass } from "class-transformer";
import graphqlFields from "graphql-fields";
import { GraphQLResolveInfo } from "graphql";
import { AggregateAssessmentArgs } from "./args/AggregateAssessmentArgs";
import { CreateAssessmentArgs } from "./args/CreateAssessmentArgs";
import { DeleteAssessmentArgs } from "./args/DeleteAssessmentArgs";
import { DeleteManyAssessmentArgs } from "./args/DeleteManyAssessmentArgs";
import { FindManyAssessmentArgs } from "./args/FindManyAssessmentArgs";
import { FindOneAssessmentArgs } from "./args/FindOneAssessmentArgs";
import { UpdateAssessmentArgs } from "./args/UpdateAssessmentArgs";
import { UpdateManyAssessmentArgs } from "./args/UpdateManyAssessmentArgs";
import { UpsertAssessmentArgs } from "./args/UpsertAssessmentArgs";
import { Assessment } from "../../../models/Assessment";
import { AggregateAssessment } from "../../outputs/AggregateAssessment";
import { BatchPayload } from "../../outputs/BatchPayload";

@Resolver(() => Assessment)
export class AssessmentCrudResolver {
  @Query(() => Assessment, {
    nullable: true,
    description: undefined
  })
  async assessment(@Context() ctx: any, @Args() args: FindOneAssessmentArgs): Promise<Assessment | undefined> {
    return plainToClass(Assessment, await ctx.prisma.assessment.findOne(args) as Assessment);
  }

  @Query(() => [Assessment], {
    nullable: false,
    description: undefined
  })
  async assessments(@Context() ctx: any, @Args() args: FindManyAssessmentArgs): Promise<Assessment[]> {
    return plainToClass(Assessment, await ctx.prisma.assessment.findMany(args) as [Assessment]);
  }

  @Mutation(() => Assessment, {
    nullable: false,
    description: undefined
  })
  async createAssessment(@Context() ctx: any, @Args() args: CreateAssessmentArgs): Promise<Assessment> {
    return plainToClass(Assessment, await ctx.prisma.assessment.create(args) as Assessment);
  }

  @Mutation(() => Assessment, {
    nullable: true,
    description: undefined
  })
  async deleteAssessment(@Context() ctx: any, @Args() args: DeleteAssessmentArgs): Promise<Assessment | undefined> {
    return plainToClass(Assessment, await ctx.prisma.assessment.delete(args) as Assessment);
  }

  @Mutation(() => Assessment, {
    nullable: true,
    description: undefined
  })
  async updateAssessment(@Context() ctx: any, @Args() args: UpdateAssessmentArgs): Promise<Assessment | undefined> {
    return plainToClass(Assessment, await ctx.prisma.assessment.update(args) as Assessment);
  }

  @Mutation(() => BatchPayload, {
    nullable: false,
    description: undefined
  })
  async deleteManyAssessment(@Context() ctx: any, @Args() args: DeleteManyAssessmentArgs): Promise<BatchPayload> {
    return plainToClass(BatchPayload, await ctx.prisma.assessment.deleteMany(args) as BatchPayload);
  }

  @Mutation(() => BatchPayload, {
    nullable: false,
    description: undefined
  })
  async updateManyAssessment(@Context() ctx: any, @Args() args: UpdateManyAssessmentArgs): Promise<BatchPayload> {
    return plainToClass(BatchPayload, await ctx.prisma.assessment.updateMany(args) as BatchPayload);
  }

  @Mutation(() => Assessment, {
    nullable: false,
    description: undefined
  })
  async upsertAssessment(@Context() ctx: any, @Args() args: UpsertAssessmentArgs): Promise<Assessment> {
    return plainToClass(Assessment, await ctx.prisma.assessment.upsert(args) as Assessment);
  }

  @Query(() => AggregateAssessment, {
    nullable: false,
    description: undefined
  })
  async aggregateAssessment(@Context() ctx: any, @Info() info: GraphQLResolveInfo, @Args() args: AggregateAssessmentArgs): Promise<AggregateAssessment> {
    const transformFields = (fields: Record<string, any>): Record<string, any> => {
      return Object.fromEntries(
        Object.entries(fields)
          .filter(([key, value]) => !key.startsWith("_"))
          .map<[string, any]>(([key, value]) => {
            if (Object.keys(value).length === 0) {
              return [key, true];
            }
            return [key, transformFields(value)];
          }),
      );
    }

    return ctx.prisma.assessment.aggregate({
      ...args,
      ...transformFields(graphqlFields(info as any)),
    });
  }
}
