import { Args, Context, Info, Query, Mutation, Resolver } from "@nestjs/graphql";
import { UpdateManyAssessmentArgs } from "./args/UpdateManyAssessmentArgs";
import { Assessment } from "../../../models/Assessment";
import { plainToClass } from "class-transformer";
import { BatchPayload } from "../../outputs/BatchPayload";

@Resolver(() => Assessment)
export class UpdateManyAssessmentResolver {
  @Mutation(() => BatchPayload, {
    nullable: false,
    description: undefined
  })
  async updateManyAssessment(@Context() ctx: any, @Args() args: UpdateManyAssessmentArgs): Promise<BatchPayload> {
    return plainToClass(BatchPayload, await ctx.prisma.assessment.updateMany(args) as BatchPayload);
  }
}
