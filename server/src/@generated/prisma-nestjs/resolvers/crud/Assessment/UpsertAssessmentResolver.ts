import { Args, Context, Info, Query, Mutation, Resolver } from "@nestjs/graphql";
import { UpsertAssessmentArgs } from "./args/UpsertAssessmentArgs";
import { Assessment } from "../../../models/Assessment";
import { plainToClass } from "class-transformer";

@Resolver(() => Assessment)
export class UpsertAssessmentResolver {
  @Mutation(() => Assessment, {
    nullable: false,
    description: undefined
  })
  async upsertAssessment(@Context() ctx: any, @Args() args: UpsertAssessmentArgs): Promise<Assessment> {
    return plainToClass(Assessment, await ctx.prisma.assessment.upsert(args) as Assessment);
  }
}
