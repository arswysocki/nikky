import { Args, Context, Info, Query, Mutation, Resolver } from "@nestjs/graphql";
import { CreateAssessmentArgs } from "./args/CreateAssessmentArgs";
import { Assessment } from "../../../models/Assessment";
import { plainToClass } from "class-transformer";

@Resolver(() => Assessment)
export class CreateAssessmentResolver {
  @Mutation(() => Assessment, {
    nullable: false,
    description: undefined
  })
  async createAssessment(@Context() ctx: any, @Args() args: CreateAssessmentArgs): Promise<Assessment> {
    return plainToClass(Assessment, await ctx.prisma.assessment.create(args) as Assessment);
  }
}
