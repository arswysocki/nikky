import { Args, Context, Info, Query, Mutation, Resolver } from "@nestjs/graphql";
import { DeleteManyAssessmentArgs } from "./args/DeleteManyAssessmentArgs";
import { Assessment } from "../../../models/Assessment";
import { plainToClass } from "class-transformer";
import { BatchPayload } from "../../outputs/BatchPayload";

@Resolver(() => Assessment)
export class DeleteManyAssessmentResolver {
  @Mutation(() => BatchPayload, {
    nullable: false,
    description: undefined
  })
  async deleteManyAssessment(@Context() ctx: any, @Args() args: DeleteManyAssessmentArgs): Promise<BatchPayload> {
    return plainToClass(BatchPayload, await ctx.prisma.assessment.deleteMany(args) as BatchPayload);
  }
}
