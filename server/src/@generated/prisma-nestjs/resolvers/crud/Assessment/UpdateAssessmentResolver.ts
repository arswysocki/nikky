import { Args, Context, Info, Query, Mutation, Resolver } from "@nestjs/graphql";
import { UpdateAssessmentArgs } from "./args/UpdateAssessmentArgs";
import { Assessment } from "../../../models/Assessment";
import { plainToClass } from "class-transformer";

@Resolver(() => Assessment)
export class UpdateAssessmentResolver {
  @Mutation(() => Assessment, {
    nullable: true,
    description: undefined
  })
  async updateAssessment(@Context() ctx: any, @Args() args: UpdateAssessmentArgs): Promise<Assessment | undefined> {
    return plainToClass(Assessment, await ctx.prisma.assessment.update(args) as Assessment);
  }
}
