import { ArgsType, Field, Int } from "@nestjs/graphql";
import { AssessmentUpdateInput } from "../../../inputs/AssessmentUpdateInput";
import { AssessmentWhereUniqueInput } from "../../../inputs/AssessmentWhereUniqueInput";
import { Type as ClassTransformer__Type } from "class-transformer";

@ArgsType()
export class UpdateAssessmentArgs {
  @ClassTransformer__Type(() => AssessmentUpdateInput)
  @Field(() => AssessmentUpdateInput, { nullable: false })
  data!: AssessmentUpdateInput;

  @ClassTransformer__Type(() => AssessmentWhereUniqueInput)
  @Field(() => AssessmentWhereUniqueInput, { nullable: false })
  where!: AssessmentWhereUniqueInput;
}
