import { ArgsType, Field, Int } from "@nestjs/graphql";
import { AssessmentUpdateManyMutationInput } from "../../../inputs/AssessmentUpdateManyMutationInput";
import { AssessmentWhereInput } from "../../../inputs/AssessmentWhereInput";
import { Type as ClassTransformer__Type } from "class-transformer";

@ArgsType()
export class UpdateManyAssessmentArgs {
  @ClassTransformer__Type(() => AssessmentUpdateManyMutationInput)
  @Field(() => AssessmentUpdateManyMutationInput, { nullable: false })
  data!: AssessmentUpdateManyMutationInput;

  @ClassTransformer__Type(() => AssessmentWhereInput)
  @Field(() => AssessmentWhereInput, { nullable: true })
  where?: AssessmentWhereInput | undefined;
}
