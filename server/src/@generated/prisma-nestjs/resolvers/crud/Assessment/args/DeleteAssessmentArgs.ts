import { ArgsType, Field, Int } from "@nestjs/graphql";
import { AssessmentWhereUniqueInput } from "../../../inputs/AssessmentWhereUniqueInput";
import { Type as ClassTransformer__Type } from "class-transformer";

@ArgsType()
export class DeleteAssessmentArgs {
  @ClassTransformer__Type(() => AssessmentWhereUniqueInput)
  @Field(() => AssessmentWhereUniqueInput, { nullable: false })
  where!: AssessmentWhereUniqueInput;
}
