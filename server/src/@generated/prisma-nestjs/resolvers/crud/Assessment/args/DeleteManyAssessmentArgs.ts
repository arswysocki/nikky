import { ArgsType, Field, Int } from "@nestjs/graphql";
import { AssessmentWhereInput } from "../../../inputs/AssessmentWhereInput";
import { Type as ClassTransformer__Type } from "class-transformer";

@ArgsType()
export class DeleteManyAssessmentArgs {
  @ClassTransformer__Type(() => AssessmentWhereInput)
  @Field(() => AssessmentWhereInput, { nullable: true })
  where?: AssessmentWhereInput | undefined;
}
