import { ArgsType, Field, Int } from "@nestjs/graphql";
import { AssessmentCreateInput } from "../../../inputs/AssessmentCreateInput";
import { Type as ClassTransformer__Type } from "class-transformer";

@ArgsType()
export class CreateAssessmentArgs {
  @ClassTransformer__Type(() => AssessmentCreateInput)
  @Field(() => AssessmentCreateInput, { nullable: false })
  data!: AssessmentCreateInput;
}
