import { ArgsType, Field, Int } from "@nestjs/graphql";
import { AssessmentCreateInput } from "../../../inputs/AssessmentCreateInput";
import { AssessmentUpdateInput } from "../../../inputs/AssessmentUpdateInput";
import { AssessmentWhereUniqueInput } from "../../../inputs/AssessmentWhereUniqueInput";
import { Type as ClassTransformer__Type } from "class-transformer";

@ArgsType()
export class UpsertAssessmentArgs {
  @ClassTransformer__Type(() => AssessmentWhereUniqueInput)
  @Field(() => AssessmentWhereUniqueInput, { nullable: false })
  where!: AssessmentWhereUniqueInput;

  @ClassTransformer__Type(() => AssessmentCreateInput)
  @Field(() => AssessmentCreateInput, { nullable: false })
  create!: AssessmentCreateInput;

  @ClassTransformer__Type(() => AssessmentUpdateInput)
  @Field(() => AssessmentUpdateInput, { nullable: false })
  update!: AssessmentUpdateInput;
}
