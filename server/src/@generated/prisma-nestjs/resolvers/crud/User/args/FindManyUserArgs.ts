import { ArgsType, Field, Int } from "@nestjs/graphql";
import { UserOrderByInput } from "../../../inputs/UserOrderByInput";
import { UserWhereInput } from "../../../inputs/UserWhereInput";
import { UserWhereUniqueInput } from "../../../inputs/UserWhereUniqueInput";
import { Type as ClassTransformer__Type } from "class-transformer";
import { UserDistinctFieldEnum } from "../../../../enums/UserDistinctFieldEnum";

@ArgsType()
export class FindManyUserArgs {
  @ClassTransformer__Type(() => UserWhereInput)
  @Field(() => UserWhereInput, { nullable: true })
  where?: UserWhereInput | undefined;

  @ClassTransformer__Type(() => UserOrderByInput)
  @Field(() => [UserOrderByInput], { nullable: true })
  orderBy?: UserOrderByInput[] | undefined;

  @ClassTransformer__Type(() => UserWhereUniqueInput)
  @Field(() => UserWhereUniqueInput, { nullable: true })
  cursor?: UserWhereUniqueInput | undefined;

  @Field(() => Int, { nullable: true, defaultValue: 20 })
  take?: number | undefined;

  @Field(() => Int, { nullable: true })
  skip?: number | undefined;

  @Field(() => [UserDistinctFieldEnum], { nullable: true })
  distinct?: Array<keyof typeof UserDistinctFieldEnum> | undefined;
}
