import { Args, Context, Info, Query, Mutation, Resolver } from "@nestjs/graphql";
import { UpdateAnswerValueArgs } from "./args/UpdateAnswerValueArgs";
import { AnswerValue } from "../../../models/AnswerValue";
import { plainToClass } from "class-transformer";

@Resolver(() => AnswerValue)
export class UpdateAnswerValueResolver {
  @Mutation(() => AnswerValue, {
    nullable: true,
    description: undefined
  })
  async updateAnswerValue(@Context() ctx: any, @Args() args: UpdateAnswerValueArgs): Promise<AnswerValue | undefined> {
    return plainToClass(AnswerValue, await ctx.prisma.answerValue.update(args) as AnswerValue);
  }
}
