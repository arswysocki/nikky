import { Args, Context, Info, Query, Mutation, Resolver } from "@nestjs/graphql";
import graphqlFields from "graphql-fields";
import { GraphQLResolveInfo } from "graphql";
import { AggregateAnswerValueArgs } from "./args/AggregateAnswerValueArgs";
import { AnswerValue } from "../../../models/AnswerValue";
import { plainToClass } from "class-transformer";
import { AggregateAnswerValue } from "../../outputs/AggregateAnswerValue";

@Resolver(() => AnswerValue)
export class AggregateAnswerValueResolver {
  @Query(() => AggregateAnswerValue, {
    nullable: false,
    description: undefined
  })
  async aggregateAnswerValue(@Context() ctx: any, @Info() info: GraphQLResolveInfo, @Args() args: AggregateAnswerValueArgs): Promise<AggregateAnswerValue> {
    const transformFields = (fields: Record<string, any>): Record<string, any> => {
      return Object.fromEntries(
        Object.entries(fields)
          .filter(([key, value]) => !key.startsWith("_"))
          .map<[string, any]>(([key, value]) => {
            if (Object.keys(value).length === 0) {
              return [key, true];
            }
            return [key, transformFields(value)];
          }),
      );
    }

    return ctx.prisma.answerValue.aggregate({
      ...args,
      ...transformFields(graphqlFields(info as any)),
    });
  }
}
