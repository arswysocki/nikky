import { Args, Context, Info, Query, Mutation, Resolver } from "@nestjs/graphql";
import { DeleteManyAnswerValueArgs } from "./args/DeleteManyAnswerValueArgs";
import { AnswerValue } from "../../../models/AnswerValue";
import { plainToClass } from "class-transformer";
import { BatchPayload } from "../../outputs/BatchPayload";

@Resolver(() => AnswerValue)
export class DeleteManyAnswerValueResolver {
  @Mutation(() => BatchPayload, {
    nullable: false,
    description: undefined
  })
  async deleteManyAnswerValue(@Context() ctx: any, @Args() args: DeleteManyAnswerValueArgs): Promise<BatchPayload> {
    return plainToClass(BatchPayload, await ctx.prisma.answerValue.deleteMany(args) as BatchPayload);
  }
}
