import { Args, Context, Info, Query, Mutation, Resolver } from "@nestjs/graphql";
import { DeleteAnswerValueArgs } from "./args/DeleteAnswerValueArgs";
import { AnswerValue } from "../../../models/AnswerValue";
import { plainToClass } from "class-transformer";

@Resolver(() => AnswerValue)
export class DeleteAnswerValueResolver {
  @Mutation(() => AnswerValue, {
    nullable: true,
    description: undefined
  })
  async deleteAnswerValue(@Context() ctx: any, @Args() args: DeleteAnswerValueArgs): Promise<AnswerValue | undefined> {
    return plainToClass(AnswerValue, await ctx.prisma.answerValue.delete(args) as AnswerValue);
  }
}
