import { Args, Context, Info, Query, Mutation, Resolver } from "@nestjs/graphql";
import { plainToClass } from "class-transformer";
import graphqlFields from "graphql-fields";
import { GraphQLResolveInfo } from "graphql";
import { AggregateAnswerValueArgs } from "./args/AggregateAnswerValueArgs";
import { CreateAnswerValueArgs } from "./args/CreateAnswerValueArgs";
import { DeleteAnswerValueArgs } from "./args/DeleteAnswerValueArgs";
import { DeleteManyAnswerValueArgs } from "./args/DeleteManyAnswerValueArgs";
import { FindManyAnswerValueArgs } from "./args/FindManyAnswerValueArgs";
import { FindOneAnswerValueArgs } from "./args/FindOneAnswerValueArgs";
import { UpdateAnswerValueArgs } from "./args/UpdateAnswerValueArgs";
import { UpdateManyAnswerValueArgs } from "./args/UpdateManyAnswerValueArgs";
import { UpsertAnswerValueArgs } from "./args/UpsertAnswerValueArgs";
import { AnswerValue } from "../../../models/AnswerValue";
import { AggregateAnswerValue } from "../../outputs/AggregateAnswerValue";
import { BatchPayload } from "../../outputs/BatchPayload";

@Resolver(() => AnswerValue)
export class AnswerValueCrudResolver {
  @Query(() => AnswerValue, {
    nullable: true,
    description: undefined
  })
  async answerValue(@Context() ctx: any, @Args() args: FindOneAnswerValueArgs): Promise<AnswerValue | undefined> {
    return plainToClass(AnswerValue, await ctx.prisma.answerValue.findOne(args) as AnswerValue);
  }

  @Query(() => [AnswerValue], {
    nullable: false,
    description: undefined
  })
  async answerValues(@Context() ctx: any, @Args() args: FindManyAnswerValueArgs): Promise<AnswerValue[]> {
    return plainToClass(AnswerValue, await ctx.prisma.answerValue.findMany(args) as [AnswerValue]);
  }

  @Mutation(() => AnswerValue, {
    nullable: false,
    description: undefined
  })
  async createAnswerValue(@Context() ctx: any, @Args() args: CreateAnswerValueArgs): Promise<AnswerValue> {
    return plainToClass(AnswerValue, await ctx.prisma.answerValue.create(args) as AnswerValue);
  }

  @Mutation(() => AnswerValue, {
    nullable: true,
    description: undefined
  })
  async deleteAnswerValue(@Context() ctx: any, @Args() args: DeleteAnswerValueArgs): Promise<AnswerValue | undefined> {
    return plainToClass(AnswerValue, await ctx.prisma.answerValue.delete(args) as AnswerValue);
  }

  @Mutation(() => AnswerValue, {
    nullable: true,
    description: undefined
  })
  async updateAnswerValue(@Context() ctx: any, @Args() args: UpdateAnswerValueArgs): Promise<AnswerValue | undefined> {
    return plainToClass(AnswerValue, await ctx.prisma.answerValue.update(args) as AnswerValue);
  }

  @Mutation(() => BatchPayload, {
    nullable: false,
    description: undefined
  })
  async deleteManyAnswerValue(@Context() ctx: any, @Args() args: DeleteManyAnswerValueArgs): Promise<BatchPayload> {
    return plainToClass(BatchPayload, await ctx.prisma.answerValue.deleteMany(args) as BatchPayload);
  }

  @Mutation(() => BatchPayload, {
    nullable: false,
    description: undefined
  })
  async updateManyAnswerValue(@Context() ctx: any, @Args() args: UpdateManyAnswerValueArgs): Promise<BatchPayload> {
    return plainToClass(BatchPayload, await ctx.prisma.answerValue.updateMany(args) as BatchPayload);
  }

  @Mutation(() => AnswerValue, {
    nullable: false,
    description: undefined
  })
  async upsertAnswerValue(@Context() ctx: any, @Args() args: UpsertAnswerValueArgs): Promise<AnswerValue> {
    return plainToClass(AnswerValue, await ctx.prisma.answerValue.upsert(args) as AnswerValue);
  }

  @Query(() => AggregateAnswerValue, {
    nullable: false,
    description: undefined
  })
  async aggregateAnswerValue(@Context() ctx: any, @Info() info: GraphQLResolveInfo, @Args() args: AggregateAnswerValueArgs): Promise<AggregateAnswerValue> {
    const transformFields = (fields: Record<string, any>): Record<string, any> => {
      return Object.fromEntries(
        Object.entries(fields)
          .filter(([key, value]) => !key.startsWith("_"))
          .map<[string, any]>(([key, value]) => {
            if (Object.keys(value).length === 0) {
              return [key, true];
            }
            return [key, transformFields(value)];
          }),
      );
    }

    return ctx.prisma.answerValue.aggregate({
      ...args,
      ...transformFields(graphqlFields(info as any)),
    });
  }
}
