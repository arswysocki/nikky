import { ArgsType, Field, Int } from "@nestjs/graphql";
import { AnswerValueUpdateInput } from "../../../inputs/AnswerValueUpdateInput";
import { AnswerValueWhereUniqueInput } from "../../../inputs/AnswerValueWhereUniqueInput";
import { Type as ClassTransformer__Type } from "class-transformer";

@ArgsType()
export class UpdateAnswerValueArgs {
  @ClassTransformer__Type(() => AnswerValueUpdateInput)
  @Field(() => AnswerValueUpdateInput, { nullable: false })
  data!: AnswerValueUpdateInput;

  @ClassTransformer__Type(() => AnswerValueWhereUniqueInput)
  @Field(() => AnswerValueWhereUniqueInput, { nullable: false })
  where!: AnswerValueWhereUniqueInput;
}
