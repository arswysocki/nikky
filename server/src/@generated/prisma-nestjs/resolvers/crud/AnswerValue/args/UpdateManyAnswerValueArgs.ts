import { ArgsType, Field, Int } from "@nestjs/graphql";
import { AnswerValueUpdateManyMutationInput } from "../../../inputs/AnswerValueUpdateManyMutationInput";
import { AnswerValueWhereInput } from "../../../inputs/AnswerValueWhereInput";
import { Type as ClassTransformer__Type } from "class-transformer";

@ArgsType()
export class UpdateManyAnswerValueArgs {
  @ClassTransformer__Type(() => AnswerValueUpdateManyMutationInput)
  @Field(() => AnswerValueUpdateManyMutationInput, { nullable: false })
  data!: AnswerValueUpdateManyMutationInput;

  @ClassTransformer__Type(() => AnswerValueWhereInput)
  @Field(() => AnswerValueWhereInput, { nullable: true })
  where?: AnswerValueWhereInput | undefined;
}
