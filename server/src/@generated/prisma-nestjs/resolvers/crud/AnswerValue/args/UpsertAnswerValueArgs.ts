import { ArgsType, Field, Int } from "@nestjs/graphql";
import { AnswerValueCreateInput } from "../../../inputs/AnswerValueCreateInput";
import { AnswerValueUpdateInput } from "../../../inputs/AnswerValueUpdateInput";
import { AnswerValueWhereUniqueInput } from "../../../inputs/AnswerValueWhereUniqueInput";
import { Type as ClassTransformer__Type } from "class-transformer";

@ArgsType()
export class UpsertAnswerValueArgs {
  @ClassTransformer__Type(() => AnswerValueWhereUniqueInput)
  @Field(() => AnswerValueWhereUniqueInput, { nullable: false })
  where!: AnswerValueWhereUniqueInput;

  @ClassTransformer__Type(() => AnswerValueCreateInput)
  @Field(() => AnswerValueCreateInput, { nullable: false })
  create!: AnswerValueCreateInput;

  @ClassTransformer__Type(() => AnswerValueUpdateInput)
  @Field(() => AnswerValueUpdateInput, { nullable: false })
  update!: AnswerValueUpdateInput;
}
