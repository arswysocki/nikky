import { ArgsType, Field, Int } from "@nestjs/graphql";
import { AnswerValueWhereUniqueInput } from "../../../inputs/AnswerValueWhereUniqueInput";
import { Type as ClassTransformer__Type } from "class-transformer";

@ArgsType()
export class FindOneAnswerValueArgs {
  @ClassTransformer__Type(() => AnswerValueWhereUniqueInput)
  @Field(() => AnswerValueWhereUniqueInput, { nullable: false })
  where!: AnswerValueWhereUniqueInput;
}
