import { ArgsType, Field, Int } from "@nestjs/graphql";
import { AnswerValueWhereInput } from "../../../inputs/AnswerValueWhereInput";
import { Type as ClassTransformer__Type } from "class-transformer";

@ArgsType()
export class DeleteManyAnswerValueArgs {
  @ClassTransformer__Type(() => AnswerValueWhereInput)
  @Field(() => AnswerValueWhereInput, { nullable: true })
  where?: AnswerValueWhereInput | undefined;
}
