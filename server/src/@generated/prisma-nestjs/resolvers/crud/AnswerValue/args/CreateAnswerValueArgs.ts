import { ArgsType, Field, Int } from "@nestjs/graphql";
import { AnswerValueCreateInput } from "../../../inputs/AnswerValueCreateInput";
import { Type as ClassTransformer__Type } from "class-transformer";

@ArgsType()
export class CreateAnswerValueArgs {
  @ClassTransformer__Type(() => AnswerValueCreateInput)
  @Field(() => AnswerValueCreateInput, { nullable: false })
  data!: AnswerValueCreateInput;
}
