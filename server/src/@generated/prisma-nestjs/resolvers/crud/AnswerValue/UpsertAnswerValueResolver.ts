import { Args, Context, Info, Query, Mutation, Resolver } from "@nestjs/graphql";
import { UpsertAnswerValueArgs } from "./args/UpsertAnswerValueArgs";
import { AnswerValue } from "../../../models/AnswerValue";
import { plainToClass } from "class-transformer";

@Resolver(() => AnswerValue)
export class UpsertAnswerValueResolver {
  @Mutation(() => AnswerValue, {
    nullable: false,
    description: undefined
  })
  async upsertAnswerValue(@Context() ctx: any, @Args() args: UpsertAnswerValueArgs): Promise<AnswerValue> {
    return plainToClass(AnswerValue, await ctx.prisma.answerValue.upsert(args) as AnswerValue);
  }
}
