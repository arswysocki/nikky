import { Args, Context, Info, Query, Mutation, Resolver } from "@nestjs/graphql";
import { FindOneAnswerValueArgs } from "./args/FindOneAnswerValueArgs";
import { AnswerValue } from "../../../models/AnswerValue";
import { plainToClass } from "class-transformer";

@Resolver(() => AnswerValue)
export class FindOneAnswerValueResolver {
  @Query(() => AnswerValue, {
    nullable: true,
    description: undefined
  })
  async answerValue(@Context() ctx: any, @Args() args: FindOneAnswerValueArgs): Promise<AnswerValue | undefined> {
    return plainToClass(AnswerValue, await ctx.prisma.answerValue.findOne(args) as AnswerValue);
  }
}
