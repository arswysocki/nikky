import { Args, Context, Info, Query, Mutation, Resolver } from "@nestjs/graphql";
import { UpdateManyAnswerValueArgs } from "./args/UpdateManyAnswerValueArgs";
import { AnswerValue } from "../../../models/AnswerValue";
import { plainToClass } from "class-transformer";
import { BatchPayload } from "../../outputs/BatchPayload";

@Resolver(() => AnswerValue)
export class UpdateManyAnswerValueResolver {
  @Mutation(() => BatchPayload, {
    nullable: false,
    description: undefined
  })
  async updateManyAnswerValue(@Context() ctx: any, @Args() args: UpdateManyAnswerValueArgs): Promise<BatchPayload> {
    return plainToClass(BatchPayload, await ctx.prisma.answerValue.updateMany(args) as BatchPayload);
  }
}
