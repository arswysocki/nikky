import { Args, Context, Info, Query, Mutation, Resolver } from "@nestjs/graphql";
import { FindManyAnswerValueArgs } from "./args/FindManyAnswerValueArgs";
import { AnswerValue } from "../../../models/AnswerValue";
import { plainToClass } from "class-transformer";

@Resolver(() => AnswerValue)
export class FindManyAnswerValueResolver {
  @Query(() => [AnswerValue], {
    nullable: false,
    description: undefined
  })
  async answerValues(@Context() ctx: any, @Args() args: FindManyAnswerValueArgs): Promise<AnswerValue[]> {
    return plainToClass(AnswerValue, await ctx.prisma.answerValue.findMany(args) as [AnswerValue]);
  }
}
