import { Args, Context, Info, Query, Mutation, Resolver } from "@nestjs/graphql";
import { CreateAnswerValueArgs } from "./args/CreateAnswerValueArgs";
import { AnswerValue } from "../../../models/AnswerValue";
import { plainToClass } from "class-transformer";

@Resolver(() => AnswerValue)
export class CreateAnswerValueResolver {
  @Mutation(() => AnswerValue, {
    nullable: false,
    description: undefined
  })
  async createAnswerValue(@Context() ctx: any, @Args() args: CreateAnswerValueArgs): Promise<AnswerValue> {
    return plainToClass(AnswerValue, await ctx.prisma.answerValue.create(args) as AnswerValue);
  }
}
