import { AnswerCrudResolver } from "./Answer/AnswerCrudResolver";
import { AnswerValueCrudResolver } from "./AnswerValue/AnswerValueCrudResolver";
import { AssessmentCrudResolver } from "./Assessment/AssessmentCrudResolver";
import { FormCrudResolver } from "./Form/FormCrudResolver";
import { ProjectCrudResolver } from "./Project/ProjectCrudResolver";
import { QuestionCrudResolver } from "./Question/QuestionCrudResolver";
import { UserCrudResolver } from "./User/UserCrudResolver";
import { Module } from "@nestjs/common";

export { AnswerCrudResolver } from "./Answer/AnswerCrudResolver";
export { FindOneAnswerResolver } from "./Answer/FindOneAnswerResolver";
export { FindManyAnswerResolver } from "./Answer/FindManyAnswerResolver";
export { CreateAnswerResolver } from "./Answer/CreateAnswerResolver";
export { DeleteAnswerResolver } from "./Answer/DeleteAnswerResolver";
export { UpdateAnswerResolver } from "./Answer/UpdateAnswerResolver";
export { DeleteManyAnswerResolver } from "./Answer/DeleteManyAnswerResolver";
export { UpdateManyAnswerResolver } from "./Answer/UpdateManyAnswerResolver";
export { UpsertAnswerResolver } from "./Answer/UpsertAnswerResolver";
export { AggregateAnswerResolver } from "./Answer/AggregateAnswerResolver";
export * from "./Answer/args";
export { AnswerValueCrudResolver } from "./AnswerValue/AnswerValueCrudResolver";
export { FindOneAnswerValueResolver } from "./AnswerValue/FindOneAnswerValueResolver";
export { FindManyAnswerValueResolver } from "./AnswerValue/FindManyAnswerValueResolver";
export { CreateAnswerValueResolver } from "./AnswerValue/CreateAnswerValueResolver";
export { DeleteAnswerValueResolver } from "./AnswerValue/DeleteAnswerValueResolver";
export { UpdateAnswerValueResolver } from "./AnswerValue/UpdateAnswerValueResolver";
export { DeleteManyAnswerValueResolver } from "./AnswerValue/DeleteManyAnswerValueResolver";
export { UpdateManyAnswerValueResolver } from "./AnswerValue/UpdateManyAnswerValueResolver";
export { UpsertAnswerValueResolver } from "./AnswerValue/UpsertAnswerValueResolver";
export { AggregateAnswerValueResolver } from "./AnswerValue/AggregateAnswerValueResolver";
export * from "./AnswerValue/args";
export { AssessmentCrudResolver } from "./Assessment/AssessmentCrudResolver";
export { FindOneAssessmentResolver } from "./Assessment/FindOneAssessmentResolver";
export { FindManyAssessmentResolver } from "./Assessment/FindManyAssessmentResolver";
export { CreateAssessmentResolver } from "./Assessment/CreateAssessmentResolver";
export { DeleteAssessmentResolver } from "./Assessment/DeleteAssessmentResolver";
export { UpdateAssessmentResolver } from "./Assessment/UpdateAssessmentResolver";
export { DeleteManyAssessmentResolver } from "./Assessment/DeleteManyAssessmentResolver";
export { UpdateManyAssessmentResolver } from "./Assessment/UpdateManyAssessmentResolver";
export { UpsertAssessmentResolver } from "./Assessment/UpsertAssessmentResolver";
export { AggregateAssessmentResolver } from "./Assessment/AggregateAssessmentResolver";
export * from "./Assessment/args";
export { FormCrudResolver } from "./Form/FormCrudResolver";
export { FindOneFormResolver } from "./Form/FindOneFormResolver";
export { FindManyFormResolver } from "./Form/FindManyFormResolver";
export { CreateFormResolver } from "./Form/CreateFormResolver";
export { DeleteFormResolver } from "./Form/DeleteFormResolver";
export { UpdateFormResolver } from "./Form/UpdateFormResolver";
export { DeleteManyFormResolver } from "./Form/DeleteManyFormResolver";
export { UpdateManyFormResolver } from "./Form/UpdateManyFormResolver";
export { UpsertFormResolver } from "./Form/UpsertFormResolver";
export { AggregateFormResolver } from "./Form/AggregateFormResolver";
export * from "./Form/args";
export { ProjectCrudResolver } from "./Project/ProjectCrudResolver";
export { FindOneProjectResolver } from "./Project/FindOneProjectResolver";
export { FindManyProjectResolver } from "./Project/FindManyProjectResolver";
export { CreateProjectResolver } from "./Project/CreateProjectResolver";
export { DeleteProjectResolver } from "./Project/DeleteProjectResolver";
export { UpdateProjectResolver } from "./Project/UpdateProjectResolver";
export { DeleteManyProjectResolver } from "./Project/DeleteManyProjectResolver";
export { UpdateManyProjectResolver } from "./Project/UpdateManyProjectResolver";
export { UpsertProjectResolver } from "./Project/UpsertProjectResolver";
export { AggregateProjectResolver } from "./Project/AggregateProjectResolver";
export * from "./Project/args";
export { QuestionCrudResolver } from "./Question/QuestionCrudResolver";
export { FindOneQuestionResolver } from "./Question/FindOneQuestionResolver";
export { FindManyQuestionResolver } from "./Question/FindManyQuestionResolver";
export { CreateQuestionResolver } from "./Question/CreateQuestionResolver";
export { DeleteQuestionResolver } from "./Question/DeleteQuestionResolver";
export { UpdateQuestionResolver } from "./Question/UpdateQuestionResolver";
export { DeleteManyQuestionResolver } from "./Question/DeleteManyQuestionResolver";
export { UpdateManyQuestionResolver } from "./Question/UpdateManyQuestionResolver";
export { UpsertQuestionResolver } from "./Question/UpsertQuestionResolver";
export { AggregateQuestionResolver } from "./Question/AggregateQuestionResolver";
export * from "./Question/args";
export { UserCrudResolver } from "./User/UserCrudResolver";
export { FindOneUserResolver } from "./User/FindOneUserResolver";
export { FindManyUserResolver } from "./User/FindManyUserResolver";
export { CreateUserResolver } from "./User/CreateUserResolver";
export { DeleteUserResolver } from "./User/DeleteUserResolver";
export { UpdateUserResolver } from "./User/UpdateUserResolver";
export { DeleteManyUserResolver } from "./User/DeleteManyUserResolver";
export { UpdateManyUserResolver } from "./User/UpdateManyUserResolver";
export { UpsertUserResolver } from "./User/UpsertUserResolver";
export { AggregateUserResolver } from "./User/AggregateUserResolver";
export * from "./User/args";

@Module({
  providers: [
    AnswerCrudResolver,
    AnswerValueCrudResolver,
    AssessmentCrudResolver,
    FormCrudResolver,
    ProjectCrudResolver,
    QuestionCrudResolver,
    UserCrudResolver
  ],
  exports: [
    AnswerCrudResolver,
    AnswerValueCrudResolver,
    AssessmentCrudResolver,
    FormCrudResolver,
    ProjectCrudResolver,
    QuestionCrudResolver,
    UserCrudResolver
  ]
})
export class CrudResolversModule {
}
