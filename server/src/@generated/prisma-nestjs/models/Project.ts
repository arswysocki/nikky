import { Field, Float, ID, Int, ObjectType } from "@nestjs/graphql";
import GraphQLJSON from "graphql-type-json";
import { Type as ClassTransformer__Type } from "class-transformer";
import { JsonValue, InputJsonValue } from "../../../../node_modules/@prisma/client";
import { Assessment } from "../models/Assessment";

@ObjectType({
  isAbstract: true,
  description: undefined,
})
export class Project {
  @Field(() => String, {
    nullable: false,
    description: undefined,
  })
  id!: string;

  assessments?: Assessment[] | undefined;
}
