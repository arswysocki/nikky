import { Field, Float, ID, Int, ObjectType } from "@nestjs/graphql";
import GraphQLJSON from "graphql-type-json";
import { Type as ClassTransformer__Type } from "class-transformer";
import { JsonValue, InputJsonValue } from "../../../../node_modules/@prisma/client";
import { Answer } from "../models/Answer";
import { AnswerValue } from "../models/AnswerValue";
import { Form } from "../models/Form";
import { QuestionType } from "../enums/QuestionType";

@ObjectType({
  isAbstract: true,
  description: undefined,
})
export class Question {
  @Field(() => String, {
    nullable: false,
    description: undefined,
  })
  id!: string;

  Form?: Form;

  @Field(() => QuestionType, {
    nullable: false,
    description: undefined,
  })
  type!: keyof typeof QuestionType;

  answerValues?: AnswerValue[] | undefined;

  @Field(() => String, {
    nullable: false,
    description: undefined,
  })
  formId!: string;

  answer?: Answer[] | undefined;
}
