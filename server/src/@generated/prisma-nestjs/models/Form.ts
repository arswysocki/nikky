import { Field, Float, ID, Int, ObjectType } from "@nestjs/graphql";
import GraphQLJSON from "graphql-type-json";
import { Type as ClassTransformer__Type } from "class-transformer";
import { JsonValue, InputJsonValue } from "../../../../node_modules/@prisma/client";
import { Assessment } from "../models/Assessment";
import { Question } from "../models/Question";

@ObjectType({
  isAbstract: true,
  description: undefined,
})
export class Form {
  @Field(() => String, {
    nullable: false,
    description: undefined,
  })
  id!: string;

  @Field(() => String, {
    nullable: false,
    description: undefined,
  })
  desc!: string;

  questions?: Question[] | undefined;

  Assessment?: Assessment;

  @Field(() => String, {
    nullable: false,
    description: undefined,
  })
  assessmentId!: string;
}
