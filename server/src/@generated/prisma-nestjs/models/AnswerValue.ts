import { Field, Float, ID, Int, ObjectType } from "@nestjs/graphql";
import GraphQLJSON from "graphql-type-json";
import { Type as ClassTransformer__Type } from "class-transformer";
import { JsonValue, InputJsonValue } from "../../../../node_modules/@prisma/client";
import { Answer } from "../models/Answer";
import { Question } from "../models/Question";

@ObjectType({
  isAbstract: true,
  description: undefined,
})
export class AnswerValue {
  @Field(() => String, {
    nullable: false,
    description: undefined,
  })
  id!: string;

  Question?: Question;

  @Field(() => String, {
    nullable: false,
    description: undefined,
  })
  questionId!: string;

  answer?: Answer | undefined;

  @Field(() => String, {
    nullable: false,
    description: undefined,
  })
  desc!: string;
}
