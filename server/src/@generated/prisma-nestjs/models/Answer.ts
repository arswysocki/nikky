import { Field, Float, ID, Int, ObjectType } from "@nestjs/graphql";
import GraphQLJSON from "graphql-type-json";
import { Type as ClassTransformer__Type } from "class-transformer";
import { JsonValue, InputJsonValue } from "../../../../node_modules/@prisma/client";
import { AnswerValue } from "../models/AnswerValue";
import { Question } from "../models/Question";
import { User } from "../models/User";
import { AnswerStatus } from "../enums/AnswerStatus";

@ObjectType({
  isAbstract: true,
  description: undefined,
})
export class Answer {
  @Field(() => String, {
    nullable: false,
    description: undefined,
  })
  id!: string;

  question?: Question;

  @Field(() => Boolean, {
    nullable: false,
    description: undefined,
  })
  canSkip!: boolean;

  user?: User;

  @Field(() => String, {
    nullable: false,
    description: undefined,
  })
  userId!: string;

  @Field(() => AnswerStatus, {
    nullable: false,
    description: undefined,
  })
  status!: keyof typeof AnswerStatus;

  @Field(() => Boolean, {
    nullable: false,
    description: undefined,
  })
  isAnonymous!: boolean;

  answerValue?: AnswerValue;

  @Field(() => String, {
    nullable: false,
    description: undefined,
  })
  answerText!: string;

  @Field(() => String, {
    nullable: false,
    description: undefined,
  })
  questionId!: string;

  @Field(() => String, {
    nullable: false,
    description: undefined,
  })
  answerValueId!: string;
}
