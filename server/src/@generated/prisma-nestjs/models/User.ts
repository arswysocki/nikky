import { Field, Float, ID, Int, ObjectType } from "@nestjs/graphql";
import GraphQLJSON from "graphql-type-json";
import { Type as ClassTransformer__Type } from "class-transformer";
import { JsonValue, InputJsonValue } from "../../../../node_modules/@prisma/client";
import { Answer } from "../models/Answer";
import { Assessment } from "../models/Assessment";
import { Gender } from "../enums/Gender";
import { Role } from "../enums/Role";

@ObjectType({
  isAbstract: true,
  description: undefined,
})
export class User {
  @Field(() => String, {
    nullable: false,
    description: undefined,
  })
  id!: string;

  @Field(() => String, {
    nullable: false,
    description: undefined,
  })
  firstName!: string;

  @Field(() => String, {
    nullable: false,
    description: undefined,
  })
  lastName!: string;

  @Field(() => String, {
    nullable: false,
    description: undefined,
  })
  email!: string;

  @Field(() => String, {
    nullable: false,
    description: undefined,
  })
  picture!: string;

  @Field(() => Gender, {
    nullable: false,
    description: undefined,
  })
  gender!: keyof typeof Gender;

  @Field(() => Role, {
    nullable: false,
    description: undefined,
  })
  role!: keyof typeof Role;

  Respondent?: Assessment[] | undefined;

  Recipient?: Assessment[] | undefined;

  Answer?: Answer[] | undefined;
}
