import { Field, Float, ID, Int, ObjectType } from "@nestjs/graphql";
import GraphQLJSON from "graphql-type-json";
import { Type as ClassTransformer__Type } from "class-transformer";
import { JsonValue, InputJsonValue } from "../../../../node_modules/@prisma/client";
import { Form } from "../models/Form";
import { Project } from "../models/Project";
import { User } from "../models/User";

@ObjectType({
  isAbstract: true,
  description: undefined,
})
export class Assessment {
  @Field(() => String, {
    nullable: false,
    description: undefined,
  })
  id!: string;

  user?: User;

  forms?: Form[] | undefined;

  @Field(() => String, {
    nullable: false,
    description: undefined,
  })
  userId!: string;

  recipient?: User;

  @Field(() => String, {
    nullable: false,
    description: undefined,
  })
  recipientId!: string;

  Project?: Project;

  @Field(() => String, {
    nullable: false,
    description: undefined,
  })
  projectId!: string;
}
