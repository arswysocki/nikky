/*
  Warnings:

  - Added the required column `desc` to the `AnswerValue` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "AnswerValue" ADD COLUMN     "desc" TEXT NOT NULL;
