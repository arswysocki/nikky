-- CreateEnum
CREATE TYPE "Role" AS ENUM ('USER', 'ADMIN');

-- CreateEnum
CREATE TYPE "Gender" AS ENUM ('Right', 'Wrong');

-- CreateTable
CREATE TABLE "User" (
    "id" TEXT NOT NULL,
    "firstName" TEXT NOT NULL DEFAULT E'',
    "lastName" TEXT NOT NULL DEFAULT E'',
    "email" TEXT NOT NULL DEFAULT E'',
    "picture" TEXT NOT NULL DEFAULT E'',
    "gender" "Gender" NOT NULL,
    "role" "Role" NOT NULL,

    PRIMARY KEY ("id")
);
