/*
  Warnings:

  - The migration will add a unique constraint covering the columns `[answerValueId]` on the table `Answer`. If there are existing duplicate values, the migration will fail.
  - Made the column `questionId` on table `AnswerValue` required. The migration will fail if there are existing NULL values in that column.
  - Made the column `assessmentId` on table `Form` required. The migration will fail if there are existing NULL values in that column.

*/
-- AlterTable
ALTER TABLE "AnswerValue" ALTER COLUMN "questionId" SET NOT NULL;

-- AlterTable
ALTER TABLE "Form" ALTER COLUMN "assessmentId" SET NOT NULL;

-- CreateIndex
CREATE UNIQUE INDEX "Answer_answerValueId_unique" ON "Answer"("answerValueId");
