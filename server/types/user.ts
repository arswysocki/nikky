type User = {
    id: string,
    firstName: string,
    lastName: string,
    email: string,
    gender: string,
    // role: User.Role,
    // status: User.Status,
    picture: string
}

type Role = {
    id: string,
    desc: string,
}
type UserToRole = {
    id: string,
    user: User,
    role: Role,
}

type Status = {
    id: string,
    desc: string,
}

type UserToStatus = {
    id: string,
    user: User,
    status: Status,
}