type EmailTemplate = {
    id: string,
    name: string,
    subject: string,
    body: string,
    kind: NotificationKind,
    recipient: NotificationRecipient
}

type NotificationKind = 'pre' | 'begin' | 'end';
type NotificationRecipient = 'author' | 'auditor';
