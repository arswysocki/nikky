import React from 'react';
import { User, Beaker, Chip } from "heroicons-react";
import './App.css';

export const App = () => {
  return (
    <div className="App">
      <div className="flex flex-col min-h-screen ">
        <div className="flex-shrink-0 w-64 bg-gray-900">
          <div className="flex items-center h-16 px-4 bg-gray-900 text-xl text-white font-medium">
            <Chip /><span className="ml-2">Nikky</span>
          </div>
          <div className={'px-2 py-2'} />
          <div className="px-6 py-6 border-l-2 border-gray-100 text-white text-left flex flex-row">
            <User /><span className="ml-2">Пользователи</span>
          </div>
          <div className="px-6 py-6 text-white text-left flex flex-row">
            <Beaker /><span className="ml-2">Исследования</span>
          </div>
        </div>
        <div className="flex-grow flex flex-col">
          <div className="relative shadow-md bg-white flex-shrink-0">
            <div className="flex justify-between items-center h-16 px-12"></div>
          </div>
        </div>
      </div>

    </div>
  );
}

export default App;
